Chaos Digest               Lundi 1 Fevrier 1993        Volume 1 : Numero 6

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.06 (1 Fev 1993)
NUMERO SPECIAL--PROGRAMMES DE RESTITUTION DES MOTS DE PASSE
File 1--Ouvre-boite pour mots de passe difficile
File 2--Communique de Presse Americain
File 3--Terroristes boliviens et WordPerfect (Reprint)
File 4--NTPASS, module chargeable sous NetWare (Reprint)
File 5--Qui est AccessData, Inc.? [societe et marque deposee]
File 6--Breve dans la presse francaise (Reprint)
File 7--Programmes de Restitution des Mots de Passe

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost from jbcondat@attmail.com. The editors may be
contacted by voice (+33 1 40101775), fax (+33 1 40101764) or S-mail at:
Jean-Bernard Condat, Chaos Computer Club France [CCCF], 47 rue des Rosiers,
93400 St-Ouen, France

Issues of Chaos-D can also be found on some French BBS. Back issues also
may be obtained from the mail server at jbcondat@attmail.com: all incoming
messages containing "Request: ChaosD #x.yy" in the "Suject:" field are
answered (x is the volume and yy the issue).

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Mon Feb  1 12:55:12 CST 1993
From: jbcondat@ATTMAIL.COM (Jean-Bernard Condat )
Subject: File 1--Ouvre-boite pour mots de passe difficile


     Qui n'a pas reve de pouvoir ouvrir sans scrupule un document protege
par un mot de passe que votre secretaire par trop consciencieuse aurait
decide de mettre sur un document de travail? La solution n'est pas simple.
Si votre document ou votre reseau est protege, il y a une raison pour cela.
Contourner ce probleme, ne rentre plus dans une optique tres catholique.

     Aussi ai-je ete tres etonne de lire deux articles de presse francais.
Ils traitaient d'un ensemble de produits specialises dans "la restitution
des mots de passe" [file 6].

     Je pris immediatement contact avec M. Serban de la Societe ANDA de
Vincennes qui n'hesita pas, en deux minutes, a me faxer un etonnant
communique de presse [file 7] et a me donner sa plus prestigieuse
reference: le secretariat du Premier Ministre!

     L'attachee de presse d'ANDA, Florence PROVOST, se deplaca pour une
demonstration sur site. Son document publicitaire parlait de
"reconstruction" a certains endroits, et de "restitution" a d'autres. La
definition du sigle PRMP etait aussi inconnue pour elle que l'utilisation
du logiciel XLPASS qu'elle tenta de faire sur un fichier Excel 2.0
fraichement cree sur mon Macintosh SE/30.

     Voici le rapport que je reussi a imprimer apres une recherche "rapide"
de 63 secondes:

                 +++++++++++++++++++++++++++++++++++++++
                 XLPASS Password Recovery Report.

                 Evaluation Copy. Not for resale.
                 Today's Date : Lun 18 Jan 1993    10:21
                 Number of files decrypted : 1
                 * * * * * * * * * * * * * * * * * * * *
                 File recovered : ABC
                 Creation Date : Lun 18 Jan 1993
                 Modified Date : Lun 18 Jan 1993
                 File size : 1024

                 File Protection Password:   jbc
                 +++++++++++++++++++++++++++++++++++++++


Je decidai de faire une petite recherche sur ce produit dans differentes
sources publiques. Tout d'abord, la societe AccessData, Inc., est un petite
structure d'Orem, UT aux USA. Elle possede sa marque depuis le 21 Octobre
1991 [file 5].

Alison Eliason, directeur du service client d'AccessData me fit parvenir un
communique de presse [File 2], ainsi que deux articles [Files 3-4]. Elle me
signala que le distributeur le plus proche de chez moi est:

     *Key Exchange Services Limited*
     30 Heath Road, Twickenham, Middlesex TW1 4DE, Grande-Bretagne
     Tel.: 081-744 1551, Fax: 081-744 0476

Olivier Schmidt-Berteau de cette societe anglaise me declara:

> Je vous prie de croire que nous avons conscience des risques d'une
> utilisation illicite de ces produits. C'est pourquoi nous avons veiller a
> etablir des garde-fous en limitant l'acces a nos logiciels.

> Ainsi chaque client doit retourner un accord signe par le responsable
> produit et contresigne par un membre de la direction. Cet accord
> [Password Recovery Programs--Agreement] stipule explicitement les
> conditions d'utilisation licites du logiciel et leurs limites.

> De plus, chaque logiciel vendu a son propre code d'acces. La delivrance
> du code d'acces par notre societe est subordonnee a la signature de
> l'accord mentionne ci-dessus. En ce qui concerne NTPASS, logiciel
> Password Recovery operant sur reseau Novell, le code d'acces est compose
> a partir du numero de serie du reseau et est donc propre a ce reseau.

> Finalement, le logiciel a ete volontairement limite par un caractere de
> securite. Password recovery ne restituera pas un mot de passe contenant
> ce caractere. Ceci permet d'avoir un deuxieme ecran de securite contre
> toute utilisation abusive.

> [...] Ce logiciel a ete explicitement autorise a l'exportation par le
> gouvernement des Etats-Unis pour une liste limitative de pays, dont la
> France.

> [...] jusqu'a present nos principaux clients ont ete des institutions
> publiques et leurs organes de securite. Ces derniers ont utilise notre
> produit principalement pour acceder a des fichiers informatiques
> verouilles, contenant des informations illicites ayant valeur de preuves.

> La liste de cee institutions va du FBI americain, de la Police Danoise,
> de l'Office des Fraudes de Scotland Yard au Ministere de la Defense
> britannique. Considerer ces clients comme etant des pirates me parait
> etre irreverencieux, au pire diffamatoire.

Chaotiquement votre,

jbc
--
   _-_|\    Jean-Bernard Condat
  /     \   Chaos Computer Club France [CCCF]   B.P. 8005
  \_.-*_/   E-Mail: jbcondat@attmail.com        69351 Lyon Cedex 08, France
       v    Phone:  +33 1 47874083              Fax:  +33 1 47877070

------------------------------

Date: Sat Jan 30 19:44:00  1993
From: Antoine_Schmitt@NeXT.COM (Antoine Schmitt )
Subject: File 2--Communique de Presse Americain

NEWS RELEASE
FOR IMMEDIATE RELEASE

CONTACT:

Allison Eliason\AccessDate, Inc.
801/224-6970 or 800/658-5199


                       PASSWORD  LOCKED  FILES ?

     OREM, UT--What does a company do when its payroll officer dies or
is terminated, leaving behind inacessible files--locked by a private
password?  Companies around the world have agonized over being locked
out of their own files because of employee spite or accident.

     AccessData in Orem, Utah--a company which specializes in data
recovery software--has released several  password  recovery  software
programs.  AccessData has password recovery software available for
WordPerfect, Lotus 1-2-3, Microsoft Excel, Quattro Pro, Paradox, and
many other software packages.  AccessData's software recovers the
password used to lock documents with the encryption process available
in many of the major software companies programs.

     "Most companies do not use passwords for fear of having critical
documents locked by unknown passwords," said AccessData president,
Eric Thompson.  "Now companies can recommend with confidence that
their employees use passwords to secure all documents. If a password
is lost or forgotten, one of our programs can be used by the systems
administrator as a master key to unlock the file."

     For instance, an information systems director for a large
Oklahoma credit union was able to use the WordPerfect version of the
program (WRPASS) to unlock critical documents left behind by employees
dismissed from the firm on bad terms.  "If I did not have this
password recovery program, I would not recommend the use of
passwords," the systems director said.  "Now we use passwords to
increase security and prevent unauthorized access to sensitive files
without the threat of losing irreplaceable data."

     Government agencies have also found the use of AccessData's
password recovery programs invaluable.  In Washington D.C., FBI agents
obtained a floppy disk which contained the diary of a man suspected of
arson.  Using WRPASS, the agents unlocked the diary and solidified the
case.  The FBI, CIA, Secret Service and all branches of the Armed
Services have purchased password recovery software from AccessData.

     Using block-frequency analysis, AccessData's programs analyze
frequently occuring patterns in the encoded file as a basis for
determining the unknown password.  The software is well documented and
features on-line help.  To protect against unauthorized use, the
program has a security feature built in which requires the user to
enter an access code before password recovery can begin.

     Suggested retail is $185.00 for programs sold within the United
States.  AccessData offers discounts to government agencies and
educational institutions as well as quantity discounts.

------------------------------

Date: Fri Jan 29 10:29:46 EST 1993
From: LUX%DMRHRZ11.BITNET@VTVM1.CC.VT.EDU (Harald Lux )
Subject: File 3--Terroristes boliviens et WordPerfect (Reprint)
Copyright: Ziff-Davis Publishing Co., 1991


 Software  cracks  terrorists'  lock on seized files. (Access Data Recovery
     Service's WPPass)
 McCormick, John
 Government Computer News  VOL.: v10  ISSUE: n3  PAGINATION: p1(2)
 PUBLICATION DATE: Feb 4, 1991
 GEOGRAPHIC LOCATION: Bolivia
 PRODUCT  NAME(S):  WPPASS  (Utility  program)  -  Usage; WordPerfect (Word
    processing software) - Usage
 COMPANY NAME(S): Access Data Recovery Service - Products
 DESCRIPTORS:  Passwords;  Encryption;  Data  Security;  Bolivia; Political
    Issues; Military; Security Software; Utility Programs
 SIC CODE: 7372
 ISSN: 0738-4300

 Software Cracks Terrorists' Lock On Seized Files

  When Bolivian government forces seized WordPerfect files from a suspected
terrorist group late last year, the terrorists probably did not fear that
their secrets would become known to government forces.

     They had encrypted their files using WordPerfect's password file lock.
But using WPPass, a password-cracking software, U.S. officials gained
access to the files.

     "The U.S. government and my country team, in particular, thank you for
your critical assistance," said Robert S. Gelbard, U.S. ambassador to
Bolivia, in a letter to Eric Thompson, president of Access Data Recovery
Service of Orem, Utah. Access Data's WPPass package enabled U.S. officials
to crack the passwords.

     The most recent act of terrorism aimed at Americans in Bolivia took
place Oct. 10, when a bomb placed outside the residence of the embassy's
Marine Corps guards killed one Bolivian and injured another. Three U.S.
Marines inside the building, although shaken by the blast, were not
injured.

     Randolph Marcus of the State Department's counterterrorism section in
Washington confirmed that the embassy had experienced several terrorist
attacks last year. "In the wake of the Panama invasion last year there were
several bombings in the vicinity of the embassy," Marcus said. "The most
recent one was the Marine House."

     Marcus said the attack on Marine House was being blamed on the CNZP,
or Nestor Paz Zamora Committee faction of the Army of National Liberation,
a leftist terorist group in Bolivia.

     The files apparently contained information on terrorist activities. No
information was available on how the government forces captured the files.

     After being contacted by several Army majors in December, Thompson
sent a copy of his company's password-busting software to the embassy.

     Thompson said WPPass, which works with many versions of WordPerfect,
analyzes password-protected files using statistical analysis of the
frequency of letters used in words contained in the file.

     Access Data bases its password-cracking programs on the normal
frequency of letters occurring in English words and sentences.

     Thompson said the programs have proven useful for other languages.

     Multilingual Capabilities
     In English, German and French, the letter "E" is the most often used,
experts have determined. To decode a block of text, one could find the most
common letter, assume it was substituted for "E," and proceed down the list
of single letters and pairs of letters according to their normal rate of
occurrence.

     Although Italian, Spanish and Portuguese differ in terms of which
individual letters are most common, enough similarities exist to let the
password-cracking software produce a useful analysis in many cases.

     WPPass and the company's other products are not general-purpose file
decryption utilities.

     They work with text files generated by specific programs, including
Word Perfect, Lotus Development Corp.'s 1-2-3 and Microsoft Corp.'s Excel

     The password-cracking programs do not decode an encrypted file and
convert it to plain text. Instead, they attempt to figure out the password
used to encrypt the file.

     Although these programs refer to their file-locking schemes as
"password protection" systems, what they actually do is use a user-selected
password as the encryption/decryption key.

     Analysis of the file can yield the password.

     Gelbard, a career diplomat, joined the State Department in 1967 and
was appointed ambassador to Bolivia in 1988.

     Bolivia, a landlocked mountainous country about the size of Texas and
California combined, has been controlled by a military dictatorship in the
past. Now it has a democratically elected government.

     War on Drugs

     The United States has been pressuring the Bolivian government to
reduce the production of coca leaves, which are processed into cocaine.

     A former official of a U.S. intelligence agency who insisted on
anonymity said this emphasis to cut the supply of one of the country's
major cash crops has led to clashes between Bolivian police and coca
growers, as well as a general increase in anti-United States sentiment.

     Marcus indicated the investigation related to the ambassador's letter
was continuing.

     State officials declined to comment on information obtained from the
terrorists' files, how the files are being used by the Bolivian government
or details relating to possible terrorist activities.

     Thompson said Access Data Recovery Service's products normally are
sold to businesses and government agencies that need access to their own
files to which the passwords have been lost. Thompson said he also works
with law enforcement and intelligence agencies, but he did not elaborate.

------------------------------

Date: Thu Jan 28 23:35:25 +0100 1993
From: langlois@ciril.fr (Langlois Ph. )
Subject: File 4--NTPASS, module chargeable sous NetWare (Reprint)
Copyright: Professional Press Inc., 1992


 TITLE: Password recovery. (Brief Article: NTPASS password recovery program
     from  AccessData for the NetWare network operating system is a NetWare
     Loadable Module) (Product Announcement)
 JOURNAL: LAN Computing  VOL.: v3  ISSUE: n9  PAGINATION: p49(1)
 PUBLICATION DATE: Sept, 1992
 ARTICLE TYPE: Product Announcement
 AVAILABILITY:  FULL TEXT Online  LINE COUNT: 00016
 SOURCE FILE: CD File 275
 OPERATING PLATFORM(S): NetWare
 PRODUCT NAME(S): NTPASS (Data security software) - Product introduction
 COMPANY NAME(S): AccessData - Product introduction
 SIC CODE: 7372
 ISSN: 0896-145X
 DESCRIPTORS: Product Introduction; Passwords; Security Software; Add-In/On
    Software; Data Security

  AccessData announced NTPASS, a Novell NetWare password recovery program
that lets users change any password without the knowledge of any previously
existing passwords.
  NTPASS is a standard NetWare Loadable Manager that can only be loaded at
the file server. In order to run it, an access code must be obtained from
the company and entered. Since the access code is a derivative of the
NTPASS serial code and NetWare serial number, each version of NetWare
requires a different code. Once the code has been entered, users can change
any NetWare password, including the supervisor's password.

     For more information, contact AccessData, 87 E. 600 S., Orem, Utah
84058; (801) 224-6970.

------------------------------

Date: Tue Jan 26 18:59:41 -0800 1993
From: jbcondat@ATTMAIL.COM (Jean-Bernard Condat )
Subject: File 5--Qui est AccessData, Inc.? [societe et marque deposee]
Copyright: TRADEMARKSCAN(r)-Federal, 1993

     ACCESSDATA

     125 STARCREST DR
     OREM, UT 84058-

Current as of:     930111


TRW Company Number: Y00365207

ADDITIONAL PAYMENTS
--------------------
                                                         % of Accounts
                                                       --Days Past Due--
  Creditor   TRW  Sale Terms         High                 1-  31  61  91
  Category   Date Date             Credit    Balance Cur  30  60  90  +
------------------------------------------------------------------------
  AIR TRANS  9207  N/A VARIED          $0         $0   0   0   0   0   0
  Comment: SATSFTRY   AGED



  PUBLIC RECORD INFORMATION
=============================
COLLATERAL COUNTS
-----------------

Company has   1 UCC filings with   1 collateral item(s).


UCC DETAILS
-----------

Date
Filed  Legal Description   Document #      Document Location
------------------------------------------------------------
921123 UCC-FILED            42683            SEC STATE,  UT
       UNDEFINED 42683

+++++++

ACCESSDATA

          INTL CLASS:   9 (Electrical and Scientific Apparatus)
          U.S. CLASS:  38 (Prints and Publications)
          STATUS: Registered
          GOODS/SERVICES: COMPUTER PROGRAMS AND INSTRUCTIONAL MANUALS SOLD
            AS A UNIT FOR FILE UTILITIES
          SERIAL NO.: 74-248,543
          REG. NO.: 1,732,702
          REGISTERED: November 17, 1992
          FIRST USE: October 21, 1991 (Intl Class 9)
          FIRST COMMERCE: October 21, 1991 (Intl Class 9)
          FILED: February 21, 1992
          PUBLISHED: August 25, 1992
          ORIGINAL APPLICANT: THOMPSON, ERIC (United States Individual), 87
            EAST 600 SOUTH, OREM, UT (Utah), 84058, USA (United States of
            America)
          OWNER AT PUBLICATION: THOMPSON, ERIC (United States Individual);
            D/B/A/ ACCESSDATA, 87 EAST 600 SOUTH, OREM, UT (Utah), 84058,
            USA (United States of America)
          FILING CORRESPONDENT: ANDREW C. HESS, CALLISTER DUNCAN-NEBEKER,
            SUITE 800, KENNECOTT BUILDING, SALT LAKE CITY, UT  84133

------------------------------

Date: Fri Jan 29 08:01:48 EST 1993
From: jbcondat@ATTMAIL.COM (Jean-Bernard Condat )
Subject: File 6--Breve dans la presse francaise (Reprints)
Copyrights: Groupe Tests et Micro-Systemes, 1993

                    MOT DE PASSE ET PASSE-PARTOUT
    (in: "L'Ordinateur Individuel", numero 37 [Fev 1993], page 60)

     Element de securite tres appreciable lorsqu'il protege contre les
indiscretions, le mot de passe devient un obstacle redoutable quand son
detenteur l'oublie ou encore quand un collaborateur ne le transmet pas
en quittant l'entreprise. Parfois il est alors indispensable de reinstaller
le reseau Netware...

     C'est pour epargner pareille mesaventure que la societe Access Data a
mis au point un ensemble de programmes de recouvrement des mots de passe
operant avec Netware et avec plusieurs grands logiciels de bureautique sous
MS-Dos ou Windows.

     Ces "Pass" sont a la fois simples a appliquer (l'utilisateur n'a
pratiquement pas a intervenir) et d'execution rapide: a titre indicatif, le
temps necessaire pour retrouver le mot de passe cryptant un fichier de
WordPerfect est de l'ordre d'une minute. Par mesure de securite, leur
utilisation est elle-meme protegee par un mot de passe.

     Distribues en France par la societe Anda, ces divers outils operent
sur les documents ecrits en allemand, espagnol, francais, italien et
portugais. Sur demande, ils sont fournis avec certains jeux de caracteres
convenant pour d'autres langues.

     Les Pass sous MS-Dos pour les applications WordPerfect, Word, Lotus
(1-2-3, Symphony et Quattro Pro), Paradox et Excel (il existe aussi une
version pour Macintosh) valent chacun 3 250 F ht.  L'ensemble des cinq
produits est vendu 9 750 F ht, et Anda propose egalement un Pass donnant
acces au reseau Novell et permettant de changer la totalite des mots de
passe, y compris celui du superviseur (5 400 F ht). Il est a signaler que,
dans ce dernier cas, le programme ne fonctionne que sur le ou les serveurs
pour lesquels l'utilisateur a obtenu un code d'acces delivre par Anda.

+++++++

                      WDPASS PERCE VOS SECRETS
                          Par V. Verhaeghe
          (in: "MicroSystemes", numero Jan 1993, pp. 34-5)


     La societe ANDA importe en France un ensemble d'utilitaires d'un genre
nouveau et assez etonnant. Ces programmes permettent en effet de retrouver
des mots de passe affectes a des documents Word au cas ou vous ne vous en
souviendriez plus.

     Cela pose evidemment un probleme de securite; s'il est possible de
trouver le mot de passe d'un fichier, rien n'interdit a un esprit
malveillant de detourner l'utilite premiere de WDPASS pour acceder a des
fichiers sensibles. Pour empecher cela, tous les produits du type PRMP
(Programmes de Reconstruction de Mots de Passe) de ANDA ne peuvent etre
vendus qu'a des societes et non a des particuliers.

     WDPASS ne fonctionne qu'avec des fichiers de type Word. ANDA propose
une version correspondant a la plupart des logiciels du marche. Ainsi,
LTPASS decrypte les fichiers Lotus 1-2-3, Symphonie (sic) et Quattro Pro,
PXPASS s'occupe des fichiers Paradox (DOS et Windows) et XLPASS des
fichiers Excel (DOS et Windows).

     L'utilisation de WDPASS est on ne peut plus simple. Vous le lancez
sous DOS, vous indiquez quel fichier est a decrypter et vous choississez un
des trois modes de recherche de mots de passe (lente, moyenne ou rapide).
Il est recommande d'utiliser tout d'abord le mode rapide puis d'etendre la
recherche si celle-ci s'est averee infructueuse. En quelques minutes, vous
pouvez ainsi recuperer les acces a plusieurs fichiers.

     Vous avez par ailleurs la possibilite de demander le decryptage de
plusieurs fichiers simultanement ou meme d'un repertoire entier.WDPASS
utilise une methode appele "analyse cryptique". La documentation affirme
que les mots de passe sont reconstruits a partir d'une "correlation des
divers modeles statistiques avec les configurations frequentes du fichier
crypte" et on ne saurait la dementir.

     Tout ce que l'on peut dire d'autre sur ce produit, c'est qu'il a tres
bien fonctionne pour les differents tests que nous avons effectues. ANDA
indique qu'il existe egalement une version pour NetWare (NTPASS) que nous
n'avons malheureusement pas eu la chance d'essayer. NTPASS peut communiquer
et changer le mot de passe de l'administrateur, meme si la personne qui
l'utilise n'a pas les droits d'acces suffisants.

     Etant donne qu'il est rare que l'administrateur perde son mot de
passe, NTPASS ne sera utile qu'en cas de malveillance d'un employe ayant
decide de faire un chantage au changement de mot de passe.

------------------------------

Date: Nov 18 1992
From: Florence Provost (22 rue de la Tremoille, 75008 Paris)
Subject: File 7--Programmes de Restitution des Mots de Passe

CONTACT:

ANDA, 2bis av. Foch, 94160 Saint-Mande
(1) 43 65 89 06 ou fax: (1) 43 65 96 22


Interet des Programmes de Reconstruction des Mots de Passe

Le besoin de securite dans l'utilisation des ordinateurs augmente. Pour
repondre a ce besoin, les editeurs de logiciels ont implante des fonctions
de securite dans leurs produits, comme par exemple les mots de passe. Il
arrive que pour differentes raisons les mots de passe soient perdus,
rendant les fichiers proteges inaccessibles.

Les PRMP permettent la restauration des mots de passe perdus,
deverrouillant ainsi les fichiers cryptes.

L'existence des PRMP encourage les utilisateurs a mettre en oeuvre les
fonctionnalites de securite des logiciels qu'ils utilisent. Les PRMP
representent une *Assurance Tous Risques* pour l'immense majorite des
utilisateurs.


Les PRMP disponibles

* WD Pass pour [Word sous] DOS et Windows;
* LT Pass pour Lotus 123, Symphonie (sic) et Quatro Pro (re-sic!) versions
Dos et Windows [et VP Planner];
* PX Pass pour toutes les versions Paradox, Dos et Windows, Works;
* XL Pass pour toutes les versions Dos et Windows de Microsoft Excel. Une
version Macintosh est aussi valable.
[* WR Pass pour toutes versions WordPerfect DOS, Windows et Macintosh;]
[* NT Pass operant sur le reseau Novell.]

Les programmes sont concus pour tous types d'utilisateurs, meme debutants.
Il est possible de deverrouiller un seul fichier, plusieurs fichiers a la
fois ou l'ensemble d'un repertoire. Les programmes sont rapides (un test de
l'Administration americaine indique qu'un IBM PC 386 Modele 80 de 16 MHz
met moins de 60 secondes pour trouver un mot de passe de WordPerfect),
faciles a utiliser et dotes d'ecrans d'aide.


Comment fonctionnent-ils?

La reconstruction des mots de passe est realisee par la correlation des
divers modeles statistiques avec les configurations frequentes du fichier
crypte. Cette methode d'analyse d'un fichier pour determiner un mot de
passe inconnu est appele analyse cryptique.


Quand utiliser les PRMP?

Pour le niveau de securite moyen necessite par la plupart des societes, la
protection par un mot de passe est suffisante. Cela peut empecher le
personnel de "fouiner" dans des fichiers auxquels il ne doit pas avoir
acces. Chaque profession a des informations ne devant pas etre accessibles
a l'ensemble du personnel. Rapports personnels, informations sur les
salaires, memos confidentiels ou autres, peuvent etre rendus inaccessibles
a des yeux indiscrets mais restent accessibles aux presonnes autorisees.
Ces securites quotidiennes dont la plupart des entreprises ont besoin, font
des PRMP des outils indispensables pour chaque responsable bureautique.


Qui peut utiliser les PRMP?

Les PRMP representent une mesure de securite pour se proteger contre les
utilisateurs non autorises. Chaque copie a son propre et unique code de
securite. Cela permet aux programmes d'etre stockes sur n'importe quel
reseau ou disque dur sans risque d'acces non autorise.

Avant que la recuperation de mot de passe commence, le code de securite
doit etre entre. S'attribuer le code de securite confidentiel permettra
d'etre la seule personne a avoir acces au logiciel, eliminant ainsi toutes
chances pour les personnes non autorisees de retrouver les mots de passe.


Securite supplementaire

Certaines organisations pourraient etre concernees par l'utilisation des
PRMP par des personnes non autorisees. Pour cette raison un caractere de
securite a ete choisi qui, s'il est inclu dans le mot de passe, empechera
le PRMP de reveler le mot de passe. Cela permet de proteger completement
contre un acces non autorise.


Les PRMP: des logiciels multilangues

Les PRMP ont ete utilises avec succes pour retrouver des documents ecrits
en francais, allemand, italien, espagnol et portugais. Les mots de passe
contenant des caracteres comme un "a" accent grave ou un "c" cedille
pourraient etre retrouves avec succes. Veuillez nous contacter pour les
autres langues et jeux de caracteres.


Systeme requis

MS-Dos 3.0 ou suivants, 256 Ko memoire vive (RAM)
Lecteur de disquette 3 1/2" ou 5 1/4"


Limitations

Ces produits sont prevus pour utiliser des mots de passe allant jusqu'a 36
caracteres. Si vous avez besoin d'utiliser des mots de passe plus longs,
veuillez nous contacter.


     "N'hesitez pas a nous demander la disquette de demonstration"


          >>> Un verrou n'est pas un verrou sans une cle! <<<


|Tous les noms des produits sont la propriete et les noms de marque de   |
|leurs auteurs (sic). Toute l'information est supposee correcte au moment|
|de l'impression. ANDA n'accepte aucune responsabilite pour toute non    |
|conformite et les specifications peuvent changer sans preavis.          |

------------------------------

End of Chaos Digest #1.06
************************************

Downloaded From P-80 International Information Systems 304-744-2253
