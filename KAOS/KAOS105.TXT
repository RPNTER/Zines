Chaos Digest               Lundi 25 Janvier 1993        Volume 1 : Numero 5

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.05 (25 Janv 1993)
File 1--Un pirate a l'abordage de la Banque de France
File 2--Le coup d'un genie de l'informatique
File 3--Definition des codes parasites autopropageables (CPAs)
File 4--Le Pheacking americain vu du cote francais
File 5--Horloge en panne, pourquoi?
File 6--Jeune Lettonien a la recherche de correspondants

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost from jbcondat@attmail.com. The editors may be
contacted by voice (+33 1 40101775), fax (+33 1 40101764) or S-mail at:
Jean-Bernard Condat, Chaos Computer Club France [CCCF], 47 rue des Rosiers,
93400 St-Ouen, France

Issues of Chaos-D can also be found on some French BBS. Back issues also
may be obtained from the mail server at jbcondat@attmail.com: all incoming
messages containing "Request: ChaosD #x.yy" in the "Suject:" field are
answered (x is the volume and yy the issue).

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Sat Jan 23 00:09:28 +0100 1993
From: langlois@ciril.fr (Langlois Ph. )
Subject: File 1--Un pirate a l'abordage de la Banque de France
Copyright: Agence France Presse, 1993

  Divers escroquerie -  Arrestation d'un jeune escroc qui avait perce
les secrets de la Banque de France- GRENOBLE, 17 jan  93 (250 MOTS)
  Un jeune homme de 22 ans qui avait reussi, par un stratageme non
revele par les policiers, a percer un secteur secret de la Banque de
France en s'infiltrant dans le systeme informatique, a ete arrete et
ecroue samedi apres avoir ete presente au Parquet de Grenoble.
  Laurent Darvey, domicilie a Fontaine (Isere) et passionne
d'informatique, a reussi a connaitre le code donnant la liste des
chequiers voles centralisee a la Banque de France, a-t-on indique de
source policiere.
  Ayant derobe des chequiers, il a pu disposer librement de ceux qui
ne figuraient pas sur ces listes. C'est ainsi qu'au cours des deux
dernieres annees, il a utilise les formulaires de six chequiers ne
lui appartenant pas et "depenser" la somme relativement modeste de
40.000 francs. Pour mieux ecouler ces cheques, il avait egalement
derobe trois cartes d'identites qu'il a falsifiees.
  Il a ete demasque par un commercant qui avait releve le numero
de la carte d'identite presentee mais egalement celui de l'automobile
au volant de laquelle l'escroc repartait. Les plaques mineralogiques
etaient egalement fausses mais le signalement precis du commercant a
permis l'arrestation du jeune homme qui s'est vu signifier une
inculpation de faux, usage de faux et escroquerie.
   HM/jmg

------------------------------

Date: Fri Jan 22 13:52:48 -0500 1993
From: Mike.Swartzbeck@p816.f70.n109.z1.fidonet.org (Mike Swartzbeck)
Subject: File 2--Le coup d'un genie de l'informatique
Copyright: Serge Pueyo, France Soir, 1993

            UN PIRATE A L'ABORDAGE DE LA BANQUE DE FRANCE

       Il annulait les oppositions faites sur les cheques voles.
                 Exploit technique mais maigre butin

     Laurent Darvey a 22 ans. Domicilie a Fontaine, pres de Grenoble
(Isere), il a une passion: l'informatique, ou plus exactement le piratage
informatique. En 1991, il se procure sous une fausse identite un document
informatique qui lui permet d'acceder au fichier de la Banque de France,
fichier contenant les identifications des cheques voles. Grace a son
ordinateur et par un procede de son invention, qui n'a pas ete revele,
Laurent Darvey reussit a annuler les procedures d'opposition faites sur les
chequiers qu'il a derobes.

     Le jeune homme ne se contente pas de pirater la Banque de France. Il
est aussi voleur et faussaire a ses heures. Il a ainsi vole a plusieurs de
ses collegues de travail leurs chequiers et leurs papiers, qu'ils
s'empressait aussitot de maquiller habilement. De plus, il circulait avec
un voiture faussement immatriculee. Laurent Darvey ne semblait pas avoir de
gros besoins, puisqu'en un peu plus d'un an, il n'aurait emis que 40.000 F
de cheques voles.

     Mais une habitante de Voiron (Isere) va mettre les policiers sur ses
traces. En decembre, Laurent darvey lui achete pour 5.500 F un ordinateur,
qu'il revend aussitot. Mefiante, la dame releve toutefois le numero de sa
carte d'identite et, a la derobee, le numero de sa plaque
d'immatriculation. Au cas ou...


CARTE VOLEE.  Decouvrant qu'elle a ete payee avec un cheque en bois, elle
porte plainte. Les enqueteurs s'apercoivent alors que la fausse carte
d'identite de l'escroc a ete volee en juin 1991 dans un centre de formation
professionnelle pour adultes, a Pont-de-Claix. Ils se font donc remettre
tous les dossiers des stagiaires--soixante-six au total--parmi lesquels se
trouve celui de Laurent Darvey, puisqu'il avait suivi une formation dans ce
centre.

     Sa victime le reconnait sur photo. Les policiers ont arrete jeudi
dernier le jeune homme a son domicile, ou ils ont retrouve d'autres
chequiers voles, des cartes d'identite falsifiees et du materiel
informatique, vole lui aussi. Laurent Darvey a reconnu sans difficulte les
faits qui lui sont reproches. Il a ete ecroue a la prison de Varces, pres
de Grenoble.

     Comme le prejudice n'est pas tres eleve, on peut penser que, pour le
petit genie de l'informatique, pirater la Banque de France, reputee
inviolable, etait finalement le but du jeu. Un petit jeu qui s'acheve
pourtant derriere les barreaux.

------------------------------

Date: Sat Jan 23 13:49:00 -0600 1993
From: roberts@decus.arc.ab.ca ("Rob Slade, DECrypt Editor, VARUG NLC rep,
604-984-4067" )
Subject: File 3--Definition des codes parasites autopropageables (CPAs)
Copyright: Robert M. Slade, 1991, 1993


            Towards a Definition of computer Viral Programs
                              Robert Slade

    (The following is excerpted from Robert Slade's weekly column on
    Computer Viral programs published in VIRUS-L and on Fidonet.  These
    articles originally appeared in July, 1991.)

The "man on the street" is now often aware of the term "computer virus"
even if he (or she) does not use a computer.  However, it is often the case
that those who are otherwise technically literate do not understand some of
the implications of the phrase.  This is not surprising in that the term is
slang, is often misused, and that "hard" information is difficult to come
by.

It is important to know what a computer virus is if you are going to defend
yourself against the many that are "out there."  It is also important to
know what a computer virus is not.  There are other types of programs and
situations which can do damage to your computer or data, and many of these
will not be caught by the same methods which must trap viral programs.

A biological analogy, which we find in the dictionary, is helpful.  The
Oxford English Dictionary, which speaks of:
    "...a moral or intellectual poison, or poisonous influence..."
while satisfying to the wounded ego of those who have been hit is not
terribly helpful in a technical sense.  Webster, however, steers us in a
more helpful route in stating that a virus is:
    "...dependent on the host's living cells for their growth and
reproduction..."

By eliminating the biological references, we can come to the definition
that a virus is an entity which uses the resources of the host to spread
and reproduceitself without informed operator action.  Let me stress here,
the word "informed."  A virus cannot run completely on its own.  The
computer user must always take some action, even if it is only to turn the
computer on.  This is the major strength of a virus: it uses *normal*
computer operations to do its dirty work, and therefore there is no single
identifying code that can be used to find a viral program.

I must make mention, before I continue, of the work of Fred Cohen.  Dr.
Cohen is generally held to have coined the term "computer virus" in his
thesis, published in 1986.  However, his definition covers only those
sections of code which, when active, attach themselves to other programs.
This, however, neglects many of the programs which have been most
successful "in the wild".  Many researchers still insist on this
definition, and therefore use other terms such as "worm" and "bacterium"
for those viri which do not attack programs.

Having established that viral programs copy themselves, and before going on
to related types of programs, let me list a few things that viri are *not*.

Let me first say that computer viral programs are not a "natural"
occurrence.  These are programs which are written by programmers.  They did
not just appear through some kind of electronic evolution.  Viral programs
are written, deliberately, by people.  (Having studied the beasts almost
from their inception, I was rather startled when a young, intelligent, well
educated executive proposed to me that viri had somehow "just grown" like
their biological counterparts.)

The popular press has recently started to publicize the term computer
virus, but without giving any details other than the fact that viri are to
be feared. (Often the reports talk about "main storage destroyed" and other
such phrases which have very little meaning.)  This has given most people
the impression that anything that goes wrong with a computer is a virus.
>From hardware failures to errors in use, everything is blamed on a virus.
*A VIRUS IS NOT JUST ANY DAMAGING CONDITION.*

Likewise, it is now considered that any program that may do damage to your
data or your access to computing resources is a virus.  We will speak
further about trojan horse programs, logic bombs and worms, but it is
important to note that viral programs have common characteristics that
other damaging or security breaking programs may lack.  Viri are not just
any damaging program.

Indeed, viral programs are not always damaging, at least not in the sense
of being deliberately designed to erase data or disrupt operations.  Most
viral programs seem to have designed to be a kind of electronic graffiti:
intended to make the writer's mark in the world, if not his or her name.
In some cases a name is displayed, on occasion an address, phone number,
company name or political party (and in one case, a ham radio license
number.)

On the other hand, viral programs cannot be considered a joke.  Often they
may have been written as a prank, but even those which have been written so
as not to do any damage have had bugs, in common with any poorly written
program.  The author of Stoned obviously knew nothing of high density
floppies or RLL drive specifications.  In fact, it appears that the
trashing of data by the Ogre/Disk Killer virus, one of the most damaging,
was originally intended to be reversible, were it not for an error on the
part of the programmer.  Any program which makes changes to the computer
system that are unknown to the operator can cause trouble, the more so when
they are designed to keep spreading those changes to more and more systems.

However, it is going to far to say, as some have, that the very existence
of viral programs, and the fact that both viral strains and numbers of
individual infections are growing, means that computers are finished.  At
the present time, the general public is not well informed about the virus
threat, and so more copies of viri are being produced than are being
destroyed.  As people become aware of the danger, this will change.

If we stick to a strictly "Cohenesque" definition of viral programs as only
those which attach to specific programs, then there are some difficulties
with defining other, similar, programs which reproduce themselves, but
without being linked to a specific program.

Unfortunately, although attempts have been made to address this issue,
there is, as yet, little agreement as to the terminology.

In early multi-tasking operating systems, programs often "broke the
bounds", and would overwrite sections of other programs or data.  Since
this damage was generally random, the pattern of damage, when mapped, gave
the appearance of twisting tracks which appeared and disappeared.  This
closely resembled the patterns seen when cutting through a piece of worm
eaten wood, giving rise to the term "worm" for such rogue programs.  One
such program escaped not only from its own partition within the computer,
but actually escaped from the original computer to another over an early
computer networking system.  The term "worm" has therefore come to be used
to refer to viral programs which do not attach to specific programs, and,
more specifically, to those which use network communications as a vehicle
for spreading and reproduction.

Two examples of this usage are the famous Morris/Internet/UNIX worm of late
1988, and the lesser known CHRISTMA EXEC mail worm of December 1987.

This still leaves a class of viral programs which do not attach
specifically to programs.  There are actually many sub-groupings within
this group, and there are within viral programs generally.  However,
European researchers, particularly those from France, often refer to such
programs as "bacteria", rather than viri.

In these areas of terminology there is often much debate about whether a
given virus, or type of viral program, fits into a given class.  Boot
sector infectors, for example, would not appear to fit the definition of a
virus as infecting another program, since BSI's can be spread by disks
which do not contain any program files.  However, the boot sector of a
normal disk, whether or not it is a "system" or bootable disk, always does
contain a program (even if it only states that the disk is not bootable),
and so it can be said that a BSI is a "true" virus.

Two other groups of security breaking programs are very often confused with
viri.  The first is the "trojan horse", the second the "logic bomb."  The
confusion is understandable, as viral type programs, trojan horses and
logic bombs make up the three largest distinct groups of security breaking
software, and often one may "contain" the code of one another.

A trojan horse is a program which pretends to do one thing, while
performing another, unwanted action.  The extent of the "pretence" may vary
greatly.  Many of the early PC trojans relied merely on the filename and a
description on a bulletin board.  "Login" trojans, popular among university
student mainframe users, will mimic the screen display and prompts of the
normal login program, and may, in fact, pass the username and password
along to the valid login program, as well as stealing it.  Some trojans may
contain actual code which does what it is supposed to be doing, while
performing additional nasty acts that it does not tell you about.  (I make
the distinction that trojans are always malicious, as opposed to "joke" or
"prank" programs.)

(A recent example of a trojan is the "AIDS Information Disk", often
incorrectly identified in both the general and computer trade press as a
virus.  Not to be confused with the, fairly rare, AIDS I and II viri, this
program appears to have been part of a well organized extortion attempt.
The "evaluation disks" were shipped to medical organizations in England and
Europe, with covers, documentation and license agreements just like any
real commercial product.  When installed and run, it did give information
and an evaluation of the subject's risk of getting AIDS, but it also
modified the boot sequence so that after 90 reboots of the computer all
files on the disk were encrypted.  The user was informed that, in order to
get the decryption key, a "license fee" had to be paid.)

Trojan horse programs are sometimes referred to as an "Arf, arf" or
"Gotcha" program from the screen messages of one of the first examples.  A
trojan horse may be used to plant a virus simply by infecting any existing
program.

A logic bomb is a malicious program which is triggered by a certain event
or situation.  Logic bomb code may be part of a regular program, or set of
programs, and not activate when first run, thus having some of the features
of a trojan.  The trigger can be any event that can be detected by
software, such as a date, username, CPU id, account name, or the presence
or absence of a certain file.  Viral programs and trojans may contain logic
bombs.

------------------------------

Date: Sun Jan 24 09:37:49 EST 1993
From: mis@seiden.com (Mark Seiden )
Subject: File 4--Le Pheacking americain vu du cote francais
Copyright: Agence France Presse, 1992

  Eco. TIE. ind. -  USA/piratage telephonique: un prejudice croissant
pour les entreprises- par Souk CHANTHALANGSY- WASHINGTON, 7 nov  92
(600 MOTS)
  Devenu une affaire lucrative et presque sans risque, le piratage
telephonique atteint des proportions inquietantes aux Etats-Unis ou,
selon les experts, il fait perdre chaque annee des centaines de
millions de dollars aux entreprises americaines.
  Ce type de fraude est d'autant plus redoutable qu'il n'existe, a
l'heure actuelle, aucune parade d'une efficacite absolue contre ces
pratiques qui font appel a des techniques informatiques
sophistiquees, selon un groupe d'experts reuni recemment sur ce sujet
a Washington par la Commission federale sur les communications (FCC).
  Ainsi, entre 1989 et 1992, plus de 550 cas de piratage
telephonique ont ete recenses au sein des quelque 700 entreprises
formant l'Association internationale des communications (ICA). Le
prejudice a ete evalue a 73,5 millions de dollars representant en
grande partie des appels internationaux, soit une moyenne de plus de
130.000 dollars pour chaque fraude.
  La quasi-totalite de ces piratages a ete menee par intrusion dans
les standards informatises ("private branch exchange", PBX) et/ou
dans les boites aux lettres vocales ("voice mail") des societes
piratees. Un autre type de fraude consiste a utiliser des cartes de
credit telephoniques dont le code secret a ete prealablement "casse".
"Ces pratiques constituent un exemple flagrant de la fragilite de
l'industrie des telecommunications", a estime le vice-president de
l'ICA, Lawrence Gessini.
  Les exemples de fraudes, evoques devant la FCC, sont nombreux. En
juin, une banque du groupe financier americain Leucadia Financial
Corporation a vu le nombre d'appels sur son numero gratuit passer
d'une centaine a plus de dix mille. La fraude n'a ete decouverte
qu'apres reception de la facture de la compagnie ATT: 250.036 dollars
pour ce seul mois, dont plus de 215.000 pour des appels en Republique
dominicaine.
  Selon un responsable du groupe bancaire Thomas Mara, les pirates
sont entres avec un ordinateur dans la ligne telephonique gratuite de
l'etablissement. En forcant le systeme de boite aux lettres vocales
et en essayant toutes les combinaisons d'acces a partir du zero, ils
ont trouve le mot de passe actionnant la ligne.
    D'apres William Cook, un ancien procureur de Chicago devenu
specialiste de la lutte contre les fraudes informatiques, des
centaines d'intrusion dans les standard telephoniques sont tentees
chaque jour aux Etats-Unis. En 1989, au cours d'un week-end prolonge,
une entreprise avait ainsi perdu 1,4 million de dollars, ses lignes
ayant ete "piratees" par des inconnus.
   Les equipementiers en materiels telephoniques accusent
generalement les victimes de n'avoir pas su proteger leur systeme
PBX, les victimes font grief aux equipementiers de ne pas les avoir
prevenues de la vulnerabilite du systeme et les deux se tournent vers
les compagnies de telephone. Entre 25 et 30 % du prejudice est
generalement assume par les compagnies de telephone.
      Une unite d'action est necessaire, selon les experts, pour
lutter contre ce piratage qui se traduit chaque annee par "des
milliards de dollars de manque a gagner pour l'economie" et qui
"menace la solvabilite de plusieurs centaines de petites entreprises
du pays", affirme M. Cook qui preconise un renforcement des sanctions
afin de dissuader les malfaiteurs.
   "Les risques sont si peu importants et les condamnations si
faibles que des trafiquants de drogue quittent le secteur des
stupefiants pour se lancer dans la fraude telephonique", a-t-il
encore indique.
   sc/rok/mpf

------------------------------

Date: Wed Jan 20 21:31:33 PST 1993
From: eggert@twinsun.com (Paul Eggert )
Subject: File 5--Horloge en panne, pourquoi?

A stopped clock never foils?

One way to discourage intruders from using covert channels to foil security is
to turn off the system clock, or at least to hide it from users.  But this
breaks a lot of software, so it's too drastic for all but the most
security-conscious sites.  So I was surprised to see J.-B. Condat's letter in
RISKS 14.28, which began:

  Date: 31 Dec 69 23:59:59 GMT
  From: jbcondat@attmail.com
  Subject: New E-journal on computer security
  [...]

Unix cognoscenti will recognize that date: it corresponds to the internal Unix
time value of -1, which is returned by system functions when the clock is not
available.  I guess Condat and the Chaos Computer Club France must really be
practicing what they preach!

+++++++

Date: Wed Jan  6 06:47:56 CST 1993
From: HART@vmd.cso.uiuc.edu ("Michael S. Hart" )

On 31 Dec 69 23:59:59 GMT you said:

Your message took about a week to get here. . .and thus was very hard to find,
as my mailer sorts by date, and yours was at the bottom.

Therefore I would suggest you preface your subject lines with !!! to make them
easier for me to spot.

Thank you for your interest,

Michael S. Hart, Professor of Electronic Text
Executive Director of Project Gutenberg Etext
Illinois Benedictine College, Lisle, IL 60532
No official connection to U of Illinois--UIUC
hart @uiucvmd.bitnet or hart@vmd.cso.uiuc.edu

+++++++

Date: Sat Jan 23 14:14:46 PST 1993
From: levene@aplpy.jhuapl.edu (Robert A. Levene )

----
> my message are date-stamped with the same time :-)
----

Please let him know that in English, this is known as a
"mistake which needs explaining" or a "lie."

------------------------------

Date: Sat Dec 26 12:31:11 GMT 1992
From: uldis@inkomi.riga.lv (Uldis Bojars )
Subject: File 6--Jeune Lettonien a la recherche de correspondants

To: Jean-Bernard CONDAT
    Chaos Computer Club France [CCCF]


    Hello !

 Harry Bush published some information about CCCF in our local
 echo.  I read it and decided to write You !

 At first let me wish You a Merry Christmas and happy New Year.


 I'm latvian teenager (i'm 18) and am studying computer sciences
 now.  My name is Uldis Bojars.  My interests lay mainly in
 programming and hacking field.  I'm interesting about phreaking,
 too, but have never built bluebox or somewhat like that. Favorite
 programming language - C, but i wanna program in Asembler, too.

 It would be nice to have a penfriend in France.  I'll be happy
 to discuss about life, programming, and much more.  I can't
 write French because I'm learning it about 2 months only.  But
 i think i will be able to understand French text...

 I'll try to call Your BBS to get to know more about CCCF.
 And - is it possible to get book You mentioned in letter
 - '_C'est decide! J'ecris mon virus' ?
 I'm not writing viruses, but i haven't got any literature
 about computers in French.

 I'll be happy to receive letters from CCCF.     /\
                                                /  \
 Sincerely Yours,                               /  \
              Uldis                            /    \
                                              /-    -\
                     Bon et heureux Noel !       ||
----------------------------------------------------------------------
About myself:  Uldis Bojars
               18 years old
               Email: uldis@inkomi.riga.lv
               i like science fiction, music (especially Beatles)
               i'm runing my own BBS

------------------------------

End of Chaos Digest #1.05
************************************

Downloaded From P-80 International Information Systems 304-744-2253
