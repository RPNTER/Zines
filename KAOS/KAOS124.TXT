Chaos Digest             Mercredi 12 Mai 1993        Volume 1 : Numero 24
                          ISSN  1244-4901

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.24 (12 Mai 1993)
File 1--Systeme dissuasif de marquage antivol (produit)
File 2--Ass. espagnole contre le crime informatique (loi)
File 3--Raid du FBI... sans aucune raison (etranger)
File 4--A quoi sert le 3644? (technique)
File 5--The Legion of Doom: le retour (droit de reponse)
File 6--Scanning de masse des telex anglais sortants (espionnage)

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.  He is a member of the EICAR and EFF (#1299)
groups.

Issues of ChaosD can also be found on some French BBS.  Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.53] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Tue May 11 09:24:40 PDT 1993
From: vol_protect@altern.com (vol_protect )
Subject: File 1--Systeme dissuasif de marquage antivol (produit)


   VOL PROTECT propose un systeme competitif de marquage des ordinateurs

     VOL PROTECT propose un systeme de protection contre le vol des
ordinateurs, a base de marquage antivol brevete (le 30 Aout 1991 a l' INPI
sous les references 9110769-71: "Dispositif de dissuasion de vol pour marquer
de maniere indelebile/inviolable et identifier un materiel/des cartes
electroniques") du materiel informatique et bureautique.

     Les marquages antivols sont personnalisables (texte au choix, logo et
couleurs de la societe).  Livre pret a poser, le marquage peut s'accompagner
d'un code barre qui facilite l'inventaire du parc.

     Arracher le marquage antivol provoque d'inportants degats sur la
machine en laissant des traces evidentes d'effraction.

     "Cela vaut la peine, lorsque l'on sait que le taux de vols dans les
entreprises dites "grands comptes" est de 2% par an, et qu'entre 1986 et
1988, 17,8% des entreprises ont ete victimes d'un vol de materiel."  Et
M. Jean-Noel Clot de citer les estimations du Centre de documentation et
d'informations des Assurances (CDIA): 110 MF de vols de materiels en 1991,
dont les 3/4 sont revendus: le vol represente 26,3% des crimes informatiques.
La repartition des vols par secteur economique est la suivante: services
47%, industrie 26,3%, finance 21%, non indique 5,3%.

     Le vol d'un ordinateur implique egalement en plus du materiel, le cout
de la reconstitution des donnees (voire du ou des logiciels, ainsi que
l'augmentation des primes d'assurances.

     Avec son systeme de marquage, Vol Protect offre gratuitement son logiciel
de gestion de parc micro, VP Parc, en saisissant pour le client, le catalogue
des appareils marques (fichier au format ASCII ou .DBF).

     De plus, un numero vert (05 17 33 02) est present sur chaque marquage,
afin que toute personne puisse verifier aupres de Vol Protect si votre
materiel a ete vole ou non.

     Garantie

     Vol Protect rembourse le marquage en cas de vol et les caracteristiques
permettant l'identification de la machine sont communiques aux organismes
tels que les constructeurs, les SAV, la police, etc.

     Contact

     Philippe Hassler, Ingenieur Commercial, Vol Protect, 101 Av. du General
Leclerc, 75014 Paris, France.
     Tel: +33 1 46 67 94 00
     Fax: +33 1 46 67 94 70

------------------------------

Date: Sat May  8 00:56:35 GMT 1993
From: gallardo@batman.fi.upm.es ("(Miguel Gallardo)" )
Subject: File 2--Ass. espagnole contre le crime informatique (loi)


                    Presentation of APEDANICA

During 1991 and 1992,  many things happened in Spain related with
computer risks.  Some of them went to the Court,  and many others
remain   in  an  unhealthy  silence.   Data  stolen  from  banks,
cryptology  used by  terrorist  organizations,  hacking,  piracy,
personal  dossiers  and  blackmailing have been  studied  by  the
police, lawyers, journalists and professional technicians.

Moreover,  a  deep  crisis  in Spanish economy does not  help  to
recover  any investment in data processing.  There are  too  many
unpaid  bills  and half performed projects in computing.  At  the
same  time,  politicians at the Parliament approved a new Law  on
Data Protection,  and a Data Protection Agency, a Computer Police
that is not clear enough who can control and how can it work.

Computer  victimization  is very high in Spain due  to  knowledge
lack and technical dependency from equipment and service sellers.
In  an increasingly complex and critical environments,  there  is
almost no local technology industry,  and multinationals are very
disconcerted  because  lack of  expertise,  expensive  commercial
nets,   counter-productive  promotional  efforts,  and  political
corruption on almost every local big business.

Since December 1992, there is an Association, APEDANICA, that can
help  to  discover sensible troubles related with  computers  and
communications,  and its markets.  Members of this non-profitable
organization acts like expert witness, cryptologist, lawyers, and
even as Sherlock Holmes in computer environments.

APEDANICA  (ASOCIACION  PARA LA PREVENCION Y ESTUDIO  DE  DELITOS
ABUSOS Y NEGLIGENCIAS EN INFORMATICA Y COMUNICACIONES AVANZADAS),
Spanish   Legal  Advanced  Communications  and   Computer   Crime
Association,  is very interested in developing relationships with
any other organization with similar goals, all over the World.


      _ _ _               _  Miguel A. Gallardo Ortiz, PX86 Engineer
     ' ) ) )             //  UNIX&C instructor working on RSA crypto
      / / / o __     _  //   P.O. Box 17083 - E-28080 Madrid (Spain)
     / ' (_<_(_//_/_</_</_   Tel: (341) 474 38 09 - FAX: 473 81 97
             _/              E-mail: gallardo@batman.fi.upm.es

President of APEDANICA-Spanish Legal Computer Crime Research Association

------------------------------

Date: Sun, 9 May 1993 16:34:53 GMT
From: jsaker@cwis.unomaha.edu (James R. Saker Jr. )
Subject: File 3--Raid du FBI... sans aucune raison (etranger)
Repost from: TELECOM Digest (special issue)

"FBI Probe, Raid Anger Curtis Man"
Stephen Buttry, {Omaha World Herald}, Sunday May 9, 1993

Curtis Neb. -- The evening was winding down for the Cole family. Ed Cole,
general manager of the Curtis Telephone Co., had dozed off on the living
room couch. His wife, Carol, was running water for her bath. The 10-year-old
identical twins, Stephanie and Jennifer, had gone to bed. Amanda, 14, was
watching "48 Hours" on television in the living room.

   "It had something to do with fingerprints and catching criminals," Amanda
remembers of the TV show.

   At 9:40 p.m., Amanda heard a knock and answered the door. In marched the
FBI. Thus began a year of fear, anger and uncertainty for the Coles.

   Mrs. Cole, 40, still has nightmares about the night of May 13, 1992, when
federal agents stormed into her bedroom, startling her as she was undressing
for her bath, naked from the waist up.

   "I used to go to bed and sleep the whole night," she said last week. "I
can't anymore."

   Federal agents did not find the illegal wiretapping equipment they were
seeking, and a year later no one has been charged. The agents siezed nothing
from the house and later returned the cassette tapes they took from the phone
company office.

   Ronald Rawalt, the FBI agent in North Platte who headed the investigation
that led to the raid, declined to comment, referring questions to the Omaha
office.

   "It's still a pending investigation, and we're not allowed to make a
statement," said agent Doug Hokenstad of the FBI's Omaha office. If the
investigation comes up empty, he said "we normally don't make a statement at
the end of the investigation."

   That infuriates Cole, 39, who says the raid cast suspicion on him and the
phone company and left them with no way to clear their names.

   "Either file charges or say there's nothing there," he said.  "This was
done in a highly visible manner, and there was no finality to it."

                              Request for Help

   Cole has asked Sen. Bob Kerrey, D-Neb., to investigate. Beth Gonzales,
Kerrey's press secretary, said the senator received Cole's letter and is
assessing the situation.

   The case that brought FBI agents from Washington, Denver, Houston and
Omaha, as well as nearby North Platte, to this tiny southwest Nebraska town
in the Medicine Creek valley apparently started with a personnel squabble in
the phone company office.

   Cole said two women complained of their treatment by two other workers.
The women who complained threatened to quit if the company did not take
action against the other women, he said.

   Cole and his assistant manager, Steve Cole, who is not related, observed
the office workers for a while.

   "We found the same two making the ultimatum were the aggressors," Ed Cole
said.

   He gave the complaining employees written reprimands, and they quit Jan 16,
1992. The two women contended in a hearing concerning state unemployment
benefits that personality differences with Ed Cole led to the reprimands and
their resignations.

   Both women declined to comment on the matter.

                                   300-Hertz Tone

   In an affidavit filed to obtain the search warrants, agent Rawalt said
one of the two, Carol Zak, contacted the FBI in March 1992 and told them of
"unusual electronic noises (tapping noises) on her telephone line at the
inception of a call received."

   Later in the affidavit, the noise is described not as tapping, but as a
300-hertz tone. Steve and Ed Cole demonstrated the tone last week on phone
company equipment.

   It is caused, they said, by a defective 5-by-7 circuit board, or card. The
defect is common, and the company replaces the card if a customer complains.

   The tone is not heard if a customer answers between rings, but if the
customer answers during a ring, the tone blares into the earpiece for an
instant, about the duration of the ring. Ed Cole, who has placed wiretaps for
law officers with warrents, said wiretaps don't cause such a sound.

   "Most wiretaps, don't they have a loud, blasting noise to announce there's
an illegal wiretap?" he asked sarcastically.

                                  Surveillance

   After Mrs. Zak told agent Rawalt of the noise on her line, the FBI began
recording her calls, the affidavit says. On April 30, the affidavit says, the
FBI began surveillance of Ed Cole -- not an easy task in a town of 791 people.

   During the weeks before the raid, phone company employees noticed a
stranger watching the office and workers' houses. They guessed that a private
investigator was watching, possibly gathering information for the former
workers.

   "When somebody sits around in a car in a small-town Curtis, especially at
3:30 when grade school lets out, people take notice," Steve Cole said. "We
had a suspicion that we were under surveilance."

   The affidavit says agent Robert Howan, an electrical engineer from FBI
headquarters, analyzed tapes of Mrs. Zak's phone calls and concluded that
a wiretap on the line "is controlled from the residence of Eddie Cole Jr.
and is facilitited through a device or computer program at the Curtis
Telephone Company."

   Based on Rawalt's affidavit, U.S. Magistrate Kathleen Jaudzemis in Omaha
issued warrents to search Cole's house and company offices. Federal agents
gathered in North Platte and headed south to Curtis for the late-evening
raid.

                            Flashlights, Commotion

   When Amanda Cole opened the door, she said "The first people that came
in went past me." They rushed through the living room into the kitchen to
let more agents in the back door.

   The agents wore black jackets and raincoats, with large, yellow letters
proclaiming "FBI." Neighbors and passersby began to notice the commotion
as other agents searched the outside with flashlights.

   The agents showed Cole the search warrant and told him and Amanda to
stay in the living room. The agents asked where the other girls were, and
Cole replied that it was a school night and they were in bed.

   Rather than flipping the hall light switch, the agents went down the
darkened hall with flashlights, "like they think my kids are going to jump
up and shoot them," Cole said.

   The twins recalled that they were puzzled, then scared, to wake up as
FBI agents shined flashlights on them. The intruders did not enter gently,
either.

   "After they left, our doorknob was broken," Jennifer said.

   Farther down the hall, the agents found the embarrassed and angry Mrs.
Cole. "They didn't knock or anything, and I was undressing," she said. "They
told me to get a T-shirt on."

   After Mrs. Cole put her clothes back on, agents allowed her to go with
them to get the frightened twins out of bed. Mrs. Cole and the twins also
were instructed to stay in the living room.

                               Interrogation

   As agents searched the house, Cole said, Rawalt told him to step out on
the porch. While he was outside, Mrs. Cole decided to call the phone
company's attorney.

   "They told me I couldn't do that," she said. "I worked at the Sheriff's
Office for several years, and I know no matter what you're accused of, you're
entitled to an attorney." She called anyway.

   Meanwhile, according to Cole, Rawalt was interrogating and berating him
loudly on the front porch, creating what Cole considered a "public spectacle."

   "I've lived here 15 years. I've built up a reputation," said Cole, who is
president of the Curtis Housing Authority, chairman of the Nebraska Telephone
Association, and coach of the twins' softball team. "And there's cars going
by real slow. Here Rawalt brings me out on the front porch, turn on the light
for everyone to see and starts interrogating me."

   Cole said Rawalt tried to pressure him to admit he was wiretapping and
tell him where the equipment was. "He pointed at my wife and kids and said,
'Look at what you're putting them through,'" Cole said.

                               Three-Hour Search

   Cole said it would take about 20 minutes for an expert to examine the
phones in the house -- a teen line, the main line plus two extensions, a
24-hour repair prone that rings at his home as well as the main office, and
an alarm that rings in from the central office.

   "The search continued for more than three hours, as agents looked in
closets, cabinets and drawers. The family could hear Garth Brooks singing as
agents played the children's tapes, apparently hunting for recorded phone
conversations.

   At the same time the Coles' house was being searched, agents visited Steve
Cole and Roger Bryant, a phone company employee who is a neighbor of Mrs.
Zak's.

   "They insinuated I had broken into my neighbor's house to put in a
wiretap," he said. The agents "asked me if I knew if Ed was making electrical
devices in his basement."

   (Cole said he wasn't. Agents found no such devices.)

   The agents told Steve Cole to take them to the phone company office
so they could search the switch room.

                                Number of Agents

   The Coles were not sure how many agents participated in the raid.  They
saw at least five at the house but thought they heard others outside and
entering the back door and going into the basement. They said seven agents
were at the office, but they weren't sure which agents searched both sites.

   When the agents said they were looking for wiretap equipment, Steve Cole
said "I told them it just couldn't be right. If Ed were to do something or I
were to do something, the other one would know."

   Steve Cole said agents searching the phone company, including Howan, did
not appear to understand the equipment very well. They would not tell him
why they suspected a wiretap.

   After 1 a.m., Ed Cole said, the search of his house ended, with agents
empty-handed and taking him to the office.

   About 4 a.m., the agents told Steve Cole about the 300-hertz tone.  "The
minute they told me, I knew what it was," he said. He said he quickly found
the defective card for Mrs. Zak's line, demonstrated the sound for the
agents, then replaced it and showed that the sound was gone.

   "I demonstrated it, and then they both got white," Steve Cole said.

                               Card Analyzed

   Howan then went to Rawalt, who was with Ed Cole outside the switch room
and explained what had caused the tone, Ed and Steve Cole said.

   "I'm jubilant," Ed Cole recalled thinking. "I've been exonerated."  But
he said Rawalt told him: "I've investigated this for two months.  I've flown
agents in from around the country ... I may charge you on circumstantial
evidence."

   "My heart just sunk," Cole said, "because that means they're not here to
find the truth. They're just trying to support their pre-conceived ideas."

   He said Rawalt told him he would take the card for analysis.

   Cole said the searches could have, and should have, been conducted without
the embarrassing fanfare -- during normal business hours, while the children
were in school and his wife was at work.

   Because of the highly public nature of the raid, Cole said, the company
has hired a lawyer to investigate the investigation. The company is trying,
with little success, Cole said, to get information from the FBI so it can
reassure regulators, lenders, stockholders and customers of the company's
integrity.

                                Tapes of Calls

   Rawalt visited the Cole's house again in January. Although this time it
wasn't a raid, his presence upset the family. He returned tapes siezed in
the raid but told Cole that the circuit card was stilll at the FBI lab being
analyzed. It still has not been returned, Cole said.

   "The FBI, the most respected law enforcement agency in the world, has had
this card in their laboratory in Washington, D.C., for almost one year, and
they still cannot determine if it has a tape recorder strapped to it," Cole
said.

   The bureau also has refused to give the phone company of its tapes of Mrs.
Zak's phone calls, which could show whether the sound on her line was the
tone from the defective card, Cole said.

   "It makes one wonder if they'd put a family and a company through this
just because they don't want to admit a mistake," he said. "If they'll just
give me my life back by making a public statement, it would be over."

 Jamie Saker                    jsaker@cwis.unomaha.edu
 Systems Engineer               Business/MIS Major
 Telenational Communications    Univ. Nebraska at Omaha
 (402) 392-7548

+++++
TELECOM Digest Moderator's Note:

Thank you very much for sending along this report.
This is just another example of the clumsy, oafish and unprofessional
organization which has become such a big joke in recent years in the
USA: The Federal Bureau of Inquisition. Imagine: a telephone line out
of order which turns into a massive FBI assault on a private family.
And of course there will be no apology; no reparations; nothing like
that. The FBI is too arrogant and powerful to bother with making
amends for the damage they have done. I hope Ed Cole and his telco
demand and obtain revenge on everyone concerned, including first and
foremost Mrs. Zak, the scorned woman who set the whole thing in motion
when she got fired for her bad attitude at work.  I know if it was
myself, I would not be content until I had turned the screws very hard
on all of them, especially her.  --PAT

------------------------------

Date: Sun, 9 May 1993 16:34:53 GMT
From: ymcrabbe@altern.com (Yves-Marie CRABBE )
Subject: File 4--A quoi sert le 3644? (technique)


Le DERAL (presente dans ChaosD #1.21.1) est un automate de France Telecom
servant aux techniciens pour  appeler le robot testeur du MT25 local.  Pour le
mettre en oeuvre, il suffit de suivre la procedure suivante:

1. Decrocher votre combine et attendre la tonalite;

2. Composer le 3644, attendre la tonalite hachee. Vous pouvez entendre de
   temps en temps le message suivant: "Vous etes connectes sur [un type de
   PABX], nous procedons au test [un code/libelle France Telecom]".

   Une tonalite hachee se fait entendre apres ce message.

3. Raccrocher et attendre dix secondes;

4. Decrocher a nouveau, vous pouvez entendre:
   * une tonalite grave continue --> bon isolement de la ligne.
   * une tonalite hachee lente   --> fuite a la terre.
   * une tonalite hachee rapide  --> fuite entre fils de ligne.

   Voila le test d'isolement termine.

5. Raccrocher et attendre la sonnerie.

6. Decrocher alors, vous pouvez entendre:
   * une tonalite grave continue --> 33 < I < 50 mA (normal).
   * une tonalite hachee lente   --> I < 33 mA (trop faible).
   * une tonalite hachee rapide  --> 50 < I < 70 mA (trop forte).
   * une tonalite irreguliere    --> I > 70 mA (beaucoup trop forte).

7. Raccrocher pour terminer le test.

------------------------------

Date: 13 May 1993 21:55:46 -0400
From: todd@hal.gnu.ai.mit.edu (The Marauder)
Subject: File 5--The Legion of Doom: le retour (droit de reponse)
Repost from: telecom13.327.7@eecs.nwu.edu


Let me set the record straight:

This "NEW" Legion of Doom, coming from "tdc@zooid.guild.org" has _NOTHING_
whatsoever to do with the Legion of Doom! group that was formed approximately
mid-1984, of which I was a member. The "real" LoD continued as a group until
somewhere around 1990.  Those of you really interested in the whole thing can
read all about it in the electronic publication called "Phrack", which is
available at the anon ftp site "ftp.eff.org", in the "/pub/cud/phrack"
directory.

I believe "Phrack" issue #31 contains "The History of The Legion of Doom!"
which was written by Lex Luthor (founder of the whole thing), and edited by
Erik Bloodaxe. The article contains a brief history of us, and ALL them
members of the real group, and is the final word as to who was/was not in
LoD. I think you will find no mention of this (ahem) Lord Havoc character.
I believe "ftp.eff.org" also contains all the LOD Technical Journals in
"pub/cud/lod". The Legion of Doom!  as a hack/phreak group DOES NOT EXIST
ANYMORE.  These clowns running around the internet calling themselves the
"NEW" LoD are simply some all the LOD Technical Journals in "pub/cud/lod".
The Legion of Doom!  as a hack/phreak group DOES NOT EXIST ANYMORE. These
clowns running around the internet calling themselves the "NEW" LoD are
simply some kids having fun with you all, so relax, take a deep breath, and
forget the whole thing. I am quite convinced you'll not hear much more from
them ;).

Most of the horror stories, and tales of terror you have read and heard about
us (real LOD), are way off base. Very few of you were around, or involved
with the "BBS" underground world back when we existed as a group so any "data"
you have about us is heresay at best.  (Although I'm sure you guys at AT&T
could probably find some fairly accurate information in "Ralph's" files,
heh ;) ). Anyway, speaking for me, I simply became obsessed with the telephone
system; it is after all the largest interconnected entity I know of. The last
thing I or any of the members of the LOD wanted to do is wreck or destroy the
very thing that caused us to come into existence in the first place. Sure we
looked at a few things we damn well had no business seeing, and yes we
occasionally impersonated the Arlington RNOC for WATS translations and what
have you. But as to being the wandering band of "Digital Henchmen" who left
smoking, crumpled 1AESS's in our wake -- you could not be further from the
truth!  I often laugh out loud at the media's portrayal of us and our
activities, as they are by far the most clueless of the lot.

Most of the original members of the LOD have remained friends, and stay in
fairly current contact with each other, sometimes swapping stories of our old
memories over a beer or two, when we have time.  That's about the extent of
it.

Forget about this "Return of the Legion of Doom!". Like a bad smell, it's
sure to blow away.

The Marauder   Legion of Doom!   Marauder@phantom.com

------------------------------

Date: Fri, 26 Feb 93 17:30:55
From: I_USERID_4@prime1.central-lancashire.ac.uk (James Faircliffe )
Subject: File 6--Scanning de masse des telex anglais sortants (espionnage)


A few months ago, a well-respected British TV documentary show (might have
been 'World in Action') discovered that all out-going telexes from the Uk were
electronically scanned by British Telecom (the main phone company) personnel,
supervised by the security services.  Direct scanning by the security services
would have been illegal.  They were looking for words like 'terrorist' &
'bomb', but the civil liberties implications are far-reaching.  Obviously,
this could affect the privacy of American telexes to the U.K.

J.F. Faircliffe.
i_userid_4@p1.uclan.ac.uk

[ChaosD: Etonne de cette information publiee dans "Computer Privacy Digest"
v2 #021, je decidai d'ecrire au President de BT.  Voici sa reponse:]

BT Centre Room A730 81 Newsgate Street LONDON EC1A 7AJ
Office of the Chairman

J B Condat Esq

                                          2 March 1993

Dear Mr Condat

Thank you for your facsimile message of 28 February.

I can confirm that BT would not be involved in the "scanning" of
outgoing telexes from the UK for security reasons.

I would not therefore be able to provide you with any information
on this matter.

                                          Yours sincerely

                                          /signed/
                                          DAVID BROWN
                                          Assistant to the Chairman

------------------------------

End of Chaos Digest #1.24
************************************

Downloaded From P-80 International Information Systems 304-744-2253
