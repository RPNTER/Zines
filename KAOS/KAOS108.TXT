Chaos Digest               Lundi 15 Fevrier 1993        Volume 1 : Numero 8

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.08 (15 Fev 1993)
File 1--Fausses Cartes Orange Pour le Metro Parisien
File 2--L'ordinateur de la RATP a piege les faussaires (reprint)
File 3--Congres International Russe, CSAM'93
File 4--Donnees Secretes sur les Reserves Cambiales Bresiliennes
File 5--Incident du Vol de Disquettes (lettre)
File 6--Reactions sur "The Little Black Book of Computer Virus"

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost from jbcondat@attmail.com. The editors may be
contacted by voice (+33 1 47874083), fax (+33 1 47877070) or S-mail at:
Jean-Bernard Condat, Chaos Computer Club France [CCCF], 47 rue des Rosiers,
93400 St-Ouen, France

Issues of Chaos-D can also be found on some French BBS. Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * ftp.eff.org (192.88.144.4) in /pub/cud
        * red.css.itd.umich.edu (141.211.182.91) in /cud
        * halcyon.com (192.135.191.2) in /pub/mirror/cud
        * ftp.ee.mu.oz.au (128.250.77.2) in /pub/text/CuD
        * nic.funet.fi (128.214.6.100) in /pub/doc/cud

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Fri Feb 12 11:08:54 CST 1993
From: ts%operations%france@bangate.compaq.com (Thierry Steinberg )
Subject: File 1--Fausses Cartes Orange Pour le Metro Parisien
Copyright: Agence France Presse, 1993


  Divers escroquerie -  Interpellation de "petits genies" de
l'informatique dans une escroquerie aux coupons de cartes oranges-
PARIS, 11 fev  93 (250 MOTS)

  David, Guillaume et Stephane, ages de 21, 22 et 23 ans, dont l'un
est un "petit genie" de l'informatique, ont ete interpelles, ces
derniers jours, en region parisienne, soupconnes d'avoir reencode,
grace a l'informatique, des coupons de cartes oranges afin de les
revendre, a-t-on appris jeudi de source policiere.

  Depuis quelques mois, selon les enqueteurs du Commissariat des
reseaux ferres parisiens (CRFP), ils revendaient, a la cadence de
cinquante par mois, ces coupons 250 francs piece, utilisables souvent
tres longtemps (la carte orange est generalement utilisee
mensuellement).

   Chez Stephane, etudiant en informatique, interpelle mardi soir, a
Vaureal (Val-d'Oise), les policiers ont notamment retrouve pres de
150.000 francs en especes provenant de cette escroquerie presumee, un
lot de materiel informatique dernier-cri, un scanner, de faux
coupons, 150 de ceux-ci vierges et un fusil.

      La RATP avait depose plainte, le 18 janvier, apres avoir
constate, par exemple, a la lecture des bandes informatiques
recensant les donnees de chaque ticket de carte orange des usagers de
la region parisienne, qu'un de ceux-ci , repondant au meme code,
avait ete utilise... 7.000 fois par jour, principalement a la gare
Saint-Lazare, aux tourniquets automatiques validant les billets et
permettant l'acces aux quais.

   Une quinzaine de personnes ont ensuite ete interpellees, ce qui a
permis de remonter aux trois "experts" presumes qui ont tous ete
deferes a la 8eme section du parquet de Paris.
   rb/jmg

------------------------------

Date: Fri Feb 12 18:30:04 GMT 1993
From: SBROWN@Kentvm.Kent.edu (Steven R Brown )
Subject: File 2--L'ordinateur de la RATP a piege les faussaires (reprint)
Copyright: Charles Desjardins, France Soir, 1993


         LES FAUSSES CARTES ORANGE ETAIENT PRESQUE PARFAITES

       Trois bidouilleurs rechargeaient des coupons pour 250 F

     "N'importe quelle officine informatique est en mesure de realiser
cette manip'",commente un specialiste des bidouillages electroniques en
tout genre. Deja poursuivi pour fraudes informatiques, il prefere garder
l'anonymat pour commenter le principe de cette "escroquerie a la carte
orange".

     "C'est un probleme de codage. J'ai deja vu realise une demonstration
assez proche sur les cartes bleues." Premiere etape: "identifier le code
utilise sur les tickets de la RATP".  Il est compose d'une serie de
caracteres alphanumeriques (chiffres, lettres ou symboles) enregistres
sur la bande magnetique situee au dos de chaque coupon.

     "Pour le lire, il existe des appareils type lecteur de badge de
securite." A ce stade, le but est d'obtenir la visualisation de la chaine
de caracteres sur un ecran. Mais la difficulte consiste a faire le tri
entre les differentes versions possibles.

     17 COMBINAISONS

     "Pour les codes-barres, par exemple, il en existe dix-sept
differents". Ils repondent chacun a des noms compliques comme le "Barcode-
WH39" ou le "code 39 simple".

     Le reve du fraudeur est bien sur d'obtenir ce renseignement
directement, par un complice travaillant dans l'entreprise, et de s'equiper
en consequence d'un lecteur de codes adapte. "Sinon, il faut bricoler, mais
cela reste possible", confie notre expert.

     Une fois la chaine de caracteres revelee, il ne reste plus qu'a la
reproduire sur d'autres tickets. C'est la seconde phase. "Il existe des
appareils d'encodage qui peuvent magnetiser la partie centrale du ticket et
y imprimer un nouveau code."

     Dans la fraude qui vient d'etre decouverte, les "bidouilleurs" ont
reproduit a l'identique (et a plusieurs milliers d'exemplaires) des vrais
coupons mensuels (de 1re ou de 2e classe et jusqu'a six zones), en
utilisant sans doute un scanner couleur, genre de photocopieuse tres
sophistiquee. De l'aveu meme des policiers, les copies etaient quasi
indecelables, de veritables "vrais-faux".

     Il leur suffisait ensuite d'imprimer au verso de ces tickets vierge la
bande magnetique supportant un code pirate. Celui-ci provenait d'un coupon
annuel, et comportait donc une identification valable douze mois
consecutifs.

     SUSPECT

     L'importance du trafic est a l'origine de sa decouverte. En effet, la
totalite des portillons automatiques de la RATP sont relies a un central
informatique qui enregistre le code du coupon a chaque passage. Ainsi,
lorsqu'un ticket deja usage ou suspect est "lu", il peut etre rejete, voire
absorbe par la machine.

     LA PREUVE

     Le 18 janvier, un controle a fait apparaitre que la meme
identification a ete donnee plusieurs milliers de fois dans la meme
journee. Comme si un seul et meme ticket avait ete utilise a plusieurs
points de passage en meme temps. Preuve evidente de l'escroquerie. L'erreur
des faussaires? Ne pas avoir change de combinaison.

[Moderateur: Le "specialiste des bidouillages electroniques en tous genres"
ou "l'expert" cite par ce journaliste n'est personne d'autre que... le
secretaire general du CCCF. L'anonymat sur son identite semble avoir ete
secretement tenu par le redacteur-en-chef de _France Soir_.]

------------------------------

Date: Fri Feb 12 15:20:42 MET 1993
From: dima@iamk4508.mathematik.uni-karlsruhe.de (Dimitrij Shiriaev )
Subject: File 3--Congres International Russe, CSAM'93


   INTERNATIONAL CONGRESS ON COMPUTER SYSTEMS AND APPLIED MATHEMATICS

                            19-23 JULY 1993

                         ST.PETERSBURG, RUSSIA

           Organized by:
           Center of Modern Communications
                         of St.Petersburg University,
           Russian Local ACM Chapter/St.Petersburg,
           St.Petersburg Education
           Computer Society "Micom-XXI"

THE AIMS

of the Congress are: provide a forum to explore common interests, interplay
across disciplines, and to bring researchers state of the art advances in
all areas of computer science, scientific computing, software engineering,
applied and computational mathematics. The official language of the
Congress is English, only papers submitted in English will be considered.

CALL FOR PAPERS

Papers and Minisymposia are invited in all areas of: Numerical Analysis;
Applied Probability and Statistics; Theory of Computing; Optimization and
Operations Research; Scientific Computation; Parallel Processing;
Programming Languages; Symbolic Computation; Supercomputing; CASE Tools;
Fuzzy Systems; Databases; Networks; Neural Nets; Artificial Intelligence;
Computer Graphics; Data Security; Simulation and Modelling; Mathematical
Education; Interval/Self-Validating Computations. Telecomminications.

CONTRIBUTED PAPERS/POSTER PRESENTATIONS

The program will also include contributed paper sessions (20-minutes
presentation), posters, and industrial exhibits. Authors are invited to
submit to the CSAM'93 Program Committee a one page abstract and indicate if
they prefer an oral or poster session. Authors may suggest the title(s) of
appropriate session(s) for their paper. Manuscripts of papers presented at
the Congress will be published as CSAM'93 Proceedings after the Congress. A
volume containing all abstracts of the accepted papers and description of
all minisymposia including titles and speakers known by May 1, 1993, will
be available for the participants at the Congress. Late papers and
sessions, if accepted, may be presented at the Congress and will be listed in
the Supplementum to the final program.

EXHIBITOR INFORMATION

Booths and tables will be available to companies wishing to display their
products and/or services.

MINISYMPOSIA/SECTIONS PROPOSAL

The Program Committee invites you, as a potential organizer, to submit a
proposal for a minisymposium (section). A minisymposium is a session of
several speakers focusing on a single topic. Minisymposium organizers are
responsible for the scientific quality of papers in their sessions,
consequently all papers invited by organizers are automatically accepted.

PROGRAM COMMITTEE

S.Baranoff (Russia), C.Brezinski (France), B.Christiansen (UK), D.Claudio
(Brazil), G.Corliss (USA), C.Evequoz (Canada), H.Fischer (Germany),
N.Holsti (Finland), D.Gay (USA), D.Grigoriev (USA), B.Kearfott (USA),
R.Klatte (Germany), K.Madsen (Denmark), S.Markov (Bulgaria), G.Menshikov
(Russia), M.Meyer (Germany), V.Nesterov (Russia), V.Shaidurov (Russia),
D.Shiriaev (Germany), S.Shirokov (Russia), S.Voitenko (Russia), W.Walster
(USA), W.Walter (Germany), J.Wolff von Gudenberg (Germany), A.Yakovlev
(Russia)

MINISYMPOSIA/SECTIONS ANNOUNCED

  Approximation - Claude Brezinski (France)
  Numerical Algorithms - Claude Brezinski (France)
  Computer System Security - Jean-Bernard Condat (France)
  Fortran 90 Programming Language - W.Brainerd (USA)
  Application Software for Macintosh - S.Shirokov (Russia)
  Interval/Self-Validation Computations - V.Nesterov (Russia)
  Automatic Differentiation - H.Fischer (Germany)
  Complexity in Symbolic Computations - D.Grigoriev (USA)
  Constraint Satisfaction Techniques and Constraint Logic Programming -
    M.Meyer (Germany)
  Performance Evaluation of Computer Communications - Claude Evequoz
    (Canada)
  Parallel Processing - Claude Evequoz (Canada)
  Mathematics of Modeling Biological Neurons - Arno Klaassen (France)

DEADLINES

Minisymposium proposals: As soon as possible;
Early submissions due: March 1, 1993;
Normal submissions due: May 1, 1993;
Late submissions: After May 1, 1993.

CONFERENCE LOCATION

Russia, St.Petersburg, Aerodromnaya 4, Education Center

REGISTRATION FEES

Their include admission to all minisymposia, sections and lectures, program
materials, admission to exhibits, refreshments, congress reception, and a
visit to the theatre or philharmonics.

Accompanying persons are welcome. Their fee includes the congress
reception, a visit to the theatre or philharmonics, a guided tour to Peter
and Paul Fortress and Cathedral and a cruise along the Neva river. An
Associates Program with visits to all the places of interest in
St.Petersburg will be offered for modest additional fees.

The student fee does not include the congress reception and the visit to
the theatre. Students must also enclose a statement from their university.

All participants will be provided with a card for all St.Petersburg public
transport (including underground) for the period of the Congress.

                         EARLY           NORMAL
Student                  $ 50            $ 60
Regular                  $ 190           $ 240
Associate                $ 75            $ 75

The Early fee applies to all fees received by April 1, 1993.
The Normal fee applies to all fees received after April 2, 1993.

Registration fees can be refunded only if cancellations are received by the
CSAM'93 Secretariat in writing. Refunds will be made as follows:
Cancellations received by April 1, 1993:
    Refund minus bank charges;
Cancellations received April 2, 1993 to June 1, 1993:
    Refund minus bank charges less $ 50 service charge.
No refunds can be made for cancellations received after June 1, 1993.
Substitution of a participant is possible any time.

VISAS

Please check with the Russian Embassy or Consulate to determine if you need
a visa. In the case you need it please use your registration receipt as the
visa support letter.


ACCOMMODATION

A large block of rooms is being held for CSAM'93 at the Congress Center
from July 17 to July 26. In case an overflow happens, blocks of rooms are
also being held at nearby hotels. We encourage you to book through the
CSAM'93 Secretariat. Participants who wish to reserve a room themselves
should bear in mind the difficulty in finding hotel rooms in St.Petersburg
during the summer.

The total price of your stay must be paid together with the registration
fees. In exchange you receive a lodging voucher with your registration
receipt.

Accommodation fees can be refunded only if cancellations are received by
the CSAM'93 Secretariat in writing. Refunds will be made as follows:
Cancellations received May 1, 1993
    Refund minus bank charges
Cancellations received May 2, 1993 to June 15 1993
    Refund minus bank charges minus $ 50 service charge.
Cancellations received after June 15 1993.
    Refund minus bank charges minus one night stay charge.

All bedrooms have private bath and/or shower. Rates include full breakfast,
tax and service charges.  The prices are $ 60 per night for a single room
or $ 40 per person per night in a double room.  The basic student hostel
accommodation: single room with no private bath or shower is offered at $20
per night for a person.

METHOD OF PAYMENT

Please pay the appropriate amount in US dollars by remittance on the
account:

   Deutsche Bank Karlsruhe, Germany
   Account No.: 0142018
   Bank Routing Code: 66070004
   Purpose: CSAM'93 Congress

We are sorry that we cannot accept credit cards or checks.  Payment must be
enclosed with the registration form.  Please do not forget to indicate
*Participant Name* and *CSAM'93 Congress* on all payments.

ABOUT ST-PETERSBURG

St-Petersburg (pop. 5 mil.), the historical capital of Russia, is one of
the major tourist attractions in the world. The city is situated on more
than 40 islands in the delta of the Neva River.  There are a lot of
museums, palaces and theaters to visit.  Among these are the Hermitage
museum with its 3 mil. exhibits, Winter Palace and magnificent summer
residences of Russian Tsars, the third largest cathedral in the world St-
Isaaks Cathedral and many others. The Associates Program of the Congress
will provide the opportunity to visit all places of interest in St-
Petersburg.

WEATHER

Since much of St.Petersburg fascination is historical, architectural and
cultural, it can be enjoyed at any time.  Typical July weather is bright
and pleasant.  Expect temperatures between 20 and 23 degrees C (67 - 72 F).

INFORMATION

 CSAM'93 Secretariat,
 Dr. Sergey S. Voitenko, Director,
 Center of Modern Communications
 of St-Petersburg University,
 Mail Box 835,
 Russia, 199178 St-Petersburg,
 Fax: +7 812 394-5004
 e-mail: csam93@polylog.spb.su

Abstracts can be sent in paper or electronic form.  Abstracts in paper form
must be sent to the CSAM'93 Secretariat.  Outside of the former USSR
abstracts in electronic form (Postscript or LATEX are welcome) must be sent
to:  Dimitri Shiriaev: dima@iamk4508.rz.uni-karlsruhe.de.

Please return this registration form by registered airmail to the CSAM'93
Secretariat (and a copy by E-mail) as soon as possible.  Please use one
form per person. Copy it if necessary.

----------------------------- cut here ----------------------------

                         REGISTRATION FORM

             Last name:
                  Name:
Title (Mrs,Mr,Dr,Prof):
           Affiliation:
               Address:

               Country:                      City:
                   ZIP:                      Tel:
                   FAX:                      E-mail: (Highly desirable!)


Fees
                      EARLY          NORMAL
Student               [ ]   $ 50      [ ]  $ 60
Academic              [ ]  $ 190      [ ] $ 240
Associate             [ ]   $ 75      [ ]  $ 75

Lodging facilities (please check)

                   July, 1993
[ ]17  [ ]18  [ ]19  [ ]20  [ ]21  [ ]22 [ ]23  [ ]24  [ ]25 [ ]26

[ ] single hotel room:   $  60     per day x  ___ days =
[ ] double hotel room:   $  80     per day x  ___ days =
[ ] triple hotel room:   $ 100     per day x  ___ days =

[ ] single student room: $  20     per day x  ___ days =

Total amount :

Date:                         Signature:

------------------------------

Date: Thu, 21 Jan 93 19:56 GMT
From: 0003965782@mcimail.com (Sanford Sherizen )
Subject: File 4--Donnees Secretes sur les Reserves Cambiales Bresiliennes
Repost from: Risks-Forum Digest


A Reuters report found in the NY Times (21 Jan 1993) states that computer
disks holding secret information on Brazil's banking reserves have disappe-
ared from the central bank.  The federal police are investigating the loss.
According to the report, President Itamar Franco "took the unusual step" of
releasing information on the reserves to offset any damage or financial
speculation from loss of the disks.  The disks held information on day-to-
day reserve operations and details like where the reserves are invested,
what they consisted of and how the reserves were generated.

COMMENTS

This disappearance may be related to ex-President Collar's involvement in
the looting of Brazil.  At a minimum, the data disappearance seems to be
another indication of the Post-Hacker Era, where governments and companies
have learned that computers can be used as an essential aspect of crime
and/or to cover up a crime.  The lines between "hacker" activities and
"legitimate" activities may become increasingly less clear.  In order to
almost have to use computer techniques.  While there continues to be an
(often unconscious) image that many have that computer crime is "bad
individuals" against "good" organizations, the Organization as Computer
Criminal is rapidly becoming a serious problem.  One but certainly not the
only instance of this is the recent British Airway's penetration of Virgin
Air's resercations system.

[Moderateur: Voici l'article du _NY Times_ dont il est fait question:

++++
Brazil data are missing
New York Times  PP: D, 20:4  Jan 21, 1993  ISSN: 0362-4331  JRNL CODE: NY
DOC TYPE: Newspaper article  LANGUAGE: English

ABSTRACT:  A  spokesman  for President Itamar Franco of Brazil said Jan 20,
1993  that  computer  disks  holding secret information on Brazil's banking
reserves  have  disappeared from the central bank. Police are investigating
the loss.

GEOGRAPHIC NAMES: Brazil
DESCRIPTORS: Bank reserves; Central banks; Government documents]

------------------------------

Date: Paris, le 8 Fevrier 1993
From: Ambassade du Bresil, Paris
Subject: File 5--Incident du Vol de Disquettes (lettre)


Ambassade du Bresil
34 Cours Albert Ier
75008 Paris


                                           Monsieur Jean-Bernard Condat
                                           CCCF
                                           47, rue des Rosiers
                                           93400 Saint-Ouen


     Monsieur,

     En reponse a votre lettre du 31 janvier, je vous confirme la publication,
par le quotidien bresilien "O GLOBO", d'un article relatif a un vol de
disquettes de la Banque Centrale qui contiendraient des donnees secretes sur
les reserves cambiales bresiliennes.

2.   En effet, l'Agence Brasil/RADIOBRAS, qio envoie chaque jour a l'Ambassade
du Bresil un resume des principales matieres publiees par les grands journaux
bresiliens, a transmis, le 20 janvier, un court resume de l'article ci-dessus.

3.   Je regrette de ne pouvoir vous envoyer copie du texte integral, etant
donne que l'Ambassade ne dispose plus de l'exemplaire du "GLOBO" dans lequel
il a ete publie.

4.   Vous trouverez ci-apres le nom et les numeros de telephone et de fax de
la correspondante du GLOBO a Paris, pour une eventuelle obtention deu texte
integral:

                          Mme Helena Celestino
                          Tel 42 79 80 78
                          Fax 43 22 66 12

5.   D'autre part, la revue VEJA du 26 Janvier, dans un article intitule "Le
citoyen commun", concernant un autre sujet, mentionne rapidement, a la fin de
l'article, l'incident du vol des disquettes.

6.   J'espere que ces renseignements seront utiles a votre projet.

     Veuillez agreer, Monsieur, mes salutations distinguees.


                                  Celina Assumpcao do Valle Pereira
                                         Ministre-Conseiller

------------------------------

Date: Sun Jan 10 13:00:58 -0500 1993
From: jbcondat@ATTMAIL.COM (Chaos Computer Club France )
Subject: File 6--Reactions sur "The Little Black Book of Computer Virus"
Copyright: Coastal Associates Publishing L.P., 1992


 Balancing  fears,  rights  in  wars  against  viruses; a little black book
     reveals  secrets  and  highlights  some  difficult  issues  facing the
     computer  industry.  (Mark Ludwig's 'The Little Black Book of Computer
     Viruses, Volume I') (Issues and Trends)(MacInTouch)
 Ford, Ric
 MacWEEK  VOL.: v6  ISSUE: n26  PAGINATION: p96(1)
 PUBLICATION DATE: July 13, 1992

  A little black book reveals secrets and highlights some difficult issues
facing the computer industry.

  Rights in conflict. Wars always raise difficult questions about the
limits of civil liberties, and the current war on computer viruses is no
exception.
     At the moment, anti-viral forces are battling to contain a breach in
the information curtain--a little black book from a virus researcher and
enthusiast that details step-by-step recipes for cooking up programs to
infect MS-DOS systems.

     Black book. The author, Mark Ludwig, is a physicist who became
interested in viruses and found it difficult to obtain detailed technical
information about them.

     "I find that the whole field is very secretive," Ludwig said. "There's
a closed group of people who have access to viruses and pass them around
among themselves."

     Unwilling to agree to their codes of secrecy, Ludwig struck out on his
own, setting up a bulletin board system and soliciting contributions of
viruses, which he analyzed.

     In Ludwig's introduction to "The Little Black Book of Computer
Viruses, Volume I," he explains his rationale: "I am convinced that
computer viruses are not evil and that programmers have a right to create
them, possess them and experiment with them."
     He warns repeatedly against irresponsible use of the virus recipes in
the book but also expresses concern about governmental abuse of power.

     "The book is a bad thing," said David Stang, chairman of the
International Computer Security Association. "We should have a law,
somehow, against it."

     He blamed an earlier book for spawning hundreds of viruses and said
defenders of such publications "have yet to show us an example of a good
virus."

     As Stang points out, Ludwig's book lacks anti-viral utilities, and it
offers little assistance to system administrators contending with real and
imagined virus threats.

     Other critics reportedly have called for bookstore boycotts, picketing
and similar protests against distribution of the book.

     The ICSA and the National Computer Security Association (which is open
to end users and organizations) analyze viruses and anti-viral programs,
primarily from DOS systems.
     Stang claims a collection of some 10,000 viruses. "I receive viruses
from anyone, but I don't trade," he said.

     The two groups distribute selected information through several public
and private channels, but current policies rule out public distribution of
sensitive materials, such as source code and viruses themselves.

     Cops and criminals. In the Macintosh community, the virus problem is
smaller and under better control than in the DOS world.

     An international "police force" shares viruses and information among
its members, many of whom are programmers of anti-viral utilities.

     This cooperative group releases bulletins to the public about new
viruses and assists in identifying and prosecuting virus writers. Its
identity and the identity of most of its members are secret.

     The group's low profile helps discourage macho challenges between
virus and anti-virus programmers, and its members avoid being overwhelmed
by thousands of false reports triggered by various other Mac bugs and
features.
     Individual responsibility. As standardization and networking grow, so
does the virus threat. Imagine a virus infecting a room full of Newton
Personal Digital Assistants sharing information in a meeting.

     Simultaneously, threats to individual computer users' rights also seem
to be on the rise. Consider the Secret Service's raid on game publisher
Steve Jackson or the implications of the FBI's new telephone-monitoring
proposal.

     In the end, responsibility for securing our systems lies ultimately in
our own hands. Free exchange of information and control over our own
systems are our most effective defensive weapons, and we should not
compromise them in a war that has never claimed a life.

------------------------------

End of Chaos Digest #1.08
************************************

Downloaded From P-80 International Information Systems 304-744-2253
