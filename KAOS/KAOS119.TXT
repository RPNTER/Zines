Chaos Digest               Lundi 19 Avril 1993          Volume 1 : Numero 19

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.19 (19 Avril 1993)
File 1--Alertes aux fausses factures d'annuaires France Telecom
File 2--Le "Clipper Chip": rapide descriptif
File 3--Slide presente a la Maison Blanche durant le briefing
File 4--Clipper Chip et le FBI (reprints)
File 5--Piratage France Telecom par un technicien France Telecom

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.

Issues of ChaosD can also be found on some French BBS.  Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.91] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Thu Apr 15 11:47:00 +0200 1993
From: dufour_s@galaxie.int-evry.fr (Stephane DUFOUR )
Subject: File 1--Alertes aux fausses factures d'annuaires France Telecom


     Depuis plus de deux ans, des vagues de pseudo-factures imitant les
factures de France Telecom, se repandent sur le territoire national.  Ces
documents constituent une double tromperie:

     * Ils se presentent d'une part comme des factures, alors qu'ils ne
sont censes etre qu'une offre d'abonnement a un service d'annuaire pour la
telecopie, le telex ou le Minitel.  Cette presentation visiblement destinee
a abuser les consommateurs est penalement reprehensible et confine a la
tentative d'escroquerie.

     * Ils imitent d'autre part d'une facon plus ou moins manifeste les
factures emises par France Telecom, tant en ce qui concerne le nom du
service (France service Minitel, France Telecopie) que la presentation
generale, le logo ou la couleur a dominante bleue.  Ceci constitue aussi
une infraction de contrefacon ou imitation illicite.

     Des l'apparition de ces fraudes, France Telecom a reagi en portant
plainte chaque fois qu'une nouvelle affaire apparaissait.  Il a mis en garde
tous ses abonnes individuellement contre les risques de confusion.  Il est
intervenu sur ce sujet a de nombreuses reprises dans les medias et continue
de la faire.  Une collaboration etroite a ete etablie avec le parquet de
Paris qui centralise les plaintes et coordonne les differentes enquetes.

     France Telecom transmet en particulierles nouveaux "modeles" de factures
des qu'ils lui sont signales.  A titre d'exemple, cette cooperation entre
la justice, France Telecom et la Poste a permis en un an d'intercepter sur
commission rogatoire pres de 350 000 plis avant leur distribution et de
bloquer, en liaison avec les banques de nombreux cheques.

     L'instruction judiciaire a ete rendue plus difficile par le caractere
international de la fraude qui affecte une bonne partie de l'Europe et
utilise l'Autriche et le Lichtenstein comme plaques tournantes.  Toutefois
plusieurs personnes devraient etre traduites devant le tribunal correction-
nel d'ici cet ete.

------------------------------

Date: Wed, 21 Apr 93 19:21:48 EDT
From: denning@cs.cosc.georgetown.edu (Dorothy Denning )
Subject: File 2--Le "Clipper Chip": rapide descriptif
Repost from: RISKS Digest


                     THE CLIPPER CHIP: A TECHNICAL SUMMARY
                               Dorothy Denning
                           Revised, April 21, 1993

INTRODUCTION

On April 16, the President announced a new initiative that will bring
together the Federal Government and industry in a voluntary program
to provide secure communications while meeting the legitimate needs of
law enforcement.  At the heart of the plan is a new tamper-proof encryption
chip called the "Clipper Chip" together with a split-key approach to
escrowing keys.  Two escrow agencies are used, and the key parts from
both are needed to reconstruct a key.


CHIP CONTENTS

The Clipper Chip contains a classified single-key 64-bit block
encryption algorithm called "Skipjack."  The algorithm uses 80 bit keys
(compared with 56 for the DES) and has 32 rounds of scrambling
(compared with 16 for the DES).  It supports all 4 DES modes of
operation.  The algorithm takes 32 clock ticks, and in Electronic
Codebook (ECB) mode runs at 12 Mbits per second.

Each chip includes the following components:

   the Skipjack encryption algorithm
   F, an 80-bit family key that is common to all chips
   N, a 30-bit serial number (this length is subject to change)
   U, an 80-bit secret key that unlocks all messages encrypted with the chip

The chips are programmed by Mykotronx, Inc., which calls them the
"MYK-78."  The silicon is supplied by VLSI Technology Inc.  They are
implemented in 1 micron technology and will initially sell for about
$30 each in quantities of 10,000 or more.  The price should drop as the
technology is shrunk to .8 micron.


ENCRYPTING WITH THE CHIP

To see how the chip is used, imagine that it is embedded in the AT&T
telephone security device (as it will be).  Suppose I call someone and
we both have such a device.  After pushing a button to start a secure
conversation, my security device will negotiate an 80-bit session key K
with the device at the other end.  This key negotiation takes place
without the Clipper Chip.  In general, any method of key exchange can
be used such as the Diffie-Hellman public-key distribution method.

Once the session key K is established, the Clipper Chip is used to
encrypt the conversation or message stream M (digitized voice).  The
telephone security device feeds K and M into the chip to produce two
values:

   E[M; K], the encrypted message stream, and
   E[E[K; U] + N; F], a law enforcement field ,

which are transmitted over the telephone line.  The law enforcement
field thus contains the session key K encrypted under the unit key U
concatenated with the serial number N, all encrypted under the family
key F.  The law enforcement field is decrypted by law enforcement after
an authorized wiretap has been installed.

The ciphertext E[M; K] is decrypted by the receiver's device using the
session key:

        D[E[M; K]; K] = M .


CHIP PROGRAMMING AND ESCROW

All Clipper Chips are programmed inside a SCIF (Secure Compartmented
Information Facility), which is essentially a vault.  The SCIF contains
a laptop computer and equipment to program the chips.  About 300 chips
are programmed during a single session.  The SCIF is located at
Mykotronx.

At the beginning of a session, a trusted agent from each of the two key
escrow agencies enters the vault.  Agent 1 enters a secret, random
80-bit value S1 into the laptop and agent 2 enters a secret, random
80-bit value S2. These random values serve as seeds to generate unit
keys for a sequence of serial numbers.  Thus, the unit keys are a
function of 160 secret, random bits, where each agent knows only 80.

To generate the unit key for a serial number N, the 30-bit value N is
first padded with a fixed 34-bit block to produce a 64-bit block N1.
S1 and S2 are then used as keys to triple-encrypt N1, producing a
64-bit block R1:

        R1 = E[D[E[N1; S1]; S2]; S1] .

Similarly, N is padded with two other 34-bit blocks to produce N2 and
N3, and two additional 64-bit blocks R2 and R3 are computed:

        R2 = E[D[E[N2; S1]; S2]; S1]
        R3 = E[D[E[N3; S1]; S2]; S1] .

R1, R2, and R3 are then concatenated together, giving 192 bits. The
first 80 bits are assigned to U1 and the second 80 bits to U2.  The
rest are discarded.  The unit key U is the XOR of U1 and U2.  U1 and U2
are the key parts that are separately escrowed with the two escrow
agencies.

As a sequence of values for U1, U2, and U are generated, they are
written onto three separate floppy disks.  The first disk contains a
file for each serial number that contains the corresponding key part
U1.  The second disk is similar but contains the U2 values.  The third
disk contains the unit keys U.  Agent 1 takes the first disk and agent
2 takes the second disk.  Thus each agent walks away knowing
an 80-bit seed and the 80-bit key parts.  However, the agent does not
know the other 80 bits used to generate the keys or the other 80-bit
key parts.

The third disk is used to program the chips.  After the chips are
programmed, all information is discarded from the vault and the agents
leave.  The laptop may be destroyed for additional assurance that no
information is left behind.

The protocol may be changed slightly so that four people are in the
room instead of two.  The first two would provide the seeds S1 and S2,
and the second two (the escrow agents) would take the disks back to
the escrow agencies.

The escrow agencies have as yet to be determined, but they will not
be the NSA, CIA, FBI, or any other law enforcement agency.  One or
both may be independent from the government.


LAW ENFORCEMENT USE

When law enforcement has been authorized to tap an encrypted line, they
will first take the warrant to the service provider in order to get
access to the communications line.  Let us assume that the tap is in
place and that they have determined that the line is encrypted with the
Clipper Chip.  The law enforcement field is first decrypted with the
family key F, giving E[K; U] + N.  Documentation certifying that a tap
has been authorized for the party associated with serial number N is
then sent (e.g., via secure FAX) to each of the key escrow agents, who
return (e.g., also via secure FAX) U1 and U2.  U1 and U2 are XORed
together to produce the unit key U, and E[K; U] is decrypted to get the
session key K.  Finally the message stream is decrypted.  All this will
be accomplished through a special black box decoder.


CAPSTONE: THE NEXT GENERATION

A successor to the Clipper Chip, called "Capstone" by the government
and "MYK-80" by Mykotronx, has already been developed.  It will include
the Skipjack algorithm, the Digital Signature Standard (DSS), the
Secure Hash Algorithm (SHA), a method of key exchange, a fast
exponentiator, and a randomizer.  A prototoype will be available for
testing on April 22, and the chips are expected to be ready for
delivery in June or July.


ACKNOWLEDGMENT AND DISTRIBUTION NOTICE

This article is based on information provided by NSA, NIST, FBI, and
Mykotronx.  Permission to distribute this document is granted.

------------------------------

Date: Mon, 19 Apr 93 9:21:53 EDT
From: clipper@first.org (Clipper Chip Announcement )
Subject: File 3--Slide presente a la Maison Blanche durant le briefing

Note:     The following material was handed out a press briefing on the
          Clipper Chip on 4/16.

                        Chip Operation

                           Microchip
User's Message      +----------------------+
------------------> |                      |      1.  Message encrypted
                    | Encryption Algorithm |          with user's key
                    |                      |
                    | Serial #             |      2.  User's key encrypted
                    |                      |-->       with chip unique key
                    | Chip Unique Key      |
User's Encryption   |                      |      3.  Serial # encrypted
Key                 | Chip Family Key      |          with chip family key
------------------> |                      |
                    |                      |
                    +----------------------+


              For Law Enforcement to Read a Suspect's Message

1. Need to obtain court authorized warrant to tap the suspect's telephone;

2. Record encrypted message;

3. Use chip family key to decrypt chip serial number;

4. Take this serial number *and* court order to custodians of disks A and B;

5. Add the A and B components for that serial number = the chip
   unique key for the suspect user;

6. Use this key to decrypt the user's message key for
   this recorded message;

7. Finally, use this message key to decrypt the recorded message.

------------------------------

Date: Mon Apr 19 12:58:44 -0400 1993
From: prutkov@garnet.acns.fsu.edu (Paul Rutkovsky )
Subject: File 4--Clipper Chip et le FBI (reprints)
Copyrights: Warren Publishing, Inc. & San Jose Mercury News, 1993


GOVT. WEIGHS IN ON PRIVACY-VS.-ENCRYPTION DEBATE,  WITH ITS OWN TECHNOL OGY

Communications Daily   April 19, 1993   p. N/A
ISSN: 0277-0679

    Clinton   Administration   Fri.  announced  sweeping  policy  directive
designed   to  protect  privacy  of  voice  and  data  transmissions  using
govt.-developed encryption technology that ensures law enforcement agencies
will  have  ability  to  eavesdrop.  Encyrption is  achieved through use of
"Clipper    Chip  "  that  will  be  built  into telephones, computers, fax
machines.  Although  govt.  will  adopt  new  chip  as its standard, use in
private sector will be on voluntary basis.

    AT&T  Fri.  became  first  company  to  announce  publicly  support  of
 Clipper    Chip  .  "We  believe  it  will  give our customers far greater
protection in defeating hackers or eavesdroppers in attempting to intercept
a  call,"  said  AT&T Vp Secure Communications Systems Edward Hickey. Govt.
already has purchased some evaluation units from AT&T with  Clipper   Chip
installed,  said  Raymond  Kramer,  acting  dir.  of  National Institute of
Standards  &  Technology  (NIST).  Govt. expects to purchase "well over the
thousands"  of  such   Clipper   Chip  units, he said, but he couldn't give
figures  for  how many it might buy from AT&T. AT&T spokesman said products
with   Clipper    Chip   included  will  be  available  commercially in 2nd
quarter.

    President  Clinton Thurs. signed Top Secret National Security Directive
outlining  details  of  privacy  and  encryption policy review. Review will
bring  together  industry  and  govt.  experts  under direction of National
Security Council in attempt to resolve long-running controversy on right of
businesses  and  citizens  to  protect all forms of communication and govt.
right to conduct lawful investigations. Review will take 3-4 months, NIST's
Kramer said.

    Law  enforcement  agencies  are  concerned  about  rising popularity of
digital   encryption   methods.  Multinational  businesses,  worried  about
economic  espionage,  increasingly  are incorporating encryption technology
for  all  communications.  Law  enforcement  agencies  have  voiced growing
concern  that if they don't move quickly to enact laws assuring them access
to   encrypted   and  digital  communications,  they  will  be  at  decided
disadvantage in attempting to thwart  criminal acts.   FBI  spokesman James
Kallstrom acknowledged that "not many" criminals today are using encryption
to  skirt  law,  but  putting  methods  in  place  now  to  assure means of
intercepting such communications "is vital" to law enforcement's mission.

    Encryption  program  will  be  available  to  any  vendor that wants to
manufacture  chips,  Kramer  said.  However,  company  that  developed  and
designed  chip  under  sole-source  contract  from National Security Agency
(NSA)  --  Mykotronx,  Torrance,  Cal.  -- has solid lead on market. Kramer
acknowledged   job   was   handed   to  it  with  NSA's  full  approval  of
noncompetitive  bid  contract.  He defended noncompetition aspect: "We went
out  and  found the only company capable of delivering this technology." He
said  govt.  has been using  Clipper   Chip  technology for "a while now in
classified  applications,"  but declined to say how long it had been in use
before  White   House  announcement.

    Each  chip will have 3 unique "keys" issued to it. When manufactured, 2
of  those  keys  will be sent to govt. and will be held by "escrow agents."
For  law  enforcement  agency to be able descramble transmissions, it first
must  get  court order that allows keys held in escrow to be released. Only
when  those keys are used in tandem can law enforcement agencies unscramble
codes and listen in on conversations. Attorney Gen.'s office will "make all
arrangements with appropriate entities to hold keys,"  White   House  said.
Those escrow keys could be held by private organizations, govt. agencies or
others,  Kramer  said.  But  only  2  entities  will  be chosen and will be
responsible  for  administering  data  base  that will store keys. Attorney
Gen.'s  office is expected to select escrow key holders "within a couple of
weeks," Kramer said.

    Plan  already  is  drawing fire from civil liberties groups and privacy
advocates.  Electronic Frontier Foundation (EFF) said  White   House  acted
"before  any  public  comment  or  discussion  has  been  allowed." It said
Administration  will  use  "its  leverage  to  get  all telephone equipment
vendors  to adopt" technology. EFF criticized govt.'s sole-source contract,
saying there may be other companies that have better encryption technology,
and  because  encryption  algorithm is classified, it can't be tested. "The
public will only have confidence in the security of a standard that is open
to  independent,  expert scrutiny," EFF said. Privacy experts are concerned
that  because   Clipper    Chip  was developed under NSA contract, it might
have "backdoor" known only to NSA that would allow agency to crack code and
bypass  court  order. Kramer disagreed: "There is positively no backdoor to
this technology."

    Because  use  of  Clipper   Chip  is entirely voluntary, businesses and
private  users  -- including criminals -- are free to choose other means of
encryption,  leaving  govt.  and law enforcement agencies with dilemma they
now  face.    FBI  's  Kallstrom  acknowledged criminals still could thwart
investigations  if  they  used  non- Clipper    Chip   products,  "but most
criminals aren't so smart."

    Ability  of  govt.   to  eavesdrop on  Clipper   Chip -equipped devices
still  doesn't  solve  broader  problem:  Ability  to wiretap conversations
moving  across  digital  telecommunications  lines.  That  problem is being
addressed  separately  by  FBI 's controversial digital wiretap legislation
that has failed to find congressional sponsor and is languishing in Justice
Dept., waiting for support of Attorney Gen.

+++++

U.S. SCHEME TO EAVESDROP ELECTRONICALLY RAISES CONCERNS CIVIL LIBERTARIANS,
INDUSTRY GROUP WANT PUBLIC INPUT INTO 'KEY' PLAN
San Jose Mercury News (SJ) - Saturday, April 17, 1993
By: RORY J. O'CONNOR, Mercury News Staff Writer
Edition: Morning Final  Section: Business  Page: 11D

Civil  libertarians  and  a  major  computer industry group raised concerns
Friday about how much protection a Clinton administration plan would afford
private   electronic  communications,  from  cellular  telephone  calls  to
computer data.

  The  administration Friday said it would begin using technology developed
by  the  government's  National  Institute  of  Standards and Technology to
balance  two  competing  interests:  the  desire  of citizens to keep their
conversations  private and the need for law enforcement agencies to monitor
those conversations after getting a court order.

The  technology  that   enables this is a computer chip called the  Clipper
Chip   that  scrambles a  telephone call or computer message using a secret
algorithm, or formula.

  But  each  chip also comes with a pair of electronic "keys" that could be
used  by law enforcement agencies to decipher the secret messages generated
by the chip.

  The Clinton proposal calls for one key to be held by each of two separate
"trusted" third parties, who would release them to law enforcement agencies
that  obtained  legal  authority to intercept the communications. Both keys
would be needed to decipher a message.

  The  Electronic  Frontier  Foundation,  a  not-for-profit civil liberties
group,  praised  the  administration  for  considering  the  issue.  But it
criticized the lack of public input into the plan.

  "They've announced a big inquiry with public input, but they've reached a
conclusion before they started," said Daniel J. Weitzner, staff counsel for
the Washington-based foundation.

  Although  the  administration's  plan calls only for equipping government
telephones  with  the  security devices, some groups are concerned the plan
might  become  a standard for all manner of electronic communication before
the public has a chance to debate its merits.

  "I  don't  want to sound too stridently opposed to this," said Ken Wasch,
executive   director  of  the  Software  Publishers  Association  (SPA)  in
Washington. "But... we feel blindsided."

  The  SPA  was discussing data security issues with Clinton administration
officials  but  had  not  expected any  White   House  action until August,
said Ilene Rosenthal, general counsel.

  Besides the lack of initial hearings, both groups said they had two major
concerns about the Clinton plan:

  (box)  Because  the  algorithm  itself  is  secret,  the groups say it is
impossible  for the public to discern if it is truly secure. Users can't be
certain  government  spy  agencies  have  not  hidden  a "back door" in the
software that will allow them to read anything they want.

  "So  far there hasn't been a credible explanation about why the algorithm
has to be secret," Weitzner said.

  (box)  The  administration  hasn't decided who will be the escrow agents,
and  it  seems  unlikely  any  government agency, corporate entity or other
organization would be deemed trustworthy by every user.

  Even  assuming  all  concerned  can  agree  on  who will hold them, civil
libertarians  are  concerned  that  the  keys,  by  giving  law enforcement
agencies access to individuals' private communications, might pose a threat
to constitutional protections against self-incrimination.

   Washington sources who requested anonymity suggested the  White    House
might  have  drafted  its  plan quickly because of concern over sales of an
AT&T  device  that  encrypts  phone  calls  using  an  older standard, Data
Encryption  Standard. The sources said law enforcement officials feared the
device  would  create  an explosion in secured telephone traffic that would
severely hamper their efforts to wiretap calls.

  American Telephone & Telegraph announced Friday it would adapt the $1,200
product,  called the Telephone Security Device, to use the  Clipper    Chip
by  the  end  of  this  fiscal  quarter. AT&T makes a related device, which
encrypts  voice and computer data transmissions, that could be converted to
the Clipper technology, said spokesman Bill Jones.

  Jones  said  he  wasn't  aware  of any concern by the government over the
current  model  of  the  Telephone  Security Device, which has been sold to
government and business customers.

  At least one company was quite pleased with the plan: San Jose chip maker
VLSI  Technology,  which  will  manufacture  the   Clipper    chips   for a
Torrance company that is selling them to the government and to AT&T.

  VLSI,  which  invented  a  manufacturing method the company said makes it
difficult  to "reverse engineer" the chip or discern the encryption scheme,
expects  to  make  $50  million in the next three years selling the device,
said Jeff Hendy, director of new product marketing for the company.

------------------------------

Date: Thu Apr 15 11:47:00 +0200 1993
From: createuraltern.com (Fabien Pigere )
Subject: File 5--Piratage France Telecom par un technicien France Telecom
Copyright: Elsevier Science Publishers Ltd., 1992

ADDING TO CHAOS
Network Monitor      March, 1992
ISSN: 0953-8402               WORD COUNT: 121
PUBLISHER:  Elsevier Advanced Technology Publications

France  Telecom was in the news again recently after a technician was found
guilty  of  aiding  hackers  to  gain information about using international
network connections.

The  technician  worked for Intelcom France, an FT subsidiary which manages
the international transit node (NTI) for France Telecom networks.

According  to  the  police  the technician "played a determine role" in the
leak of NTI access codes notably to the Chaos Computer Club France.

Jean-Bernard Condat of the CCCF denied the accusation that his organization
which he says specializes in the security of information systems,  received
the access codes from the technician.

The  Chaos  Computer  Club  began life in Germany and has played a twilight
role,  half  supporting hackers and half finding security loopholes ignored
or hidden by suppliers and operations.

INDUSTRY:  Telecommunications (TL)

------------------------------

End of Chaos Digest #1.19
************************************

Downloaded From P-80 International Information Systems 304-744-2253
