Chaos Digest            Mercredi 28 Avril 1993        Volume 1 : Numero 21
                             ISSN 1244-4901

       Editeur: Jean-Bernard Condat (jbcondat@attmail.com)
       Archiviste: Yves-Marie Crabbe
       Co-Redacteurs: Arnaud Bigare, Stephane Briere

TABLE DES MATIERES, #1.21 (28 Avril 1993)
File 1--Liste des 36xx de France Telecom
File 2--_Securite Informatique '93_ (annonce)
File 3--Nouveau virus Mac: INIT-M

Chaos Digest is a weekly electronic journal/newsletter. Subscriptions are
available at no cost by sending a message to:
                linux-activists-request@niksula.hut.fi
with a mail header or first line containing the following informations:
                    X-Mn-Admin: join CHAOS_DIGEST

The editors may be contacted by voice (+33 1 47874083), fax (+33 1 47877070)
or S-mail at: Jean-Bernard Condat, Chaos Computer Club France [CCCF], B.P.
155, 93404 St-Ouen Cedex, France.

Issues of ChaosD can also be found on some French BBS.  Back issues of
ChaosD can be found on the Internet as part of the Computer underground
Digest archives.  They're accessible using anonymous FTP from:

        * kragar.eff.org [192.88.144.4] in /pub/cud/chaos
        * uglymouse.css.itd.umich.edu [141.211.182.91] in /pub/CuD/chaos
        * halcyon.com [192.135.191.2] in /pub/mirror/cud/chaos
        * ftp.cic.net [192.131.22.2] in /e-serials/alphabetic/c/chaos-digest
        * ftp.ee.mu.oz.au [128.250.77.2] in /pub/text/CuD/chaos
        * nic.funet.fi [128.214.6.100] in /pub/doc/cud/chaos
        * orchid.csv.warwick.ac.uk [137.205.192.5] in /pub/cud/chaos

CHAOS DIGEST is an open forum dedicated to sharing French information among
computerists and to the presentation and debate of diverse views. ChaosD
material may be reprinted for non-profit as long as the source is cited.
Some authors do copyright their material, and they should be contacted for
reprint permission.  Readers are encouraged to submit reasoned articles in
French, English or German languages relating to computer culture and
telecommunications.  Articles are preferred to short responses.  Please
avoid quoting previous posts unless absolutely necessary.

DISCLAIMER: The views represented herein do not necessarily represent
            the views of the moderators. Chaos Digest contributors
            assume all responsibility for ensuring that articles
            submitted do not violate copyright protections.

----------------------------------------------------------------------

From: jbcondat@attmail.com
Date: 26 Apr 939 06:59:59 GMT
Subject: File 1--Liste des 36xx de France Telecom
Repost from: telecom13.281.1@eecs.nwu.edu


All week, France Telecom added new direct services to the list of
special numbers beginning by {ZAB=} _36.  This new "36" numbers are
available all over the French regions with some restrictions.

Following the list of all services available April 26th:


                   LIST OF  36PQ SERVICES
                      APRIL 26TH, 1993

Number      Description of Provided Service

3600        Transpac access (1,200 baud/s)
3601        Transpac access (300 baud/s)
3602        Transpac access (2,400 & 4,800 baud/s)
3603        Transpac X.32
3605mcdu    "Numero Vert" by Minitel (it's like the 800 phone
            numbers but for X.25 computer access). FREE.
36062424    Asynchrounous computer access (300-2,400 baud/s)
360736du    Reference PAVI (for X.25 services test procedures)
36086464    Synchronous Transpac access 64 kbit/s
3609mcdu    Computer access for Alphapage (the French pager)
            message delivery. {mcdu=} 0909 for a 80-characters
            alpha-numeric message
3610        Automatic French access for calls using a "Carte
            Pastel" (the equivalent of an MCI or AT&T phone
            card, but with three options: (1) restricted access
            for children able to phone only to 10 predefined
            phone numbers; (2) national, and (3) international)
3612        Minicom, the France Telecom videotex mail system
            directly linked with your phone number
3613        Videotex access Teletel 1
            TTY access (no-business services)
3614        Videotex access Teletel 2
3615        Videotex access Teletel 3
3616        Videotex access, business services
3617        Videotex access, business services (high price)
3618        Minitel-to-Minitel communications (for blind peoples
            for example)
3619        International Minitel services (phone directories, e.g.)
3621        ASCII standard access
3622        Teletel access for Germany, Belgium Italy, Luxembourg
            and in a short time, all European countries
3623        Minitel--High Speed Access (2,400 baud/s and more)
3624mcdu    Business X.25 access
3625mcdu    Business X.25 access
3626mcdu    Business X.25 access
3627mcdu    Business X.25 access
3628mcdu    Business X.25 access
3629mcdu    Business X.25 access
3637        TV special abbreviated national phone number, like for
            "Telethon 1993" action
3638mcdu    ISDN France-Luxembourg
3642        Technician Position--FRANCE TELECOM (confidential)
36431111    International Phone Book
3644        DERAL (phone qualities parameters tests)--FRANCE
            TELECOM (confidential)
3646        Configuration tests--FRANCE TELECOM (confidential)
3650        Direct voice phone operator for "Carte Pastel"
3653        Systeme Teletext-Telex
3655        Phoned telegrams
3656        Telegrams posted by Minitel
3658        France Telecom' customers local reclamation service
3659mcdu    Local night phone information service
36606060    15-digit message for Alphapage
36616136    Operator's pager messages delivery service
3663mcdu    Numero Azur (another type of tarification for extra
            phone number: FRF 0.73 pro call without any restriction
            regarding the call duration)
3664mcdu    Audiotel, paided phone service
3665mcdu    Audiotel, paided phone service: 5 UT (FRF 3.65 pro call)
3666mcdu    Audiotel, paided phone service: MEDIA
3667mcdu    Audiotel, paided phone service
3668mcdu    Audiotel, paided phone service
3670mcdu    Audiotel, paided phone service
3672        MemoPhone: public answering system (maximum 5 messages
            of 30 secondes): FRF 1.46
3673mcdu    MemoPhone between all French departements: {mcdu=} {mcmc=}
            with {mc=} the departement postal code. For example, for
            Paris, {mc=} 75.
3689mcdu    Conference by phone (max. 25 persons)
3699        Voice clock


                   _____Jean-Bernard Condat_____
 [Editor of _Chaos Digest_, the first computer security e-journal]
             CCCF, B.P. 8005, 69351 Lyon Cedex 08, France
              Phone: +33 1 47874083; Fax: +33 1 47877070
          InterNet: jbcondat@attmail.com  or  cccf@altern.com

------------------------------

Date: Thu, 22 Apr 93 18:02:45 -0500
From: celma_s@epita.fr (Samuel Celma )
Subject: File 2--_Securite Informatique '93_ (annonce)


                      5e manifestation annuelle
                      Securite Informatique '93

            23-24 Septembre 1993, Maison de la Chimie, PARIS
        (Maison de la Chimie, 28 rue Saint-Dominique, 75007 Paris
                    tel.: 47051073; fax: 45559862)

               21-22 Juin 1993, Hotel Beau Rivage, GENEVE
        (Hotel Beau Rivage, 13 quai du Mont-Blanc, CH-1201 Geneve
               tel +41 22 731 02 21, fax: +41 22 738 98 47)


21 Bonnes Raisons de Participer a cette Manifestation

1. Determinez les directions dans lesquelles les structures europeennes vont
   s'engager en matiere de securite informatique;

2. Apprenez a mettre en place et a gerer le suivi de votre politique de
   securite;

3. Mesurez les couts et reussissez la negociation du budget securite avec
   votre Direction Generale;

4. Faites le point sur les resultats de l'utilisation de la loi en matiere
   de fraude informatique et anticipez les changements au niveau des
   institutions europeennes;

5. Prenez conscience du suivi de l'enquete sur le terrain et des relations
   indispensables entre le manager et les forces de l'ordre;

6. Evaluez le role de l'assurance et sa place dans une politique de securite;

7. Prenez part a une demonstration d'interactivite virale afin de mieux
   comprendre les facons de gerer l'apres sinistre;

8. Determinez les moyens a mettre en oeuvre pour assurer a la fois Qualite
   et securute;

9. Maitrisez les elements d'une mission securite des Echanges de Donnees
   Informatisees;

10. Devenez l'expert securite de l'environnement UNIX de votre entreprise;

11. Maitrisez la gestion au quotidien d'une securite qui est primordiale a
   la bonne sante de votre entreprise;

12. Tirez profit des etudes de cxas afin de baser vos futurs developpements
   sur les conseils avertis d'utilisateurs;

13. Adaptez la securite informatique a votre societe, quelle que soit sa
   taille, en profitant d'exploses concrets, destines aux entreprises de
   dimensoin moyenne;

14. Ayez une longueur d'avance sur la concurrence en appliquant les principes
   et les methodes de securisation de vos Systemes d'Information;

15. Developpez une strategie securite afin de parer a l'eventualite d'une
   sous-traitance temporaire ou definitive de vos moyens informatiques;

16. Optimisez votre participation en choisissant a l'avance vos ateliers;

17. Beneficiez des syntheses d'atelier afin de maitriser l'aspect global de
   la securite informatique;

18. Venez rencontrer vos homologues et debattre avec eux des metiers de la
   securite informatique;

19. Beneficiez du dialogue offert par la table ronde pour reflechir en
   interne, sur la formation des professionels de la securite des Systemes
   d'Infomation;

20. Rencontrez les experts en securite informatique venus de Belgique, de
   France et de Suisse pour prendre part a cet evenement annuel;

21. faites le bilan de vos competences pour palier les besoins de
   securisation.


PREMIERE JOURNEE

 8.30  Accueil des participants

 9.00  Allocution d'ouverture du President
       PARIS: Jean-Philippe JOUAS, Dir Surete & Protection, BULL
       GENEVE: Jean-Luc CHAPPUIS, Pres. CLUSIS

 9.15  Le Panorama de la Securite Informatique
       - Quelles sont les normes europeennes qui vont changer le panorama
         de la securite informatique?
       - Quels sont les enjeux du programme Infosec?
       Gerard BOUGET, Vice-Pres. FIASI

 9.45  La methodologie d'un audit de securite du systeme d'information
       - Approche et demarche de l'audit, de la methodologie a la pratique
       - Presentation des resultats et bilans
       Guy TOLLET, Internal Audit Manager, AG 1824

10.30  Pause cafe

10.45  Mise en oeuvre et evolution du schema directeur
       - La mise en oeuvre, la gestion et son suivi
       - Que faire pour que le schema directeur reflete la strategie de
         l'entreprise?
       - Comment apporter les modifications necessaires afin de suivre
         l'evolution?
       Guy POINAS, Responsable Qualite-Securite, FRAMATOME

11.30  Comment maintenir operationnels les moyens de sauvegarde et de secours
       - La disponibilite des donnes et des moyens
       - Actualisation des differents plans
       - Quels sont les controles, audit et tests a realiser?
       Alain REFFRAY, Pres. du Directoire, EXPLOITIQUE

12.15  Les couts de la securte: comment justifier un budger securite
       informatique en periode de crise economique?
       - Comptabilite analytique des couts: comment mesurer la rentabilite
         de l'investissement securite?
       - Comment chiffrer les besoins en securite informatique?
       - La negociation avec la DG: quels sont les arguments a developper?
       - Peut-on vendre le projet securite?
       Luc GOLVERS, Pres. du CLUSIB

13.00  Dejeuner

14.45  Evolution des besoins en matiere de fraude informatique
       - Etudes et resultats de l'utilisation de la loi de 1988
       - Etat de la legislation dans les pays de la CEE
       - Etat des projets au niveau des institutions europeennes
       Jacques GODFRAIN, Depute de l'Aveyron, ASSEMBLEE NATIONALE

15.30  ATELIER LEGISLATION
       Aspects Juridiques: Moyens de Prevention et sanctions
       - Mesures de prevention pour assurer la securite du systeme
         d'information
       - Quels sont les recours et sanctions?
       Alain BENSOUSSAN, Avocat, Cabinet Alain BENSOUSSAN

       ATELIER ASSURANCE
       Risk Management: Le financement de l'apres sinistre
       - Les limites du transfert a l'assurance: garanties et capacites
         financieres actuellement disponibles
       - Les conditions d'une assurance adequate et fiable: comment etendre
         le champ de l'assurable et eviter surprimes et sous-assurances
       Yves MAQUET, Risk Manager Consultant, SUGMA RISK
       & Gilbert FLEPP, Resp. Souscription Risques et Techn., CIGNA FRANCE

       ATELIER REPRESSION
       La demarche d'une enquete sur la fraude informatique
       - L'ampleur du phenomene criminalite reelle, apparente et legale
       - Les differents services repressifs: problematique et illustration
         de l'enquete de Police
       - Perspectives d'evolution de la deliquance informatique
       PARIS: Christian MIRABEL, Chef Sect. Informatique, Brigade Financiere
              Direction de la Police Judiciaire
       GENEVE: Daniel CHEVALLEY, Brig. Fin., Police de Surete Vaudoise

16.15  Pause cafe

16.45  Synthese des Ateliers

17.15  Les virus informatiques: les enjeux de la securite post-contamination
       - Comment aborder la demarche post-contamination
       - La distinction entre detection/diagnostic et elimination/reparation
       - Caracteristiques et risques de l'interactivite virale
       Jean-Claude HOFF, Vice-Pres. Club Sec. Informatique Region Picardie

18.15  Allocution de cloture du President

18.30  Fin de la premiere journee


SECONDE JOURNEE

 8.30  Accueil des participants

 9.00  Allocution du President
       PARIS: Luc GOLVERS, President du CLUSIB
       GENEVE: Gerard BOUGET, FIASI

 9.15  ATELIER SECURITE & EDI
       Les enjeux specifiques a la securite des echanges de donnees
       - Doit-on controler et auditer les systemes EDI?
       Christophe CUSSAC, Expert Comptable, GUERARD VIALA

       Les moyens de securite et de controle propres aux EDI
       - Les possibilites de controle a l'interieur des messages et de l'EDI
       - Utilisation des messages EDI specifiques pour assurer la securite
       Juerg BRUN, Expert Comptable diplome, ATAG ERNST & YOUNG

       ATELIER SECURITE D'UN PARC MICRO
       Mesures de precaution et de recherche d'outils
       - Le role du centre d'expertise de lutte anti-virus
       - Presentation de differents moyens de sensibilisation
       - Explication des differents outils
       Pierre LESCOP, Resp. Div. Micro-Informatique et Anti-Virus, Serv.de
       Recherche Tech., LA POSTE

       Systemes de gestion et d'administration de la securite des reseaux
       locaux
       - Presentation des besoins pour la securite d'un Lan
       - Mise en place d'un concept de securite
       Christian SCHERRER, Dir., CW CONCEPTWARE

       ATELIER SECURITE SOUS ENVIRONNEMENT UNIX
       - Historique et principes: Unix est-il un OS passoire?
       - Les failles les plus frequentes en environnement UNIX
       - Les solutions additionnelles de niveau C2 et apercu d'un solution B1
       - Les solutions a moyen terme pour une informatique distribuee
       Loup GRONIER, Resp. Formation, CF6 AGENCE SECURITE

10.45  Pause cafe

11.15  Synthese des Ateliers

11.45  Etude de Cas
       Qualite d'acces logique dans le cadre d'un reseau heterogene a la RATP
       Christian PETITPAS, Resp. Securite des Syst. d'Inf., RATP

12.30  Dejeuner

14.15  ATELIER ENTREPRISES DE DIMENSION MOYENNE
       De la therapie appliquee a un jeune etablissement financier en vue de
       son immunisation contre les risques
       - L'approche du generaliste
         * L'examen clinique face a l'environnement "hostile"
         * Les forces vitales a preserver
         * Le traitement applique
       - Les examens de controle du specialiste
       - Le bilan de sante actuel
       Bernard MALAN, Resp. Gestion Res. Techn., FIMAGEST

       Comment aborder la demarche de choix d'un solution de backup
       PARIS: Alan SILLITOE, Resp. Informatique, CHRISTIAN BERNARD DIFFUSION
       GENEVE: Pierre DELETRAZ, Dir., COMPUTER SECURITY SITE S.A.

       ATELIER INDUSTRIE
       Les enjeux de la securite des systemes d'information dans un
       environnement de production
       - Introduction aux objectifs d'une entreprise industrielle
       - La vulnerabilite des systemes d'information dans ce cadre
       - Realisation, choix et perspectives
       Jean-Louis SZUBA, Agent Cental de Sec. Inf., AEROSPACIALE

       La Securite informatique dans un centre de recherche (Etude de Cas)
       - La securite informatique et la protection du patrimoine
       - Les moyens a la disposition des chercheurs
       - Organisation de la securite; les procedures et les outils
       Michel COMBE: Resp. Mission Sec., EDF-DIR ETUDES & RECHERCHES

       ATELIER BANQUES
       Le perimetre "Securite": Pourquoi un tableau de bord securite?
       - Les connections (VTAM)
       - Le logiciel de securite du systeme d'exploitation (RACF)
       - Le referentiel de la securite applicative
       PARIS: Bertrand de la RENAUDIE, Resp. Sec., BANQUE INDOSUEZ
       GENEVE: Jacques BOURACHOT, Resp. Inf., BANQUE INDOSUEZ

       Etudes de cas
       PARIS: La demarche securite au CTR Nord-Est
              - Etude securite et mesures prises
              - L'acquis des experiences
              Roger KNOCHEL, Resp. Securite, CTR NORD-EST, CAISSE D'EPARGNE
       GENEVE: Organisation et gestion de la securite informatique dans un
              contexte de facilities management
              - Le FM et ses prestations dans un milieu bancaire
              - Presentation d'un contexte operationnel
              - Politique de securite: regles et responsabilites
              - Les domaines de securite et leurs interactions
              - Les impacts du FM sur la gestion et l'admin. de la securite
              Raymond ROCHAT, Resp. Securite Inf., BANQUE CANTONALE VAUDOISE

15.45  Pause cafe

16.15  Qualite et securite, partenaires incontournables
       - De quelle maniere peut-on conciler Qualite et Securite?
       Gerard BOUGET, FIASI

17.00  Table ronde
       Les metiers de la securite des systemes d'information
       - Comment la structure peut-elle influencer la fonction?
       - Jusqu'ou vont les responsabilites du Dir. des Syst. d'Inf. (DSI)
         dans sa mission securite informatique
       - Remise en cause de la fonction Dir. Sec. des Syst. d'Inf. (DSSI):
         comment assurer une parfaite securite lorsqu'on assure plusieurs
         missions DSI?
       - Comment assurer la formation des professsionnels de la securite?
       Animateur Paris: Luc Golvers. Avec: Rene KRAFT, CENCEP; Bernard
         MALAN, FIMAGEST; Eddie SOULIER, CIGREF, & Jean VERGNOUX, RENAULT.
       Animateur Geneve: Jean-Luc CHAPPUIS, CLUSIS. Avec: Bernard MALAN,
         FIMAGEST; Jean MENTHONNEX, EPI INGENIEURS-CONSEILS S.A.; Raymond
         ROCHAT, BANQU CANTONALE VAUDOISE.

18.30  Allocution de cloture du President

18.45  Fin de manifestation


|     Inscrivez vous et envoyez le reglement avant le 23 Avril 1993 et      |
|l'Institute for International Research vous offrira gracieusement l'ouvrage|
|                                                                           |
|"Les virus, methodes et techniques de securite" de  Jean-Claude HOFF (1992)|

------------------------------

Date: Thu, 22 Apr 93 18:02:45 -0500
From: spaf@cs.purdue.edu
Subject: File 3--Nouveau virus Mac: INIT-M


                New Macintosh Virus Discovered (INIT-M)
                             22 Apr 1993


Virus: INIT-M
Damage: Alters applications and other files; may severely damage
        file system on infected Macs.  See text below.
Spread: possibly limited, but has potential to spread quickly
Systems affected: All Apple Macintosh computers, under only System 7


The INIT-M virus was recently discovered at Dartmouth College, in a
file downloaded off the net.  This is a DIFFERENT virus than the
INIT-17 virus announced April 12.  It is a malicious virus that may
cause severe damage.

INIT-M rapidly spreads to applications, system extensions, documents
and preference files under System 7; it does not spread or activate on
System 6 systems.  The virus spreads as the application files are run,
and is likely to spread extensively on an infected machine.  The
infection is accomplished by altering existing program code. Besides
this incidental damage (that may, because of bugs in the virus code,
cause more severe damage), the virus also does extensive damage to
systems running on any Friday the 13th -- *not* just booted on that
day.  Files and folders will be renamed to random strings, creation
and modification dates will be changed, and file creator and type
information will be scrambled.  In some very rare circumstances, a
file or files may be deleted.  This behavior is similar to the
previously announced (March 1992) INIT-1984 virus.  Recovery from this
damage will be very difficult or impossible.

Note that the next three Friday the 13ths are in August 1993, May 1994,
and January 1995.

The virus, when present on an infected system, may interfere with the
proper display of some application window operations.  It will also
create a file named "FSV Prefs" in the Preferences folder.

Recent versions of Gatekeeper and SAM Intercept (in advanced and
custom mode) are effective against this virus.  Either program should
generate an alert if the virus is present and attempts to spread to
other files.


The authors of all other major Macintosh anti-virus tools are planning
updates to their tools to locate and/or eliminate this virus. Some of
these are listed below. We recommend that you obtain and run a CURRENT
version of AT LEAST ONE of these programs.


Some specific information on updated Mac anti-virus products follows:

    Tool: Central Point Anti-Virus
    Status: Commercial software
    Revision to be released: 2.01e
    Where to find: Compuserve, America Online, sumex-aim.stanford.edu,
                   Central Point BBS, (503) 690-6650
    When available: immediately
    Comments: The MacSig file will be dated 4/22/93


    Tool: Disinfectant
    Status: Free software (courtesy of Northwestern University and
            John Norstad)
    Revision to be released: 3.2
    When available: immediately
    Where to find: usual archive sites and bulletin boards --
                   ftp.acns.nwu.edu, sumex-aim.stanford.edu,
                   rascal.ics.utexas.edu, AppleLink, America Online,
                   CompuServe, Genie, Calvacom, MacNet, Delphi,
                   comp.binaries.mac


    Tool: Gatekeeper
    Status: Free software (courtesy of Chris Johnson)
    Revision to be released: No new revision needed; 1.2.7 works
    When available: immediately
    Where to find: usual archive sites and bulletin boards --
                   microlib.cc.utexas.edu, sumex-aim.stanford.edu,
                   rascal.ics.utexas.edu, comp.binaries.mac


    Tool: Rival
    Status: Commercial software
    Revision to be released: INIT-M Vaccine
    When available: Immediately.
    Where to find it: AppleLink, America Online, Internet, Compuserve.


    Tool: SAM (Virus Clinic and Intercept)
    Status: Commercial software
    Revision to be released: 3.5.6
    When available: immediately
    Where to find: CompuServe, America Online, Applelink, Symantec's
                   Customer Service @ 800-441-7234
    Comments: Updates to various versions of SAM to detect and remove
          INIT-M are available from the above sources.


    Tool: Virex
    Status: Commercial software
    Revision to be released: 3.93
    Where to find: Datawatch Corporation, (919) 490-1277
    When available: Detection Strings will be available 4/27 on AOL
        and on the "DataGate" BBS @ (919) 419-1602. Updated version
        with detection, repair and prevention capabilities will be
        available next week.
    Comments: Virex 3.93 will detect the virus in any file, and
    repair any file that has not been permanently damaged.  All Virex
    subscribers will automatically be sent an update on diskette.  All
    other registered users will receive a notice by mail.


    Tool: VirusDetective
    Status: Shareware
    Revision to be released: 5.0.9
    When available: immediately
    Where to find: various Mac archives
    Comments: VirusDetective is shareware.  Search strings for the new
           virus will be sent only to registered users.


If you discover what you believe to be a virus on your Macintosh
system, please report it to the vendor/author of your anti-virus
software package for analysis.  Such reports make early, informed
warnings like this one possible for the rest of the Mac community.  If
you are otherwise unsure of who to contact, you may send e-mail to
spaf@cs.purdue.edu as an initial point of contact.

Also, be aware that writing and releasing computer viruses is more
than a rude and damaging act of vandalism -- it is also a violation of
many state and Federal laws in the US, and illegal in several other
countries.  If you have information concerning the author of this or
any other computer virus, please contact any of the anti-virus
providers listed above.  Several Mac virus authors have been
apprehended thanks to the efforts of the Mac user community, and some
have received criminal convictions for their actions.  This is yet one
more way to help protect your computers.

------------------------------

End of Chaos Digest #1.21
************************************

Downloaded From P-80 International Information Systems 304-744-2253
