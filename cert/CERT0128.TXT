
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.23
Original issue date: October 28, 1996
Last revised: --
              
Topic: Vulnerability in WorkMan
- -----------------------------------------------------------------------------

                The original technical content for this advisory
                was published by the IBM-ERS response team and
                is used here with their permission.

There is a vulnerability in the WorkMan compact disc-playing program that
affects UNIX System V Release 4.0 and derivatives and Linux systems.
When the program is installed set-user-id root, it can be used to make any
file on the system world-writable.

To address this problem, you should remove the set-user-id bit from the
program.

We will update this advisory as we receive additional information.
Please check advisory files regularly for updates that relate to your site.

- -----------------------------------------------------------------------------

I.   Description

WorkMan is a popular program used for playing audio compact disks on local
workstation CD-ROM drives that is widely available from many sites around the
Internet. Versions of WorkMan are also included with some operating system
distributions, such as Linux.

On systems where WorkMan was built and installed using the procedures that
are given in "Makefile.linux" or "Makefile.svr4" (in general, this means on
Linux systems and UNIX System V Release 4.0 systems), the WorkMan program
is installed set-user-id root. This means that when the program is run,
it will execute with super-user permissions.

In order to allow signals to be sent to it, WorkMan writes its process-id
to a file called /tmp/.wm_pid. The "-p" option to the program allows the
user to specify a different file name in which to record this information.
When a file is specified with "-p", WorkMan simply attempts to create and/or
truncate the file, and if this succeeds, WorkMan changes the permissions on
the file so that it is world-readable and world-writable.

In the general case, when WorkMan is installed without the set-user-id bit
set, the normal file access permissions provided by the operating system will
prevent users from creating or truncating files they are not authorized to
create or truncate.  However, when WorkMan is installed set-user-id root,
this process breaks down (because "root" is allowed to create/truncate any
file).

WorkMan does not require the set-user-id bit to work; it is installed this
way only on systems that do not make the CD-ROM device file world-readable
by default.

Note: The vulnerability described by "r00t" on several mailing lists is not
      the same one that we describe in this advisory.

II.  Impact

A user with access to an account on the system can use the "-p" option to
create a file anywhere in the file system or to truncate any file in the file
system. The file specified with "-p" will be world-readable and world-writable
when WorkMan is finished.  This can enable the user to create accounts,
destroy log files, and perform other unauthorized actions.

III. Solution

1. Remove the set-user-id bit from the WorkMan program using a command
   such as

        chmod u-s /usr/local/bin/workman

2. Make the CD-ROM device world-readable using a command such as

        chmod +r /dev/cdrom

   On multi-user systems, Step 2 will allow any user to access the contents
   of the disc installed in the CD-ROM; this may not be desirable in all
   environments.

The vulnerability described in this advisory is related to the WorkMan
program, not to the products of particular vendors. However, if a vendor sends
us advice for their users, we will put it in Appendix A. 

...........................................................................

Appendix A - Vendor Information

This appendix contains advice vendors wish to offer their users. Note that the
vulnerability described in this advisory is related to the WorkMan program,
not particular vendors' products.

Sun Microsystems, Inc.
======================

        Sun does not recommend that workman and other utility programs
        be installed setuid root (or anything else) unless that step is
        absolutely necessary. Programs which were not designed with
        security in mind (and most non-setuid programs are not) are
        unlikely to have built-in allowances for abuse. The proper way to
        allow such programs to work is to install them as unprivileged,
        ordinary software, then modify device permissions as necessary
        to allow them to function.

        When an unprivileged users executes a recent version of the workman
        program on a properly configured Solaris 2.x system, a message
        similar to the following appears. (Ellipses added to save space.)

                As root, please run

                        chmod 666 /devices/iommu@0,...sd@6,0:c,raw

                to give yourself permission to access the CD-ROM device.


        That's pretty good advice. Of course, if you don't want to give
        every user access to the contents of a CD (which will sometimes
        be data or software, and sometimes music) such permissions are
        not appropriate.

- -----------------------------------------------------------------------------
The CERT Coordination Center thanks IBM-ERS for permission to reproduce the
technical content in their IBM Emergency Response Service Security
Vulnerability Alert ERS-SVA-E01-1996:005.1. These alerts are copyrighted 1996
International Business Machines Corporation.
- -----------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident Response 
and Security Teams (see ftp://info.cert.org/pub/FIRST/first-contacts). 


CERT/CC Contact Information 
- ---------------------------- 
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST(GMT-5) / EDT(GMT-4)
                and are on call for emergencies during other hours.

Fax      +1 412-268-6989

Postal address
         CERT Coordination Center
         Software Engineering Institute
         Carnegie Mellon University
         Pittsburgh PA 15213-3890
         USA

Using encryption
   We strongly urge you to encrypt sensitive information sent by email. We can
   support a shared DES key or PGP. Contact the CERT/CC for more information. 
   Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

Getting security information
   CERT publications and other security information are available from
        http://www.cert.org/
        ftp://info.cert.org/pub/

   CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce 

   To be added to our mailing list for advisories and bulletins, send your
   email address to 
        cert-advisory-request@cert.org 

- ---------------------------------------------------------------------------
Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is
included.

CERT is a service mark of Carnegie Mellon University.
- ---------------------------------------------------------------------------

This file: ftp://info.cert.org/pub/cert_advisories/CA-96.23.workman_vul
           http://www.cert.org
               click on "CERT Advisories"


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history 




-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMnTdVXVP+x0t4w7BAQEvaQQA3BgO7mS8X7G/qaAQxDx9b7HsIb9JVUGg
H9Zxe7jaG1q3+MISCFdxPtqopdBHbLhNZ8vIN2ZChxsRG52Oj11mvafZK2wqg0ub
3YRZ16QzNaezC+kyyqHn8vIw0+3aSgj2DnxgYSdeVBzF41jcgeEBK5Kra7Qf0ME9
E+CwivneAvU=
=J4RP
-----END PGP SIGNATURE-----

