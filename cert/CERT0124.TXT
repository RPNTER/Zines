
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.19
Original issue date: August 15, 1996
Last revised: December 20, 1996
              Appendix A, HP - revised information from vendor has been
              included.	      

              A complete revision history is at the end of this file.

Topic: Vulnerability in expreserve
- -----------------------------------------------------------------------------

            *** This advisory supersedes CA-93:09 and CA-93:09a. ***

The CERT Coordination Center has received reports of a vulnerability in
expreserve. Though this is not a new vulnerability, it is one that is widely
known and that many users have not yet patched. The CERT/CC team recommends
that you patch your system as soon as possible, as exploitation scripts are
publicly available. Appendix A contains the information we have received from
vendors. Until you can install a patch, you should apply the workaround in
Section III below.

We will update this advisory as we receive additional information.
Please check advisory files regularly for updates that relate to your site.

- -----------------------------------------------------------------------------

I.   Description

     Expreserve is a utility that preserves the state of a file being edited
     by vi(1) or ex(1) when an edit session terminates abnormally or when the
     system crashes. Expreserve has a vulnerability that allows users to
     overwrite any file on the system. Exploitation scripts are publicly
     available.

II.  Impact

     By exploiting this vulnerability, users with access to an account on the
     system can readily gain root privileges.


III. Solution

     A.  Apply a patch or workaround provided by your vendor.
         Below is a summary list of the vendors who have provided information,
         which we have placed in Appendix A of this advisory. If your vendor's
         name is not on this list, please contact the vendor directly.

            Berkeley Software Design, Inc.
            Cray Research
            Data General Corporation
            Digital Equipment Corporation
            Hewlett-Packard Company
            IBM Corporation
            NeXT Software, Inc.
            Open Software Foundation
            The Santa Cruz Operation, Inc.
            Sun Microsystems, Inc.

     B.  Until you are able to apply a patch or workaround, we recommend
         that you remove the execute permissions on the existing
         /usr/lib/expreserve program.  Do this as root:

         % /usr/bin/chmod  a-x  /usr/lib/expreserve

         This workaround disables expreserve functionality. The result of this
         workaround is that if vi(1) or ex(1) is running, and the sessions are
         interrupted, the files being edited will not be preserved and all
         edits not explicitly saved by the users will be lost. Encourage users
         to save their files often.

.........................................................................

Appendix A: Vendor Information

Below is information we have received from vendors concerning the
vulnerability described in this advisory. If you do not see your vendor's
name, please contact the vendor directly for information.


Berkeley Software Design, Inc.
==============================
        BSD/OS is not vulnerable to this problem.  We ship the
        current Keith Bostic nvi which does not use the old expreserve
        scheme to save files (it uses the 4.4BSD-style 1777 tmp
        directories to store user tmp files in /var/tmp owned by
        the user and therefore doesn't require a setuid scheme to
        recover them).

Cray Research
=============
        We have fixed this problem at Cray Research in Unicos version 7.0.

Data General Corporation
========================
        The binary /usr/lib/expreserve is not a setuid program on DG/UX,
        any flavor.  We are not, therefore, vulnerable to the exploitation
        described.  Nevertheless, the suggested change has been made and
        will be included in subsequent releases of DG/UX.

Digital Equipment Corporation
=============================
        This reported problem is not present for Digital's ULTRIX or
        Digital UNIX Operating Systems Software.

        Source:
                Digital Equipment Corporation
                Software Security Response Team
                Copyright (c) Digital Equipment Corporation 1996.
                All rights reserved.

        8/13/96 - DIGITAL EQUIPMENT CORPORATION

Hewlett-Packard Company
=======================

  PROBLEM:  **REVISED 01**  Vulnerability in /usr/lib/expreserve in HP-UX
            9.X and 10.X
  PLATFORM: HP 9000 series 300/400s and 700/800s
  DAMAGE:   The default permissions of file expreserve(1) are in error,
            thereby allowing users to potentially gain root privileges
            on the host.
  SOLUTION: Apply patch PHCO_6363 (series 700/800, HP-UX 9.x), or
                        PHCO_7833 (series 300/400, HP-UX 9.x), or
                        PHCO_8652 (series 700/800, HP-UX 10.0X), or
                        PHCO_8653 (series 700/800, HP-UX 10.10), or
                        PHCO_8654 (series 700/800, HP-UX 10.20).

   Perform the actions described below in releases of HP-UX prior
          to 9.X.)

  AVAILABILITY:  All patches are available now.
  CHANGE SUMMARY:  New patches available for releases of HP-UX 10.XX.

Update

   A. Additional patches for HP-UX have been released to address the
   vulnerability originally appearing in the 18 July 1996 version of
   the Hewlett-Packard bulletin.

   A private communication to HP described a vulnerability that allows
   ordinary users to potentially gain super-user privileges.
   The default permission for the file /usr/lib/expreserve (or on HP-UX
   10.X /usr/lbin/expreserve) needs only minimal privileges.  If the
   patches mentioned above are applied the vulnerability cannot be
   exploited.

   In case no patches is available for your host HP-UX release, system
   administrators are asked to perform the following action to achieve
   the same result.

Fixing the problem

   The vulnerability can be eliminated from releases 9.X of HP-UX
   by applying a patch.  Since some patches will not be made available
   on some releases of HP-UX (e.g., prior to 9.X), affected systems can
   be protected by system administrators.
   They should:

              $ su root
              # chmod 0555 /usr/lib/expreserve

   For HP-UX 10.X systems applying the patches specified above is now in
   order.

   Hewlett-Packard recommends that all customers concerned with the
   security of their HP-UX systems either apply the appropriate
   patch or perform the actions described above as soon as possible.

How to Install the Patch

     1.  Determine which patch is appropriate for your hardware platform
         and operating system:

                      PHCO_6363 (series 700/800, HP-UX 9.x), or
                      PHCO_7833 (series 300/400, HP-UX 9.x),
                      PHCO_8652 (series 700/800, HP-UX 10.0X), or
                      PHCO_8653 (series 700/800, HP-UX 10.10), or
                      PHCO_8654 (series 700/800, HP-UX 10.20).

     2.  Hewlett Packard's HP-UX patches are available via email
         and World Wide Web

     To obtain a copy of the HP SupportLine email service user's
         guide, send the following in the TEXT PORTION OF THE MESSAGE
         to support@us.external.hp.com (no Subject is required):

                               send guide

         The users guide explains the HP-UX patch downloading process
         via email and other services available.

         World Wide Web service for downloading of patches
         is available via our URL:
                  (http://us.external.hp.com)


     3.  Apply the patch to your HP-UX system.

     4.  Examine /tmp/update.log (in 9.X) or /var/adm/sw/swinstall.log
         (in 10.X), for any relevant WARNINGs or ERRORs.

Impact

    These patches for HP-UX releases 9.X and 10.X provide a proper
    permissions /usr/lib/expreserve which fixes the vulnerability.  No
    patches will be available for versions of HP-UX older than 9.X.
    Instead, the workaround is described above.


IBM Corporation
===============
        AIX versions 3.2.5, 4.1, and 4.2 are not vulnerable to this
        particular problem.

        IBM and AIX are registered trademarks of International Business
        Machines Corporation.

NeXT Software, Inc.
===================
        This problem was fixed in or before release 3.3 of NeXTstep.

Open Software Foundation
========================
        OSF's OSF/1 R1.3 is not effected by this vulnerability.

The Santa Cruz Operation, Inc.
==============================
        SCO Operating Systems are not vulnerable to this problem.

Silicon Graphics, Inc.
======================
The Silicon Graphics implementation of expreserv is setgid sys and
not setuid root as reported in the CERT(sm) advisory.   As such this
redefines the exposure to a setgid sys issue.  Exploit would have to
occur on group sys writable files, however, on a default configured
IRIX system there are no system critical files that are group sys
writable and therefore exposure and exploit does not exist.

Silicon Graphics will not be releasing a patch for this issue,
however, the issue will be corrected in future releases of IRIX.

If desired, the setgid permission of the expreserv could be removed
however, this will disable the recovery functions of the vi(1) and
ex(1) editors.   This functionality could be fixed by manually
creating directories for each user in /var/preserve directory.

Sun Microsystems, Inc.
======================
System          Patch ID    Filename           MD5 Checksum
- ------          --------    ---------------    -----------
SunOS 4.1.1     101080-01   101080-01.tar.Z    53c8a5c4eee770924560c5fc100542a3
SunOS 4.1.2     101080-01   101080-01.tar.Z    53c8a5c4eee770924560c5fc100542a3
SunOS 4.1.3     101080-01   101080-01.tar.Z    53c8a5c4eee770924560c5fc100542a3
SunOS 4.1.3C    101080-01   101080-01.tar.Z    53c8a5c4eee770924560c5fc100542a3
SunOS 4.1.3_U1  101579-01   101579-01.tar.Z    327b89942b02c4cb15bb80bf61b2df94
SunOS 4.1.4     Not vulnerable

Solaris 2.0     101119-01   101119-01.tar.Z    No longer available
Solaris 2.1     101089-01   101089-01.tar.Z    No longer available
Solaris 2.2     101090-01   101090-01.tar.Z    e9ff98823abbc75d95410a0cb7856644
Solaris 2.3     Vulnerable; no patch made
Solaris 2.4     102756-01   102756-01.tar.Z    61f4a48ddba41ae1c27e70b84f4c8d87
Solaris 2.4_x86 102757-01   102757-01.tar.Z    1f2b7f3824565ef849eb3c4677567399
Solaris 2.5     Not vulnerable
Solaris 2.5.1   Not vulnerable


- ---------------------------------------------------------------------------
The CERT Coordination Center thanks all the vendors who provided input
for this advisory.
- ---------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact
the CERT staff for more information.

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

CERT Contact Information
- ------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        http://www.cert.org/
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org


Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is
included.

CERT is a service mark of Carnegie Mellon University.

This file: ftp://info.cert.org/pub/cert_advisories/CA-96.19.expreserve
           http://www.cert.org
               click on "CERT Advisories"


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Dec. 20, 1996  Appendix A, HP - revised information from vendor has been
	       included.
Aug. 30, 1996  Information previously in the README was inserted into the
               advisory.
Aug. 28, 1996  Appendix A, SGI - added an entry for this vendor.
Aug. 21, 1996  Appendix A, Sun - added more patch information.





-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMrqlCnVP+x0t4w7BAQEyZQQAps9QDVFvnip8A+VxWuBVQkdUYbDAzrLM
B9l4W2FSXssrJRVewzZafVEB3/lxLDv7dxwuFTNdlGt+sObAjbQTbaAGg8rtzFuc
qy3+uQMLh5oWBR+8UzaB+94VvKBxOzt/rvAdG/8uvAGc+7wHKxMU91ItJGPl0EnV
vZmqP7RTgNo=
=6reG
-----END PGP SIGNATURE-----

