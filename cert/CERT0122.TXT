
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.17
Original issue date: August 6, 1996
Last revised: August 30, 1996
              Removed references to the advisory README file.

              A complete revision history is at the end of this file.

Topic: Vulnerability in Solaris vold
- -----------------------------------------------------------------------------

   The text of this advisory was originally released on August 2, 1996, as
   AUSCERT Advisory AL-96.04, developed by the Australian Computer Emergency
   Response Team. We are reprinting the AUSCERT advisory here with their
   permission. Only the contact information at the end has changed: AUSCERT
   contact information has been replaced with CERT/CC contact information.

   We will update this advisory as we receive additional information.
   Please check advisory files regularly for updates that relate to your site.

=============================================================================

AUSCERT has received a report of a vulnerability in the Sun Microsystems
Solaris 2.x distribution involving the Volume Management daemon, vold(1M).
This program is used to help manage CDROM and floppy devices.

This vulnerability may allow a local user to gain root privileges.

Exploit details involving this vulnerability have been made publicly
available.

At this stage, AUSCERT is not aware of any official patches.  AUSCERT
recommends that sites take the actions suggested in Section 3 until official
patches are available.

- -----------------------------------------------------------------------------

1.  Description

    The Volume Management daemon, vold(1M), manages the CDROM and floppy
    devices.  For example, it provides the ability to automatically detect,
    and then mount, removable media such as CDROMs and floppy devices.

    vold is part of the Solaris 2.x Volume Management package (SUNWvolu).
    It is executed as a background daemon on system startup and runs as root.

    When vold detects that a CDROM or floppy has been inserted into a drive,
    it is configured to automatically mount the media, making it available
    to users.  Part of this process includes the creation of temporary files,
    which are used to allow the Openwindows File Manager, filemgr(1), to
    determine that new media has been mounted.  These files are created by
    the action_filemgr.so shared object which is called indirectly by vold
    through rmmount(1M).  The handling of these files is not performed in a
    secure manner.  As vold is configured to access these temporary files
    with root privileges, it may be possible to manipulate vold into creating
    or over-writing arbitrary files on the system.

    This vulnerability requires that vold be running and media managed by
    vold, such as a CDROM or floppy, be physically loaded into a drive.  Note
    that a local user need not have physical access to the media drive to
    exploit this vulnerability.  It is enough to wait until somebody else
    loads the drive, exploiting the vulnerability at that time.

    This vulnerability is known to be present in Solaris 2.4 and Solaris 2.5.
    Solaris distributions prior to Solaris 2.4 are also expected to be
    vulnerable.

2.  Impact

    Local users may be able to create or over-write arbitrary files on the
    system.  This can be leveraged to gain root privileges.

3.  Workaround

    AUSCERT believes the workarounds given in Sections 3.1 or 3.2 will address
    this vulnerability.  Vendor patches may also address this vulnerability
    in the future (Section 3.3).

3.1 Edit /etc/rmmount.conf

    The temporary files which are susceptible to attack are created by the
    /usr/lib/rmmount/action_filemgr.so.1 shared object which is called
    indirectly by vold through rmmount(1M).  rmmount(1M) can be
    configured so that it does not create the temporary files, thereby
    removing this vulnerability.

    To our knowledge, configuring rmmount(1M) in this fashion will not
    affect the functionality of vold.  It will, however, remove the
    ability of the Openwindows File Manager, filemgr(1), to automatically
    detect newly mounted media.

    To prevent rmmount(1M) creating temporary files, sites must edit the
    /etc/rmmount.conf file and comment out (or remove) any entry which
    references action_filemgr.so.

    The standard /etc/rmmount.conf contains the following entries which
    must be commented out (or deleted) to remove this vulnerability:

        action cdrom action_filemgr.so
        action floppy action_filemgr.so

    After applying this workaround, an example of /etc/rmmount.conf may look
    like:

        # @(#)rmmount.conf 1.2     92/09/23 SMI
        #
        # Removable Media Mounter configuration file.
        #

        # File system identification
        ident hsfs ident_hsfs.so cdrom
        ident ufs ident_ufs.so cdrom floppy pcmem
        ident pcfs ident_pcfs.so floppy pcmem

        # Actions
        #
        # Following two lines commented out to remove vold vulnerability
        #
        # action cdrom action_filemgr.so
        # action floppy action_filemgr.so

    Note that vold does not have to be restarted for these changes to
    take effect.

3.2 Remove the Volume Management system

    Sites who do not require the vold functionality should remove the complete
    set of Volume Management packages.  These are SUNWvolg, SUNWvolu and
    SUNWvolr.  These packages can be removed using pkgrm(1M).

3.3 Install vendor patches

    Currently, AUSCERT is not aware of any official patches which address
    this vulnerability.  When official patches are made available, AUSCERT
    suggests that they be installed.

- -----------------------------------------------------------------------------
AUSCERT wishes to thanks to Leif Hedstrom, Mark McPherson(QTAC),
Marek Krawus(UQ), DFN-CERT and CERT/CC for their assistance in this matter.
- -----------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident
Response and Security Teams (FIRST).

We strongly urge you to encrypt any sensitive information you send by email.
The CERT Coordination Center can support a shared DES key and PGP. Contact
the CERT staff for more information.

Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

CERT Contact Information
- ------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST
                (GMT-5)/EDT(GMT-4), and are on call for
                emergencies during other hours.

Fax      +1 412-268-6989

Postal address
        CERT Coordination Center
        Software Engineering Institute
        Carnegie Mellon University
        Pittsburgh PA 15213-3890
        USA

CERT publications, information about FIRST representatives, and other
security-related information are available for anonymous FTP from
        http://www.cert.org/
        ftp://info.cert.org/pub/

CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

To be added to our mailing list for CERT advisories and bulletins, send your
email address to
        cert-advisory-request@cert.org



CERT is a service mark of Carnegie Mellon University.

This file:
        ftp://info.cert.org/pub/cert_advisories/CA-96.17.Solaris_vold_vul
        http://www.cert.org
               click on "CERT Advisories"


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Aug. 30, 1996  Removed references to CA-96.17.README.
               Beginning of the advisory - removed AUSCERT advisory header
                 to avoid confusion.



-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMiTFq3VP+x0t4w7BAQG5VwP/fwY/1z1OKfXRAhNynR6fLuqR4KYqBIKe
2OhwlHxfdNhCVb7gYqWW0Dq1KMgMx8uBAqMqQtQ9BmZ9jjExk7x/hAOQKjVIPcKE
dbgNhPtSZHN5mZMTP/HPj07jYkd5oI92yvJD8ci9Tjh2s3OHdGxRe+L1T7u1MF2q
Cql9+g5MsC4=
=O/qX
-----END PGP SIGNATURE-----

