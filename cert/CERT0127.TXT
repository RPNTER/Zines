
-----BEGIN PGP SIGNED MESSAGE-----

=============================================================================
CERT(sm) Advisory CA-96.22
Original issue date: October 8, 1996
Last revised: November 13, 1996
              Noted that yy_readline_get does not require the patch included
              in the original advisory. Removed the patch from Sec. III.

              A complete revision history is at the end of this file.

Topic: Vulnerabilities in bash
- ------------------------------------------------------------------------------
                The original technical content for this advisory
                was published by the IBM-ERS response team and
                is used here with their permission.

This advisory describes two problems with the GNU Project's Bourne Again
SHell (bash): one in yy_string_get() and one in yy_readline_get().

The vulnerability in yy_string_get() allows the character with value 255
decimal to be used as a command separator. When used in environments where
users provide strings to be used as commands or arguments to commands, bash
can be tricked into executing arbitrary commands.

When the advisory was first published, was not clear whether the problem with
yy_readline_get() resulted in an exploitable vulnerability. As of November
1996, it appears that the problem is not exploitable in yy_readline_get.

The problems affect bash versions 1.14.6 and earlier.

The CERT/CC team recommends that you upgrade to bash 1.14.7 as soon as
possible, as discussed in Section III.A below. Section III.B contains a
patch for 1.14.7, which we recommend using to address the yy_readline_get()
problem.

We will update this advisory as we receive additional information.
Please check advisory files regularly for updates that relate to your site.

- ----------------------------------------------------------------------------

I. Description

   A. Introduction

      The GNU Project's Bourne Again SHell (bash) is a drop-in replacement
      for the UNIX Bourne shell (/bin/sh). It offers the same syntax as the
      standard shell, and it also includes additional functionality such as
      job control, command line editing, and history.

      Although bash can be compiled and installed on almost any UNIX
      platform, its most prevalent use is on "free" versions of UNIX such as
      Linux, where it has been installed as "/bin/sh" (the default shell for
      most uses).

      The bash source code is freely available from many sites on the
      Internet.

   B. Vulnerability Details

      1. Vulnerability in yy_string_get()

         There is a variable declaration error in the "yy_string_get()"
         function in the "parse.y" module of the "bash" source code. This
         function is responsible for parsing the user-provided command line
         into separate tokens (commands, special characters, arguments, etc.).
         The error involves the variable "string", which has been declared to
         be of type "char *".

         The "string" variable is used to traverse the character string
         containing the command line to be parsed. As characters are
         retrieved from this pointer, they are stored in a variable of type
         "int". On systems/compilers where the "char" type defaults to
         "signed char" this value will be sign-extended when it is assigned
         to the  "int" variable. For character code 255 decimal (-1 in two's
         complement form), this sign extension results in the value (-1)
         being assigned to the integer.

         However, (-1) is used in other parts of the parser to indicate the
         end of a command. Thus, the character code 255 decimal (377 octal)
         will serve as an unintended command separator for commands given to
         bash via the "-c" option. For example,

         bash -c 'ls\377who'

         (where "\377" represents the single character with value 255 decimal)
         will execute two commands, "ls" and "who".

    Note about yy_readline_get()
         A similar problem exists with the "yy_readline_get()" function, which
         is also in the file "parse.y" and which is used to read commands in
         interactive shells (ones that print a prompt and read from the
         keyboard, a shell script, or a pipe).

         However, it appears that this problem does not produce an exploitable
         vulnerability.

II.  Impact

     This unexpected command separator can be dangerous, especially on systems
     such as Linux where bash has been installed as "/bin/sh," when a program
     executes a command with a string provided by a user as an argument using
     the "system()" or "popen()" functions (or by calling "/bin/sh -c string"
     directly).

     This is especially true for the CGI programming interface in World Wide
     Web servers, many of which do not strip out characters with value 255
     decimal. If a user sending data to the server can specify the character
     code 255 in a string that is passed to a shell, and that shell is bash,
     the user can execute any arbitrary command with the user-id and
     permissions of the user running the server (frequently "root").

     The bash built-in commands "eval," "source," and "fc" are also
     potentially vulnerable to this problem.

III. Solution

     Install the most current version of bash. On 27 August 1996, Version
     1.14.7 of bash was released;  It is available from

        ftp://slc2.ins.cwru.edu/pub/dist/bash-1.14.7.tar.gz

     This version addresses the vulnerability in yy_string_get.
       
     When this advisory was first released, we included a patch for 
     yy_readline_get. It now appears that the patch is unnecessary as the
     problem is not exploitable in yy_readline_get. Upgrading to the current
     version of bash is sufficient.  

..............................................................................
Appendix A

The following is vendor-supplied information.
For the most up-to-date information, contact your vendor.

IBM Corporation
===============
AIX does not ship with the bash shell.

IBM and AIX are registered trademarks of International
Business Machines Corporation.


Silicon Graphics, Inc.
======================
SGI has distributed bash (version 1.14.6) as part of the Freeware 1.0
CDROM.  This collection of software has been compiled for IRIX as a
service to our customers, but is furnished without formal SGI support.

The problem identified by IBM in bash is present in the version of bash
on the Freeware 1.0 CDROM.  This CDROM included both the source code
for bash an compiled versions of it.

SGI urges customers to recompile bash after making the changes in
parse.y suggested by IBM.

As a service similar to that of the original Freeware 1.0 CDROM, SGI
intends to make available a compiled version of bash and its source in
the near future.  This action does not necessarily imply a commitment to any
future support actions for the programs found on the Freeware 1.0
CDROM.


Linux
=====		
Patches for the following Linux versions are available.

SuSE 4.2
	ftp://ftp.suse.de/suse_update/suse42/a1/bash.tgz

Red Hat 3.0.3
	ftp://ftp.redhat.com/pub/redhat/updates/3.0.3/{architecture}/bash-1.14.6.8.{architecture}.rpm


Yggdrasil
	Patched bash source and binary tar files are now  FTPable from

        ftp://ftp.yggdrasil.com/pub/support/fall95

WGS Linux Pro
	ftp://ftp.wgs.com/pub/linux/redhat/updates/3.0.3/i386/bash-1.14.6-8.i386.rpm

Caldera - Have built the new bash-1.14.7 code from prep.ai.mit.edu
	-/pub/gnu. Tested only insofar as to ascertain that the security bug
	is fixed.  Binary and source RPMs live in ftp -

	ftp://ftp.caldera.com/pub/cnd-1.0/updates/bash-1.14.7-1.i386.rpm
	ftp://ftp.caldera.com/pub/cnd-1.0/updates/bash-1.14.7-1.src.rpm

- ----------------------------------------------------------------------------
The CERT Coordination Center thanks IBM-ERS for permission to reproduce the
technical content in their IBM Emergency Response Service Security
Vulnerability Alerts ERS-SVA-E01-1006:004.1 and ERS-SVA-E01-1006:004.2.
These alerts are copyrighted 1996 International Business Machines Corporation.
- ----------------------------------------------------------------------------

If you believe that your system has been compromised, contact the CERT
Coordination Center or your representative in the Forum of Incident Response
and Security Teams (see ftp://info.cert.org/pub/FIRST/first-contacts).


CERT/CC Contact Information
- ---------------------------
Email    cert@cert.org

Phone    +1 412-268-7090 (24-hour hotline)
                CERT personnel answer 8:30-5:00 p.m. EST(GMT-5) / EDT(GMT-4)
                and are on call for emergencies during other hours.

Fax      +1 412-268-6989

Postal address
         CERT Coordination Center
         Software Engineering Institute
         Carnegie Mellon University
         Pittsburgh PA 15213-3890
         USA

Using encryption
   We strongly urge you to encrypt sensitive information sent by email. We can
   support a shared DES key or PGP. Contact the CERT/CC for more information.
   Location of CERT PGP key
         ftp://info.cert.org/pub/CERT_PGP.key

Getting security information
   CERT publications and other security information are available from
        http://www.cert.org/
        ftp://info.cert.org/pub/

   CERT advisories and bulletins are also posted on the USENET newsgroup
        comp.security.announce

   To be added to our mailing list for advisories and bulletins, send your
   email address to
        cert-advisory-request@cert.org

- -----------------------------------------------------------------------------
Copyright 1996 Carnegie Mellon University
This material may be reproduced and distributed without permission provided
it is used for noncommercial purposes and the copyright statement is
included.

CERT is a service mark of Carnegie Mellon University.
- -----------------------------------------------------------------------------

This file: ftp://info.cert.org/pub/cert_advisories/CA-96.22.bash_vuls
           http://www.cert.org
               click on "CERT Advisories"


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Revision history

Nov. 13, 1996   Noted that yy_readline_get does not require the patch included
                in the original advisory. Removed the patch from Sec. III.

Oct. 14, 1996	Added Appendix A - vendor information.




-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMook5nVP+x0t4w7BAQHqXAQAmZvYO3Yq57sCh7zPU98YXEnTAnIr2pta
RkyNfjSBkDlMhdiHD/nyNL0Hn1kCQK0oCXv6o27rgEb+B9ZfLvXj0O5DiNhaMB1Q
d+6b0H6o8iQ6WdEbU/3774LgFD1CpTp0MiZTwwMoNAfZ4/DD+zWGI4lO/5l2dU49
MGMkjqFgHoc=
=SbL0
-----END PGP SIGNATURE-----

