        _____________________________________________________

             The Computer Incident Advisory Capability

                         ___  __ __    _     ___

                        /       |     / \   /

                        \___  __|__  /___\  \___

        _____________________________________________________

                         Information Bulletin



May 16, 1991, 1330 PST                                     Number B-26



   Inconsistent Directory and File Permissions in SunOS 4.1 and  4.1.1  

________________________________________________________________________

PROBLEM:  SunOS versions 4.1 and 4.1.1 have several inconsistent file 

  and directory permissions.

PLATFORM: Sun computer architectures sun3, sun3x, sun4, and sun4c that

  run SunOS 4.1 or SunOS 4.1.1.

DAMAGE:  May allow unauthorized or unintended user access to files.

SOLUTIONS:  Patch/update available from Sun via Patch-ID# 100103-06 or

  through anonymous ftp from uunet.uu.net or from CIAC

IMPACT OF PATCH:  File and directory permissions set to intended

  permissions.  No other side-effects reported.  

________________________________________________________________________

 Critical Information about Inconsistent Directory and File Permissions



CIAC has discovered inconsistent directory and file permissions on

Sun Microsystems computers that run the SunOS 4.1 and 4.1.1 operating 

systems.  A patch is available from Sun Microsystems as the updated Patch 

ID# 100103-06 (this number is required to order this patch from the 

Sun Answer Center).  Sun Microsystems, Inc. states that this patch is

applicable to Sun architectures sun3, sun3x, sun4, and sun4c.  This patch

is also available via anonymous ftp at uunet.uu.net (IP address 192.48.96.2)

in the file sun-dist/100103-06.tar.Z or from CIAC. 



If you need assistance in obtaining this patch by anonymous ftp or

extracting compressed files, please use the instructions in the

appendix of this bulletin.  For additional information or assistance,

please contact CIAC:



        Kenneth L. Pon

        (415) 422-1783 or (FTS) 532-1783

        pon@cheetah.llnl.gov



        or



        Hal Brand

        (415) 422-0039 or (FTS) 532-0039

        brand@addvax.llnl.gov



        During working hours call CIAC at (415) 422-8193 or (FTS)

        532-8193 or send e-mail to ciac@llnl.gov.



        Send FAX messages to:  (415) 423-0913 or (FTS) 543-0913.

_________________________________________________________________________

                             Appendix

      Instructions for Obtaining Patch using ftp anonymous and 

                    Extracting Compressed Files



The string "%" is the default UNIX csh(1) prompt; the string "ftp>" is

the ftp(1C) prompt.  In the procedure described below, the text

displayed after these prompts on the same line as the prompts is what

you must enter.  Text displayed on any line without a prompt is what

the system replies in response.  System dialogue is indented to

distinguish it from surrounding comments.



First log into your system and find a place (e.g., a writeable

directory) to put the patch.  In this example, a directory is made for

the patch.  Note that you do not need to login as root to obtain the

patch.  However, you need to be root to apply the patch.



   % mkdir newpatch

   % cd newpatch



Next ftp to uunet.uu.net.  Login as "anonymous" and enter your identity

(in the following example, "pon") as your password.  Your password will 

not be echoed.  Then use the following procedure to obtain 100103-06.tar.Z.



   % ftp uunet.uu.net

   Connected to uunet.uu.net.

   220 uunet FTP server (Version 5.100 Mon Feb 11 17:13:28 EST 1991) ready.

   Name (uunet.uu.net:pon): anonymous

   331 Guest login ok, send ident as password.

   Password:

   230 Guest login ok, access restrictions apply.

   ftp> cd sun-dist

   250 CWD command successful.

   ftp> ls

   200 PORT command successful.

   150 Opening ASCII mode data connection for file list.

   100100-01.tar.Z

   100108-01.tar.Z

   100125-04.tar.Z

   100133-01.tar.Z

   100184-02.tar.Z

   100187-01.tar.Z

   100188-01.tar.Z

   100201-02.tar.Z

   100224-02.tar.Z

   100251-01.tar.Z

   100103-06.tar.Z

   README.sendmail

   226 Transfer complete.

   204 bytes received in 0.033 seconds (6 Kbytes/s)

   ftp> binary

   200 Type set to I.

   ftp> get 100103-06.tar.Z

   200 PORT command successful.

   150 Opening BINARY mode data connection for 100103-06.tar.Z (3830 bytes).

   226 Transfer complete.

   local: 100103-06.tar.Z remote: 100103-06.tar.Z

   3830 bytes received in 0.0039 seconds (9.7e+02 Kbytes/s)

   ftp> quit

   221 Goodbye.

   %



Now extract the usable files from the compressed (evident by the "Z"

suffice), tar (tape archive) file that you just ftp'ed.



   % uncompress 100103-06.tar.Z



This will uncompress 100103-06.tar.Z into 100103-06.tar.  To see what files

are archived on the 100103-06.tar file, use the following command:



   % tar tvf 100103-06.tar

   rw-r--r--  0/0   8106 May 14 10:23 1991 4.1secure.sh

   rw-r--r--  0/0    692 May  9 10:30 1991 README



Now extract the two files from tar format:



   % tar xvf 100103-06.tar

   x 4.1secure.sh, 8106 bytes, 16 tape blocks

   x README, 692 bytes, 2 tape blocks



The README file contains instructions for applying the patch.  Note that

the patch needs to be applied by user root.

__________________________________________________________________________

Brad Powell provided some of the information used in this bulletin.  This

document was prepared as an account of work sponsored by an agency of

the United States Government. Neither the United States Government nor

the University of California nor any of their employees, makes any

warranty, express or implied, or assumes any legal liability or

responsibility for the accuracy, completeness, or usefulness of any

information, apparatus, product, or process disclosed, or represents

that its use would not infringe privately owned rights. Reference

herein to any specific commercial products, process, or service by

trade name, trademark, manufacturer, or otherwise, does not necessarily

constitute or imply its endorsement, recommendation or favoring by the

United States Government or the University of California. The views and

opinions of authors expressed herein do not necessarily state or

reflect those of the United States Government or the University of

California, and shall not be used for advertising or product

endorsement purposes.

