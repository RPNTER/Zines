========================================================================

||            THE COMPUTER INCIDENT ADVISORY CAPABILITY               ||

||                                                                    ||        

||                             C I A C                                ||

||                                                                    ||

||                        INFORMATION NOTICE                          ||

========================================================================

                                   

            VMS Security Problem with ANALYZE/PROCESS_DUMP

            ----------------------------------------------



October 22, 1990, 1200 PST                                      Number B-4



Summary::  Critical VMS Security Problem Facts 

----------------------------------------------------------------------------

PROBLEM:        VMS security problem with the ANALYZE/PROCESS_DUMP command 

PLATFORM:       DEC VMS systems (all versions 4.0 to 5.3 including MicroVMS)

DAMAGE:         Allows system privileges to non-privileged users

                (including the user decnet on older VMS systems)

WORKAROUND:     Disable ANALYZE/PROCESS_DUMP for non-privileged users

PATCH:          Not currently available, but DEC is aware of the problem

SYSTEM IMPACT:  The workaround will disallow the use of analyze/process_dump 

                for non-privileged users.  Other program debuggers are

                unaffected

----------------------------------------------------------------------------



CIAC has learned of a serious security problem on Digital Equipment

Corp. (DEC) VMS systems.  The potential damage of this problem is that

users may gain unauthorized system privileges through the use of the

ANALYZE/PROCESS_DUMP dcl command.  In addition, systems that have set

up the FAL and default DECNET account to use the same directory have a

potential to allow system access to other VMS machines connected to

the network.



DEC is currently working on a permanent solution to this problem.  As

a interim measure, DEC recommends that this command be disabled for

all non-privileged users.  This may be accomplished using the

following procedure:



1.      Log into the system account.



2.      $ SET PROC/PRIV=ALL



3.      a) For VMS systems prior to V5.0,



        Modify SYS$MANAGER:SYSTARTUP.COM to include the following

        lines as the first two lines in the file:



                $ SET NOON

                $ MCR INSTALL ANALIMDMP.EXE/DELETE



        b) For VMS system V5.0 and later,



        Modify SYS$MANAGER:SYSTARTUP_V5.COM to include the following

        as the first two lines of the file:



                $SET NOON

                $ MCR INSTALL ANALIMDMP.EXE/DELETE



        c) For MicroVMS systems,



        The image ANALIMDMP.EXE is not installed by default, but

        SYSTARTUP.COM contains a suggestion of installing the image if

        you have multiple users on your system.  You mus ensure that

        this image is not installed in SYSTARTUP.COM.  You can use the

        following command to verify that the image is not installed:



        $MCR INSTALL ANALIMDMP/LIST



        If you receive the message similar to the following:



        %INSTALL-W-FAIL, failed to LIST entry for ANALIMDMP.EXE



        then you do not have the image installed.  Otherwise, proceed

        as step 3.a above.



4.      $ MCR INSTALL ANALIMDMP/DELETE



        This command removes the installed image from the active system.



5.      (Optional) Restart your systems and verify that the image is

        not installed using the following command:



        $MCR INSTALL ANALIMDMP/LIST



        If you receive the message similar to the following:



        %INSTALL-W-FAIL, failed to LIST entry for ANALIMDMP.EXE

        -INSTALL-E-NOKFEFND, Known File Entry not found



        then you do not have the image installed and your system does

        not have the security problem.



For additional information or assistance, please contact CIAC   



        Thomas A. Longstaff

        (415) 423-4416 or (FTS) 543-4416



        FAX:  (415) 423-0913 or (FTS) 543-0913 

 

or send e-mail to:



        ciac@tiger.llnl.gov



Neither the United States Government nor the University of California

nor any of their employees, makes any warranty, expressed or implied,

or assumes any legal liability or responsibility for the accuracy,

completeness, or usefulness of any information, product, or process

disclosed, or represents that its use would not infringe privately

owned rights.  Reference herein to any specific commercial products,

process, or service by trade name, trademark manufacturer, or

otherwise, does not necessarily constitute or imply its endorsement,

recommendation, or favoring by the United States Government or the

University of California.  The views and opinions of authors expressed

herein do not necessarily state or reflect those of the United States

Government nor the University of California, and shall not be used for

advertising or product endorsement purposes.



