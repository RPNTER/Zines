***********************************************************************
DDN Security Bulletin 90-01       DCA DDN Defense Communications System
25 Jan 90                Published by: DDN Security Coordination Center
                                      (SCC@NIC.DDN.MIL)  (800) 235-3155

                        DEFENSE  DATA  NETWORK
                          SECURITY  BULLETIN

The DDN  SECURITY BULLETIN  is distributed  by the  DDN SCC  (Security
Coordination Center) under  DCA contract as  a means of  communicating
information on network and host security exposures, fixes, &  concerns
to security & management personnel at DDN facilities.  Back issues may
be  obtained  via  FTP  (or  Kermit)  from  NIC.DDN.MIL  [26.0.0.73 or
10.0.0.51] using login="anonymous" and password="guest".  The bulletin
pathname is SCC:DDN-SECURITY-yy-nn (where "yy" is the year the bulletin
is issued and "nn" is a bulletin number, e.g. SCC:DDN-SECURITY-90-01).
**********************************************************************
 
                   SECURITY VIOLATION REPORTING
 

    a.  Initial Notification.  Any DDN user (person/department/agency)
having knowledge of a suspected network security violation must
contact the appropriate Defense Communications Agency OC/ACOC
(Operations Center/Area Communications Operations Center) to report
the violation.  If possible, reporting should be via secure means.

Secure and commercial telephone numbers to DCA Operations Centers are: 

  WESTHEM/CONUS OC has KY3-2222; STU III (DSN) 312-746-1849;
                       (COMM) 202-692-5726/2268 or 1-800-451-7413.

  PACIFIC ACOC has STU III (DSN) 315-456-2777; (COMM) 808-656-2777.  
 
  EUROPEAN ACOC has KY3-6429; STU III (DSN) 314-430-5703; 
                    (COMM) 49-0711-680-5703.  

The SCO or MC supervisor will request the following information:
 
        (1) Identity of caller:
            -What is caller's name and phone number
            -Where is caller calling from (organization)
            -Where is caller calling from (city & state)
            -What is the caller's DDN network address
 
        (2) Details about the incident:
            -When did the violation occur
            -What happened
            -How did the violation occur (if known)
            -What damage was done
            -What has the subscriber done about the violation
            -What networks are they connected to
            -What software is being used - Version
            -How many subscribers are known to be affected
            -How many subscribers are vulnerable (if known)
            -Who else has been notified - Names & phone
             numbers
 
        (3) Anything else the caller wishes to report
 
Once a suspected violation is reported and the above
information collected, the PACIFIC and EUROPEAN ACOCs will
immediately relay this information back to the DCAOC
(WESTHEM/CONUS) for action.
 

    b.  Follow-on Network Information via the Security Coordination
Center.  DCA, through direction provided by the DDN Network Security
Officer (NSO), will provide rapid and reliable follow-on information
on security exposures, fixes, and concerns via the SCC.  Distribution
of information is accomplished via DDN Security Bulletins.  Network
security and management personnel are encouraged to pay close
attention to these bulletins as they may be of great assistance either
in preventing network security problems or in solving existing
problems.  DDN Security Bulletins will be published on as "as needed"
basis.

Note: this bulletin starts a new numbering scheme for DDN Security
Bulletins.  From now on, all bulletin numbers, including those of 
previously issued bulletins, will follow the form YY-NN, where YY is
the year the bulletin is issued and NN is the number of the bulletin 
for that year.  Thus, this is DDN Security Bulletin 90-01; it is 
online at NIC.DDN.MIL as SCC:DDN-SECURITY-90-01.
-------


