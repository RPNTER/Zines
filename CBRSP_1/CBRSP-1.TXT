::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::                   We, the People proudly Present                     ::::
::                                                                          ::
::        The Cyberspace Chronicle            Volume 1, number 1            ::
::                                                                          ::
::                       Brought to you by Phardak                          ::
::::                                                                      ::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

First off, Welcome!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Carrying on a war against misinformation and injustice is hardly
something I can do by myself. If you wish to contribute to this
magazine, telnet to MARS hotel (130.18.64.3 log in as bbs) and send
mail to DigitalBishop. It's cumbersome but seeing as how I have no
real internet address, it'll do.

This magazine speaks out for the rights of those who have been
injusticed by the likes of Tom Forester, the US Government, and Bell
Security. Once again, speaking out for hackers' rights is risky and
hence the alias. Such is another injustice. For did someone not say,
long ago, that "all men were created equal"?

If you are going to submit something to me, make the subject either
Personal (to me personally), Letters to the Editor (which may be
reprinted herein), or Submission (an article which may be used). This
will provide some distinction.

Part 1: Tom Forester, the computer nerd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Mister Forester and his hippocritical words piss me off. This word's
for you, Tom. I'm not going to resort to name calling or
generalizations, look where they got Tom!

Everyone who gains access to a computer system through unintended
channels is NOT vandalizing it. Vandalizing would include leaving
notes or modifying data, which is simply not done by those whom I
would call "hackers".  Tom Forester states that hackers did damage to
the E911 system, which routes and handles 911 calls. THESE HACKERS DID
NO SUCH THING. They distributed a file describing how such calls are
handled to the public. This document was later found to be public
access information. In other words, Mister Forester has blown
distribution of information out of proportion into maliciously
damaging a system. Not quite the same, Tom.

Tom, you will probably be greatly disillusioned by this, but I am not
a social outcast. I have girlfriends and accquaintances just like
other people. Normal people, if you will. I do not wear glasses, I do
not wear plaid (God I hate plaid), and I don't have a pocket
protector. Right now I am wearing a Metallica T-Shirt and some old
jeans. Whoops, that makes me a heavy metal fan. Do I worship Satan
too? I am not mad at society, nor do I take said anger out on systems
by means of hacking. I have only had one pizza delivered to this house
in all the years I have been alive, I rarely drink carbonated
beverages, and eat even fewer snacks. Generalization contributes to
racism, Mr. Forester. It is now also a cause of discrimination against
hackers.  Where were we? Oh yes, my computer skills are mediocre. I've
been using computers since I was 4, that gives me 10 years of
experience, plus mastery of C, Pascal, 6502 and 65816 assembly
language, not to mention higher level languages like BASIC, Logo, etc.
and partial mastery of 80x86 AT Assembly language. One again, a
generalization.

Also, Mr. Forester, it is time to clamp down on hacking and throw all
these nerds in jail. This was based on your words and generalizations,
not mine. Why do I hack? To free information from Illuminati hands, to
show system administrators their security holes. Ask Prarie States
Marketing of Lenexa, KS if any harm was done to their system. No, I
left a little note to them showing them all the security holes they
had. When I came back, they had patched these holes. Maybe it's also a
game of chess for me, outwitting the sysop. The idea of Pawn takes
King always appealed to me. There are many definitions of 'hacking'.
My personal definition, is any person who obtains access to a system
through unauthorized channels and does not damage the system. It
stands to reason, however, that hackers modify security logs to ensure
their safety, lest Big Brother and his injustices come knocking at
your door. When the public thinks of hacking, they will think of
someone who 'breaks in' to a top secret computer and trashes
everything. This guy would not be a hacker, but would be an outcast of
any hacker community as would anyone else who damages a system, or
creates viruses. This sort of thing is not tolerated anywhere in the
world, Forester, not even by those uncouth, nerdly hackers. You also
have the media to thank for that misleading definition of 'hacker'.

God, I could go on forever, but I think I'll stop here, Forester.

Tom Forester is not a psychologist, neither is he an ethicist, nor is
he an anylist. Other than that, he's a good writer, I want you to
know.

** If you reading this have anything to say to Forester to be printed in the    next issue, or anythg to say to me, please send it.

Part 2: Definition of a hacker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
What is a hacker, to clarify the word? A hacker is someone who:

-> NEVER damages any system
-> USUALLY tells the system operator of security holes one he has
   finished
-> ALWAYS gains access thru unauthorized channels

'Nuff said? Undoubtedly, I forgot a few. Mail them to me.

Part 3: A parting thought (and a poor choice of words!)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If I left anything unsaid, write an article.
Hello to the Computer Underground Digest,
   The EFF,
   Knight Lightning,
   and the MECH.
Expect future issues to be lengthier than this one..

You'll hear more from me soon.

   - Phardak
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
 Another file downloaded from:                                NIRVANAnet(tm)

 &TOTSE                510/935-5845   Walnut Creek, CA         Taipan Enigma
 Burn This Flag        408/363-9766       San Jose, CA                Zardoz
 realitycheck          415/666-0339  San Francisco, CA    Poindexter Fortran
 Governed Anarchy      510/226-6656        Fremont, CA             Eightball
 New Dork Sublime      805/823-1346      Tehachapi, CA               Biffnix
 Lies Unlimited        801/278-2699 Salt Lake City, UT            Mick Freen
 Atomic Books          410/669-4179      Baltimore, MD               Baywolf
 Sea of Noise          203/886-1441        Norwich, CT             Mr. Noise
 The Dojo              713/997-6351       Pearland, TX               Yojimbo
 Frayed Ends of Sanity 503/965-6747     Cloverdale, OR              Flatline
 The Ether Room        510/228-1146       Martinez, CA Tiny Little Super Guy
 Hacker Heaven         860/456-9266        Lebanon, CT         The Visionary
 The Shaven Yak        510/672-6570        Clayton, CA             Magic Man
 El Observador         408/372-9054        Salinas, CA         El Observador
 Cool Beans!           415/648-7865  San Francisco, CA        G.A. Ellsworth
 DUSK Til Dawn         604/746-5383   Cowichan Bay, BC         Cyber Trollis
 The Great Abyss       510/482-5813        Oakland, CA             Keymaster

                          "Raw Data for Raw Nerves"
X-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-X
